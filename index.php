<?php

use ProjektiBlog\public\classes\SessionManager;

require_once 'public/classes/SessionManager.php';

$session = new SessionManager();
if ($session->isSignedIn()) {
    if ($session->getUserRole() !== 2) {
        header("Location: home/index.php");
    } else {
        header("Location: public/authors/authors_dashboard.php");
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="dist/output.css">
    <link rel="stylesheet" href="dist/custom.css">
    <script src="js_jquery/jquery.js"></script>
    <script src="js_jquery/jquery.validate.js"></script>
    <script src="js_jquery/jquery.validate.min.js"></script>
    <script src="js_jquery/minified/gsap.min.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=League+Spartan:wght@300;400;700&family=Open+Sans:wght@300;500;700&family=Oswald:wght@600&family=PT+Mono&family=Playfair+Display:wght@400;600;700&family=Roboto+Mono:wght@700&family=Roboto+Slab:wght@300;400;700&family=Roboto:wght@300;500;700&family=Slabo+27px&family=Source+Sans+3:wght@300&family=Ysabeau+SC:wght@300;600&display=swap');
    </style>
</head>

<body class="bg-[black] lg:overflow-hidden  overflow-x-hidden">
    <div class="round_shape bg-amber-400 opacity-75 w-1/4  h-1/4 absolute right-0"></div>
    <header id="header_container" class="w-screen lg:h-screen h-auto relative text-[white]">
        <nav class="flex w-full justify-between md:px-16 px-0 py-[0] items-center h-[90px]">
            <img src='resources/images/Plates and Places. (1).png' class='2xl:w-[500px] lg:w-[300px] md:w-[250px] w-[200px] object-cover 2xl:translate-y-4 md:-translate-y-[15px] -translatey-y-[12px]' alt='Plates And Places'>
            <div class="mr-4">
                <a href="user/login.php">
                    <button class="2xl:w-60 2xl:h-[5rem] 2xl:mt-8 lg:w-40 lg:h-[3.3rem] w-32 h-[2.8rem] hover:shadow-sm hover:shadow-white after:absolute after:w-full after:h-full after:bg-amber-400 after:-top-1 after:-left-1 after:-z-50
 group bg-[white] border-[none] ml-6 font-bold text-[1.6rem] after:hover:-top-2 after:hover:-left-2 after:duration-200 
 relative text-[black] cursor-pointer flex items-center justify-center md:gap-3 gap-2">
                        <span class="group-hover:text-white group-hover:z-20 duration-200 2xl:text-4xl">Login</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-in transform group-hover:translate-x-3 group-hover:stroke-white group-hover:z-20 duration-300">
                            <path d="M15 3h4a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2h-4"></path>
                            <polyline points="10 17 15 12 10 7"></polyline>
                            <line x1="15" y1="12" x2="3" y2="12"></line>
                        </svg>
                        <span class="absolute top-0 left-0 bg-black w-0 h-full group-hover:w-full transition-all duration-300"></span>
                    </button>
                </a>
            </div>
        </nav>
        <div class="flex xl:items-center lg:justify-between justify-center lg:items-stretch items-center ">
            <article class="md:text-left text-center lg:w-[45%] md:w-[90%] lg:mx-0 md:mx-8 mx-auto lg:pt-8 md:pt-32 pt-8 w-[90%] lg:relative absolute  md:pl-8 pl-0 px-0 md:px-4">
                <ul class="md:pt-4 pt-0 lg:text-[1.3rem] md:text-3xl lg:pr-4 pr-0 [list-style:none] relative md:z-auto z-40">
                    <h1 id="foodiesTitle" class=' 2xl:text-9xl lg:text-[3.5rem] md:text-[4.8rem] text-[3.5rem] font-bold leading-none lg:w-[90%] w-full'>The social media for foodies.</h1>
                    <div class='lg:w-[95%] md:w-[70%] w-full'>
                        <li class='lg:pt-4 md:pt-8 pt-6'>Hungry for more than just a meal? Join the culinary conversation! Be the chef of your own story</li>
                        <li class='lg:pt-4 md:pt-8 pt-6'>Scroll through a world of posts - a buffet for your eyes and cravings. It's not just food; it's a journey on a plate</li>
                    </div>
                </ul>
                <a href="">
                    <button class="px-12 w-auto lg:h-[3.6rem] md:h-[4.3rem] h-[3.6rem] bg-[white] border-[none] group text-[black] font-bold text-[1.8rem]
         mt-8 relative cursor-pointer hover:bg-amber-400 duration-200">
                        <div class="round_shape2 absolute top-0 right-0 w-1/2 h-full bg-amber-400 group-hover:w-full duration-300 "></div>
                        <div class="round_shape3 absolute top-0 left-0 w-1/2 h-full bg-amber-400 group-hover:w-full  duration-300"></div>
                        <span class="relative z-30 group-hover:text-transparent">Own a food business?</span>
                        <span class="absolute flex items-center justify-center top-0 left-0 w-full text-transparent group-hover:text-black h-full group-hover:scale-100 scale-0 transform duration-300">Click here to learn more.</span>
                    </button>
                </a>

            </article>
            <div class="lg:overflow-visible overflow-hidden pt-16 lg:w-[70%] md:w-5/6 w-full md:h-[70vh] h-full flex flex-col  relative lg:z-auto -z-20 lg:pt-16">
                <div class="flex">
                    <img src="resources/images/blog_img2.avif" alt="" class="w-[200px] lg:active:scale-150 transform duration-200 2xl:w-[600px] 2xl:h-[350px] md:w-[450px] h-[200px] object-cover opacity-20 lg:opacity-60 filter contrast-[100%] active:opacity-90 active:filter active:contrast-[120%]  active:brightness-100 brightness-[0.7] [transition:0.3s_ease] ">
                    <div class="w-[400px]"></div>
                    <img src="resources/images/blog_img1.avif" alt="" class="w-[200px] lg:active:scale-150 transform duration-2002xl:w-[600px] 2xl:h-[350px] md:w-[450px] h-[200px] object-cover opacity-20 lg:opacity-40 filter contrast-[100%] active:opacity-80 z-0 active:z-30 active:filter active:contrast-[120%]  active:brightness-100 brightness-[0.7] [transition:0.3s_ease]">
                </div>
                <div class="flex">
                    <div class="w-[400px]"></div>
                    <img src="resources/images/blog_img3.avif" alt="" class="w-[200px] lg:active:scale-150 transform duration-200 2xl:w-[600px] 2xl:h-[350px] md:w-[450px] h-[200px] object-cover opacity-20 lg:opacity-60 filter contrast-[100%] active:opacity-90 active:filter active:contrast-[120%]  active:brightness-100 brightness-[0.7] [transition:0.3s_ease] ">
                    <div class="w-[400px]"></div>
                </div>
                <div class="flex">
                    <img src="resources/images/blog_img5.png" alt="" class="w-[200px] lg:active:scale-150 transform duration-200 2xl:w-[600px] 2xl:h-[350px] md:w-[450px] h-[200px] object-cover opacity-20 lg:opacity-60 filter contrast-[100%] active:opacity-90 active:filter active:contrast-[120%]  active:brightness-100 brightness-[0.7] [transition:0.3s_ease] ">
                    <div class="w-[400px]"></div>
                    <img src="resources/images/blog_img4.avif" alt="" class="w-[200px] lg:active:scale-150 transform duration-200 2xl:w-[600px] 2xl:h-[350px] md:w-[450px] h-[200px] object-cover opacity-20 lg:opacity-60 filter contrast-[100%] active:opacity-90 active:filter active:contrast-[120%]  active:brightness-100 brightness-[0.7] [transition:0.3s_ease] ">
                </div>
                <img src="resources/images/click.png" alt="" class="lg:visible invisible absolute right-[25%] top-[20%] -z-40 opacity-60 w-[30%] invert">
            </div>
        </div>
    </header>
    <script>
        // Wait for the document to be ready
        $(document).ready(function() {
            // Get the text content of the element
            var text = $('#foodiesTitle').text();

            // Clear the text content of the element
            $('#foodiesTitle').empty();

            // Iterate through each character and create a span for it
            for (var i = 0; i < text.length; i++) {
                // Create a span for each letter
                var span = $('<span>' + text[i] + '</span>');

                // Append the span to the element with delay for animation effect
                span.appendTo('#foodiesTitle').hide().delay(i * 80).fadeIn(400);
            }
        });
    </script>
</body>

</html>