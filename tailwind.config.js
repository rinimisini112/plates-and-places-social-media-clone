/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.{html,js,php}"],
  theme: {
    extend: {
      colors: {
        // Background color
        "dark-active": "#31343d",
        dark: "#191c20",
        darker: "#0c0e10",

        "adm-active": "rgb(127,145,183)",
        "adm-black": "#1a1a1a",
        "adm-grey": "#333333",
        "adm-lgrey": "rgb(230,230,230)",
        "adm-white": "#f6f6f6",
      },
      boxShadow: {
        round: "0 0 3px ",
      },
    },
  },
};
