-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 13, 2023 at 12:10 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sis_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` int NOT NULL,
  `published_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `main_image` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `content` text COLLATE utf8mb4_general_ci,
  `content1` text COLLATE utf8mb4_general_ci,
  `content2` text COLLATE utf8mb4_general_ci,
  `sub_title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title_content` text COLLATE utf8mb4_general_ci,
  `sub_title_content1` text COLLATE utf8mb4_general_ci,
  `sub_title_content2` text COLLATE utf8mb4_general_ci,
  `image` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title1` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title1_content` text COLLATE utf8mb4_general_ci,
  `sub_title1_content1` text COLLATE utf8mb4_general_ci,
  `sub_title1_content2` text COLLATE utf8mb4_general_ci,
  `sub_title2` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title2_content` text COLLATE utf8mb4_general_ci,
  `sub_title2_content1` text COLLATE utf8mb4_general_ci,
  `sub_title2_content2` text COLLATE utf8mb4_general_ci,
  `sub_title3` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title3_content` text COLLATE utf8mb4_general_ci,
  `sub_title3_content1` text COLLATE utf8mb4_general_ci,
  `sub_title3_content2` text COLLATE utf8mb4_general_ci,
  `sub_title4` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title4_content` text COLLATE utf8mb4_general_ci,
  `sub_title4_content1` text COLLATE utf8mb4_general_ci,
  `sub_title4_content2` text COLLATE utf8mb4_general_ci,
  `sub_title5` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title5_content` text COLLATE utf8mb4_general_ci,
  `sub_title5_content1` text COLLATE utf8mb4_general_ci,
  `sub_title5_content2` text COLLATE utf8mb4_general_ci,
  `sub_title6` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title6_content` text COLLATE utf8mb4_general_ci,
  `sub_title6_content1` text COLLATE utf8mb4_general_ci,
  `sub_title6_content2` text COLLATE utf8mb4_general_ci,
  `sub_title7` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sub_title7_content` text COLLATE utf8mb4_general_ci,
  `sub_title7_content1` text COLLATE utf8mb4_general_ci,
  `sub_title7_content2` text COLLATE utf8mb4_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `published_at`, `main_image`, `title`, `content`, `content1`, `content2`, `sub_title`, `sub_title_content`, `sub_title_content1`, `sub_title_content2`, `image`, `sub_title1`, `sub_title1_content`, `sub_title1_content1`, `sub_title1_content2`, `sub_title2`, `sub_title2_content`, `sub_title2_content1`, `sub_title2_content2`, `sub_title3`, `sub_title3_content`, `sub_title3_content1`, `sub_title3_content2`, `sub_title4`, `sub_title4_content`, `sub_title4_content1`, `sub_title4_content2`, `sub_title5`, `sub_title5_content`, `sub_title5_content1`, `sub_title5_content2`, `sub_title6`, `sub_title6_content`, `sub_title6_content1`, `sub_title6_content2`, `sub_title7`, `sub_title7_content`, `sub_title7_content1`, `sub_title7_content2`) VALUES
(1, '2023-12-13 10:51:27', 'IMG_0001.jpg', 'Celebrate Your Unique Style with SIS: Where Fashion Meets Empowerment and Lifelong Friendship.', 'Are you in search of a new and stylish clothing brand? Look no further! Welcome to the grand opening of <span class=\"lg:text-2xl rische\">SlS</span>,\n                    founded by two individuals with 25 years of shared experiences. Immerse yourself in a world where fashion\n                    expertise meets makeup artistry, creating a unique blend of style and empowerment. Let\'s explore what sets <span class=\"lg:text-2xl rische\">SlS</span>\n                    apart and why it\'s your new go-to destination for contemporary fashion.', 'Rooted in a lifelong friendship and shared memories, <span class=\"lg:text-2xl rische\">SlS</span> is a manifestation of passion, creativity,\n                    and a deep appreciation for the art of style. Join us as we introduce you to the essence of <span class=\"lg:text-2xl rische\">SlS</span> and the exciting\n                    journey that lies ahead.', NULL, 'A Unique Fusion of Fashion and Friendship <span class=\"stink\">:</span>', 'Two distinct worlds collided 25 years ago to create the universe of SIS. Rezarta and Drita,\n                    originally friends and now considered family, affectionately refer to each other as <span class=\"lg:text-2xl rische\">SlS</span>.\n                    Hailing from the realms of makeup and beauty, fine art, and design, their shared passion for fashion\n                    not only propelled them to excel in their individual domains but also led them to collaboratively\n                    establish their own brand, <span class=\"lg:text-2xl rische\">SlS</span>.', NULL, NULL, 'IMG_0004.jpg', 'Meet the Founders <span class=\"stink\">:</span>', 'Blending their unique talents to create a brand that speaks to the modern, in-the-know individual.', 'One founder, a seasoned fashion expert in the Berlin and London scene, brings an eye for trends and timeless elegance.\n                            The other, a skilled makeup artist, adds a touch of transformative beauty to the mix.\n                            Together, they are excited to present a brand that transcends the boundaries of conventional fashion.', NULL, 'Unveiling the Collections <span class=\"stink\">:</span>', 'With the opening of SIS, we proudly showcase our inaugural collections that capture the spirit of contemporary fashion.\n                    From versatile everyday wear to statement pieces that turn heads, our curated selections are a testament to the founders\'\n                    commitment to offering diverse and distinctive fashion choices. Get ready to discover your new go-to pieces that effortlessly\n                    blend comfort with style.', NULL, NULL, 'The SIS Experience <span class=\"stink\">:</span>', 'More than a clothing store, SIS is a fashion experience. Our focus is on creating evergreen staple pieces qualitatively\n                    that can always style with your old clothes that haven\'t been used for a while, curating timeless styles, and providing\n                    expertise in your purchases. Experience personal styling by our expert Drita, who has extensive fashion experience,\n                    bringing a unique touch to your fashion journey.', 'Explore daily outfits and night looks, including vintage pieces and festival\n                    outfits, and always leave a lasting impression. Immerse yourself in cutting-edge apparel and exquisite jewelry,\n                    transforming your fashion sense and perspective. It\'s not just about what you wear but how you wear it. With us,\n                    you are guided by true trendsetters.', NULL, 'SIS Community', 'More than a clothing store, SIS is a fashion experience. Our focus is on creating evergreen staple pieces qualitatively\n                    that can always style with your old clothes that haven\'t been used for a while, curating timeless styles, and providing\n                    expertise in your purchases. Experience personal styling by our expert Drita, who has extensive fashion experience,\n                    bringing a unique touch to your fashion journey. Explore daily outfits and night looks, including vintage pieces and festival\n                    outfits, and always leave a lasting impression. Immerse yourself in cutting-edge apparel and exquisite jewelry,\n                    transforming your fashion sense and perspective. It\'s not just about what you wear but how you wear it. With us,\n                    you are guided by true trendsetters.', NULL, NULL, 'CONCLUSION', 'As you explore the world of SIS, remember that fashion is a form of self-expression, and everyone has a unique \n                    story to tell. Embrace your individuality, celebrate your journey, and let SIS be your partner in \n                    expressing the beautiful, confident creation that you are. Join us in the pursuit of fashion that empowers \n                    and uplifts - because at SIS, we believe that style is a celebration of you!', 'As SIS opens its doors to the world, we extend a warm welcome to you, our discerning and stylish \n                community. This is just the beginning of an exciting journey, and we invite you to be part of our story. \n                Discover the fusion of fashion, friendship, and individuality at SIS - where every piece tells a story, \n                and everyone is celebrated. Cheers to the start of something beautiful!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
