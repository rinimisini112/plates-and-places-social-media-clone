<nav class="flex justify-between items-center w-full h-[90px] lg:px-8 px-2">
            <a href="../index.php"><img src="../resources/images/Plates and Places. (2).png" class="w-[200px] md:w-[250px] object-cover translate-y-[-17px]" alt="Plates And Places"></a>
            <a href="login.php" class="group bg-black md:px-12 px-6 py-2 font-bold text-2xl relative text-white cursor-pointer group md:mr-6 mr-2">
                Login
                <span class="absolute -left-0  -top-0 group-hover:-left-3 duration-200 group-hover:-top-3 bg-gray-600 w-full h-full -z-10"></span>
            </a>
        </nav>