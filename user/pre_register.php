<?php 
require 'include/head.php';
?>
<head>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=League+Spartan:wght@300;400;700&family=Open+Sans:wght@300;500;700&family=Oswald:wght@600&family=PT+Mono&family=Playfair+Display:wght@400;600;700&family=Roboto+Mono:wght@700&family=Roboto+Slab:wght@300;400;700&family=Roboto:wght@300;500;700&family=Slabo+27px&family=Source+Sans+3:wght@300&family=Ysabeau+SC:wght@300;600&display=swap');
    </style>
</head>
<body class="overflow-x-hidden ">
    <header class="w-full h-screen relative text-black flex flex-col items-center lg:gap-0 gap-6">
        <?php require 'include/navbar.php'?>
        <article class="lg:w-[70%] w-4/5 flex flex-col items-center lg:gap-8 md:gap-12 gap-8">
            <h1 class="relative lg:text-[2rem] text-[2rem] md:text-[4rem] ">Register with </h1>
            <div class="w-full lg:w-2/4 flex flex-col items-center   lg:gap-2 md:gap-8 gap-4 ml-0 lg:ml-6 lg:mb-0 mb-7">
                <a href="register.php" class="lg:w-full w-full md:w-[80%]"><div class="relative after:absolute after:left-0 after:top-0 after:w-0 after:h-full after:bg-slate-200 after:hover:w-full after:duration-300 after:bg-opacity-50 after:-z-10 cursor-pointer flex justify-between items-center border lg:py-5 lg:px-6 py-6 px-6 border-gray-400">
                    <img src="../resources/images/email_logo.png" alt="" class="w-[10%]">
                    <p class="lg:text-[1rem] md:text-2xl text-lg lg:font-bold pr-4">Sign in with Email!</p>
                </div></a>
                <div class="relative after:absolute after:left-0 after:top-0 after:w-0 after:h-full after:bg-slate-200 after:hover:w-full after:duration-300 after:bg-opacity-50 after:-z-10 cursor-pointer lg:w-full w-full md:w-[80%] flex justify-between items-center border lg:py-5 lg:px-6 py-6 px-6 border-gray-400">
                    <img src="../resources/images/google_logo.png" alt="" class="w-[10%]">
                    <p class="lg:text-[1rem] md:text-2xl text-lg lg:font-bold pr-4">Sign in with Google!</p>
                </div>
                <div class="relative after:absolute after:left-0 after:top-0 after:w-0 after:h-full after:bg-slate-200 after:hover:w-full after:duration-300 after:bg-opacity-50 after:-z-10 cursor-pointer lg:w-full w-full md:w-[80%] flex justify-between items-center border lg:py-5 lg:px-6 py-6 px-6 border-gray-400">
                    <img src="../resources/images/apple_logo.png" alt="" class="w-[10%]">
                    <p class="lg:text-[1rem] md:text-2xl text-lg lg:font-bold pr-4">Sign in with Apple &nbsp;&nbsp;&nbsp;</p>
                </div>
                <div class="relative after:absolute after:left-0 after:top-0 after:w-0 after:h-full after:bg-slate-200 after:hover:w-full after:duration-300 after:bg-opacity-50 after:-z-10 cursor-pointer lg:w-full w-full md:w-[80%] flex justify-between items-center border lg:py-5 lg:px-6 py-6 px-6 border-gray-400">
                    <img src="../resources/images/facebook_logo.png" alt="" class="w-[10%]">
                    <p class="lg:text-[1rem] md:text-2xl text-lg lg:font-bold pr-4">Sign in with Facebook!</p>
                </div>

            </div>
            <div class="text-center lg:w-[55%] w-full ">
                <p class="leading-6 lg:text-[1.1rem] text-[1.1rem] md:text-[1.3rem] tracking-[1px]">
                    By creating an account, you agree to our Terms of Service and have read and understood the Privacy Policy
                </p>
            </div>
        </article>
    </header>
</body>

</html>