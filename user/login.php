<?php

use ProjektiBlog\public\classes\SessionManager;

require '../vendor/autoload.php';
require 'include/head.php';
$session = new SessionManager();
if ($session->isSignedIn()) {
    if ($session->getUserRole() !== 2) {
        header("Location: ../home/index.php");
    } else {
        header("Location: ../public/authors/authors_dashboard.php");
    }
}
?>
<style>
    @import url('https://fonts.googleapis.com/css2?family=League+Spartan:wght@300;400;700&family=Open+Sans:wght@300;500;700&family=Oswald:wght@600&family=PT+Mono&family=Playfair+Display:wght@400;600;700&family=Roboto+Mono:wght@700&family=Roboto+Slab:wght@300;400;700&family=Roboto:wght@300;500;700&family=Slabo+27px&family=Source+Sans+3:wght@300&family=Ysabeau+SC:wght@300;600&display=swap');

    #username_or_email,
    #password {
        margin-bottom: 10px;
    }
</style>
</head>

<body class="overflow-x-hidden ">
    <header class="w-full h-screen relative text-black flex flex-col">
        <nav class="flex justify-between items-center w-full h-[90px] lg:px-8 px-2">
            <a href="../index.php"><img src="../resources/images/Plates and Places. (2).png" class="w-[200px] md:w-[250px] object-cover translate-y-[-17px] " alt="Plates And Places"></a>
            <a href="pre_register.php" class="block lg:hidden"><button class=" bg-black md:px-12 px-6 py-2 -translate-y-[2px]  font-bold text-2xl relative text-white cursor-pointer group md:mr-6 mr-2">
                    Sign up
                </button>
            </a>

            <a href="pre_register.php" class="hidden lg:block"><button class=" -translate-y-1 w-68 h-14 bg-white mr-12 font-bold text-2xl relative text-black cursor-pointer group">Don't have an account?
                    <span class="absolute left-0 bottom-0 w-full h-full hidden lg:block bg-black tracking-widest transform scale-x-0 origin-left group-hover:text-white pt-3 transition-transform duration-300 group-hover:scale-x-100">Sign up Now!</span>
                </button></a>
        </nav>
        <article class="w-3/4 mx-auto flex flex-col  items-center relative">
            <h1 class=" text-center text-2xl py-12">Login to Plates And Places</h1>
            <?php
            if (isset($_SESSION["registerSuccesful"])) {
                echo "<div id='loginMessage' class=' text-green-500 text-center pb-4'>" . $_SESSION["registerSuccesful"] . "</div>";
                unset($_SESSION["registerSuccesful"]);
            }
            if (isset($_SESSION['message'])) {
                echo "<div id='loginMessage' class=' text-red-500 text-center pb-4'>" . $_SESSION["message"] . "</div>";
                unset($_SESSION["message"]);
            }
            ?>
            <div class="flex flex-col lg:flex-row lg:justify-center lg:w-5/6 w-full lg:items-center md:items-center">
                <form action="../public/auth/login.inc.php" method="post" id="login" class=" lg:ml-6 ml-0 w-full md:w-[80%] flex flex-col lg:gap-4 gap-6 lg:w-2/4 lg:px-6 px-0">
                    <label for="username_or_email" class="text-center lg:text-left lg:text-lg text-lg md:text-xl text-gray-400">Username or Email Adress</label>
                    <input type="text" name="username_or_email" id="username_or_email" placeholder="Username or Email Adress" class="lg:w-[90%] w-full">
                    <label for="pass" class="lg:text-lg text-lg md:text-xl text-center lg:text-left  text-gray-400">Password</label>
                    <input type="password" name="pass" id="pass" placeholder="password" class="lg:w-[90%] w-full">
                    <input type="submit" value="Log in" id="submitLogin" name="login" class="w-full lg:w-[90%] h-12 cursor-pointer text-2xl bg-black text-white ">
                    <div class=" py-2 lg:hidden flex justify-center">
                        <a href="">Forgot Your Password?</a>
                    </div>
                </form>
                <div class="flex flex-row gap-6 items-center my-8 mx-auto lg:block lg:w-10 lg:pr-6">
                    <div class=" h-px md:w-48 w-24 lg:w-px lg:h-24 bg-gray-400 lg:translate-x-[10px]"></div>
                    <p class="lg:my-4 text-gray-400">OR</p>
                    <div class=" h-px md:w-48 w-24 lg:w-px lg:h-24 bg-gray-400 lg:translate-x-[10px]"></div>
                </div>
                <div class="w-full lg:w-2/4 flex flex-col items-center   gap-4 ml-0 lg:ml-6 lg:mb-0 mb-7">
                    <div class="relative after:absolute after:left-0 after:top-0 after:w-0 after:h-full after:bg-slate-200 after:hover:w-full after:duration-300 after:bg-opacity-50 after:-z-10 cursor-pointer lg:w-full w-full md:w-[80%] flex justify-between items-center border lg:p-6 py-6 px-10 border-gray-400">
                        <img src="../resources/images/google_logo.png" alt="" class="w-[10%]">
                        <p class="lg:text-[1rem] md:text-2xl text-lg lg:font-bold pr-4">Sign in with Google!</p>
                    </div>
                    <div class="relative after:absolute after:left-0 after:top-0 after:w-0 after:h-full after:bg-slate-200 after:hover:w-full after:duration-300 after:bg-opacity-50 after:-z-10 cursor-pointer lg:w-full w-full md:w-[80%] flex justify-between items-center border lg:p-6 py-6 px-10 border-gray-400">
                        <img src="../resources/images/apple_logo.png" alt="" class="w-[10%]">
                        <p class="lg:text-[1rem] md:text-2xl text-lg lg:font-bold pr-4">Sign in with Apple &nbsp;&nbsp;&nbsp;</p>
                    </div>
                    <div class="relative after:absolute after:left-0 after:top-0 after:w-0 after:h-full after:bg-slate-200 after:hover:w-full after:duration-300 after:bg-opacity-50 after:-z-10 cursor-pointer lg:w-full w-full md:w-[80%] flex justify-between items-center border lg:p-6 py-6 px-10 border-gray-400">
                        <img src="../resources/images/facebook_logo.png" alt="" class="w-[10%]">
                        <p class="lg:text-[1rem] md:text-2xl text-lg lg:font-bold pr-4">Sign in with Facebook!</p>
                    </div>
                </div>
            </div>
            <div class=" py-10 sm:pt-8 lg:block hidden">
                <a href="" class="relative group">Forgot Your Password?
                    <span class="absolute -left-1  -bottom-1 transform scale-0 group-hover:scale-100 duration-200 bg-adm-black w-full h-px"></span>
                </a>
            </div>
        </article>
    </header>
    <script>
        // Wait for the document to be ready
        $(document).ready(function() {
            // Function to hide the message after 4 seconds
            function hideMessage() {
                $('#loginMessage').fadeOut('slow');
            }

            // Check if the message element exists
            if ($('#loginMessage').length > 0) {
                // Hide the message after 4 seconds
                setTimeout(hideMessage, 4000);
            }
        });
        const emailInput = document.getElementById('email');
        const passwordInput = document.getElementById('p');
        const submitButton = document.getElementById('submitLogin');

        // Function to check if the password field is empty
        function checkPassword() {
            if (passwordInput.value.trim() !== '') {
                submitButton.removeAttribute('disabled');
            } else {
                submitButton.setAttribute('disabled', 'disabled');
            }
        }

        // Initial check when the page loads
        checkPassword();

        // Add an input event listener to the password field
        passwordInput.addEventListener('input', checkPassword);
        $(document).ready(function() {
            $.validator.addMethod("alphabetsOnly", function(value, element) {
                return /^[a-zA-Z\s]+$/.test(value);
            }, "Enter letters only.");

            // Add validation rules and messages to your registration form
            $("#login").validate({
                rules: {
                    username_or_email: {
                        required: true,
                    },
                    pass: {
                        minlength: 8,
                        required: true,
                    },
                },
                messages: {
                    username_or_email: {
                        required: "Please enter your email or username",
                    },
                    pass: {
                        required: "Please enter your password",
                        minlength: "Minimum 8 characters",
                    },
                },
                errorPlacement: function(error, element) {
                    // Place the error message below the input element
                    error.insertAfter(element);

                    // Add a class to the input when there is an error
                    element.addClass("error_effect");
                },
                success: function(label, element) {
                    // Remove the error class when the input is valid
                    $(element).removeClass("error_effect");
                }
            });
        });
    </script>
</body>

</html>