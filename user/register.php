<?php
session_start();
require 'include/head.php';

?>
<head>
<style>
        @import url('https://fonts.googleapis.com/css2?family=League+Spartan:wght@300;400;700&family=Open+Sans:wght@300;500;700&family=Oswald:wght@600&family=PT+Mono&family=Playfair+Display:wght@400;600;700&family=Roboto+Mono:wght@700&family=Roboto+Slab:wght@300;400;700&family=Roboto:wght@300;500;700&family=Slabo+27px&family=Source+Sans+3:wght@300&family=Ysabeau+SC:wght@300;600&display=swap');

        body {
            overflow-y: unset;
        }

        .login_wrapper {
            justify-content: center;
            width: 40%;
        }
    </style>
</head>
<body class="overflow-x-hidden ">
    <header class="w-full h-screen relative text-black flex flex-col items-center">
    <?php require 'include/navbar.php'?>
        <article class="lg:w-[40%] md:w-[70%] w-[80%] flex flex-col items-center justify-center gap-8">
            <h1 class="relative text-[2rem]">Create your Account</h1>
            <form action="../public/auth/register.inc.php" method="post" id="register" class="lg:w-full md:w-[80%]  w-[90%] flex flex-col items-center h-[70%] gap-2">
                <!-- Add the hidden token field -->

                <?php
                if (isset($resubmissionDetected)) {
                    echo "<p class='resubmit_false'></p>";
                }
                if (isset($_SESSION["fieldsNotEmpty"])) {
                    echo "<div class=' text-red-600 text-center pb-4'>" . $_SESSION["fieldsNotEmpty"] . "</div>";
                    unset($_SESSION["fieldsNotEmpty"]);
                }
                if (isset($_SESSION["userExists"])) {
                    echo "<div class=' text-red-600 text-center pb-4'>" . $_SESSION["userExists"] . "</div>";
                    unset($_SESSION["userExists"]);
                }
                ?>

                <label class="text-center lg:text-left lg:text-lg text-lg md:text-xl text-gray-400" for="username">Username</label>
                <input type="name" name="username" id="username" class="lg:w-[90%] w-full" placeholder="Make it fancy">
                <label class="text-center lg:text-left lg:text-lg text-lg md:text-xl text-gray-400" for="name">Full Name</label>
                <input type="text" name="name" id="name" class="lg:w-[90%] w-full" placeholder="John Doe">
                <label class="text-center lg:text-left lg:text-lg text-lg md:text-xl text-gray-400" for="email">Email Adress</label>
                <input type="email" name="email" id="email" class="lg:w-[90%] w-full" placeholder="name@example.com">
                <label class="text-center lg:text-left lg:text-lg text-lg md:text-xl text-gray-400" for="pass">Password</label>
                <input type="password" name="pass" id="pass" class="lg:w-[90%] w-full" placeholder="password">
                <label class="text-center lg:text-left lg:text-lg text-lg md:text-xl text-gray-400" for="pass">Confirm password</label>
                <input type="password" name="passConfirm" id="passConfirm" class="lg:w-[90%] w-full" placeholder="Confirm password">
                <input type="submit" value="Continue" id="registerSubmit" name="submit" class="w-full lg:w-[90%] py-3 cursor-pointer text-2xl bg-black text-white ">
                </form>

                <div class="mx-[0] my-8 md:w-[30rem] w-full md:pb-0 pb-8  flex flex-col items-center">
                    <a href="pre_register.php" class="inline-block lg:no-underline underline underline-offset-2 lg:text-[1.4rem] text-lg text-[rgb(19,_19,_19)] 
                    relative mb-6 tracking-[1px] lg:px-2 group">Choose Another Registration Method
                        <span class="lg:block hidden group-hover:scale-100 transform scale-0 absolute -bottom-0 left-0 
                        w-full h-px bg-[rgb(0,_0,_0)] duration-300"></span>
                    </a>
                    <p class="text-center text-[#2f2f2f]">
                        By creating an account, you agree to our Terms of Service and have read and understood the Privacy Policy
                    </p>
                </div>
        </article>
    </header>
    <script>
        $.validator.addMethod("alphabetsOnly", function(value, element) {
            return /^[a-zA-Z\s]+$/.test(value);
        }, "No numbers or special characters.");

        // Add validation rules and messages to your registration form
        $("#register").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    alphabetsOnly: true,
                },
                username: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                pass: {
                    minlength: 6,
                    required: true,
                },

                passConfirm: {
                    required: true,
                    equalTo: pass,
                },
            },
            messages: {
                name: {
                    required: "Please enter your name",
                    minlength: "Name should be longer than 2 characters",
                },
                username: {
                    required: "Please enter your username",
                },
                email: {
                    required: "Please enter your email",
                    email: "Enter a valid email address",
                },
                pass: {
                    required: "Please enter your password",
                    minlength: "Minimum 6 characters",
                },
                passConfirm: {
                    required: "Please confirm your password",
                    equalTo: 'Passwords dont match',
                },
            },
            errorPlacement: function(error, element) {
                // Place the error message below the input element
                error.insertAfter(element);

                // Add a class to the input when there is an error
                element.addClass("error_effect");
            },
            success: function(label, element) {
                // Remove the error class when the input is valid
                $(element).removeClass("error_effect");
            }
        });
    </script>
</body>

</html>