-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 26, 2023 at 09:18 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projektiblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `post_id` int DEFAULT NULL,
  `content` text COLLATE utf8mb4_general_ci,
  `media` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `content`, `media`, `created_at`) VALUES
(3, 86, 50, 'It looks a bit like this', '', '2023-12-20 11:43:51'),
(4, 86, 50, 'Look at dis ', '', '2023-12-20 11:45:47'),
(7, 86, 50, 'Dress ', 'blackvintageDress2.jpg', '2023-12-20 11:52:02'),
(9, 86, 48, 'Damn brother looking sick #trueArtist', '', '2023-12-20 12:37:31'),
(10, 86, 54, 'This should have it all #tags #text #undVideo', '2023-12-10 01-23-33.mp4', '2023-12-20 13:12:53'),
(11, 89, 54, 'Nice shit o nigga', '', '2023-12-20 13:34:55'),
(12, 89, 52, 'cool', '', '2023-12-20 13:45:48');

-- --------------------------------------------------------

--
-- Table structure for table `comments_tags`
--

CREATE TABLE `comments_tags` (
  `id` int NOT NULL,
  `comment_id` int NOT NULL,
  `tag_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `comments_tags`
--

INSERT INTO `comments_tags` (`id`, `comment_id`, `tag_id`) VALUES
(1, 8, 34),
(2, 10, 35),
(3, 10, 36),
(4, 10, 37);

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `follower_user_id` int DEFAULT NULL,
  `followed_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `follower_user_id`, `followed_at`) VALUES
(1, 86, 85, '2023-12-14 11:17:31'),
(46, 85, 86, '2023-12-15 23:51:25'),
(47, 84, 86, '2023-12-15 23:57:22'),
(48, 86, 87, '2023-12-16 00:00:28'),
(49, 86, 88, '2023-12-16 00:02:02'),
(88, 51, 89, '2023-12-17 20:48:07'),
(89, 85, 89, '2023-12-17 20:48:18'),
(90, 87, 89, '2023-12-17 20:48:31'),
(91, 83, 89, '2023-12-17 20:52:46'),
(92, 84, 89, '2023-12-17 20:53:31'),
(93, 88, 89, '2023-12-17 20:53:57'),
(94, 86, 89, '2023-12-17 20:56:56'),
(95, 83, 86, '2023-12-17 21:24:36'),
(100, 87, 86, '2023-12-19 20:26:58');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `post_id` int NOT NULL,
  `liked_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `post_id`, `liked_at`) VALUES
(13, 86, 48, '2023-12-19 13:32:46'),
(16, 86, 49, '2023-12-19 16:38:32'),
(22, 89, 54, '2023-12-20 13:48:37'),
(23, 89, 53, '2023-12-20 13:48:41');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `media` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edited_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `content`, `media`, `created_at`, `edited_at`) VALUES
(47, 86, 'Inserting a new post thread with 2 tags #firstTag #secondTag', '', '2023-12-18 16:56:43', NULL),
(48, 86, 'Hello im trying to be more artistic now and add an image #fooood', 'buka.jpeg', '2023-12-19 00:19:51', NULL),
(49, 85, 'Testinjoooo baba #onDeck', '', '2023-12-19 01:26:37', NULL),
(50, 85, 'Lets add another posts for example qetash ja zgatim tekstin pak ma shum me pa qysh po doket referat e bojna copi paste go go go new post incoming madafaka #oppaaa', '', '2023-12-19 11:39:42', NULL),
(52, 86, 'new post', 'banner.jpg', '2023-12-20 12:45:00', NULL),
(53, 86, '', 'IMG_0601.jpg', '2023-12-20 12:46:44', NULL),
(54, 86, '', '2023-12-15 20-10-53.mp4', '2023-12-20 13:09:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts_tags`
--

CREATE TABLE `posts_tags` (
  `id` int NOT NULL,
  `post_id` int NOT NULL,
  `tag_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `posts_tags`
--

INSERT INTO `posts_tags` (`id`, `post_id`, `tag_id`) VALUES
(31, 47, 29),
(32, 47, 30),
(33, 48, 31),
(34, 49, 32),
(35, 50, 33);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `full_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `bio` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `cover_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `website_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `is_setup` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `full_name`, `bio`, `created_at`, `cover_image`, `website_link`, `is_setup`) VALUES
(9, 83, 'Rini Misniiii', NULL, '2023-12-09 14:59:48', NULL, NULL, b'0'),
(10, 84, 'John Doe', NULL, '2023-12-09 15:00:38', NULL, NULL, b'0'),
(11, 85, 'Test Gashiiii', '', '2023-12-09 18:11:55', 'scenery.jpg', '', b'1'),
(12, 86, 'Rini Bossi', 'Gamer, foodie and a total grub for pizza', '2023-12-09 23:13:11', 'MainBanner_patekst.webp', 'https://twitter.com/rini_misini', b'1'),
(13, 87, 'Arben Hoda', NULL, '2023-12-15 23:58:43', NULL, NULL, b'0'),
(14, 88, 'Blerando Thiu', NULL, '2023-12-16 00:01:30', NULL, NULL, b'0'),
(15, 89, 'Alpella Bar', '', '2023-12-16 00:40:15', 'buka.jpeg', '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE `replies` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `comment_id` int DEFAULT NULL,
  `content` text COLLATE utf8mb4_general_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`) VALUES
(29, '#firstTag', '2023-12-18 16:56:43'),
(30, '#secondTag', '2023-12-18 16:56:43'),
(31, '#fooood', '2023-12-19 00:19:51'),
(32, '#onDeck', '2023-12-19 01:26:37'),
(33, '#oppaaa', '2023-12-19 11:39:42'),
(34, '#trueArtist', '2023-12-20 12:37:31'),
(35, '#tags', '2023-12-20 13:12:53'),
(36, '#text', '2023-12-20 13:12:53'),
(37, '#undVideo', '2023-12-20 13:12:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL,
  `surname` varchar(30) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `profile_picture_path` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role` int DEFAULT '0',
  `username` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `profile_picture_path`, `password`, `role`, `username`, `created_at`) VALUES
(51, 'adm', '', 'adm@gmail.com', 'profile_pic_Forme.JPG', '$2y$10$0IVkw1RRznQbkVw4qeHsEOzqmvjQNx.7pIrYN6TzueCVLHbmNqK9a', 2, 'adminrini', '2023-12-09 13:55:23'),
(83, 'Rini', 'Misniiii', 'KUMIkumi@gmail.com', 'blankProfileImage.webp', '$2y$10$SRY2bsrl6KFkQIvFxHxH3.F9PWBylplui4RVPzWOFhYG38tRScwLS', 0, 'rinimisni1222', '2023-12-09 14:59:48'),
(84, 'John', 'Doe', 'doeBaba@gmail.com', 'blankProfileImage.webp', '$2y$10$d5DVqAIu7YyGmlRKtv8kmewv7OG7HT62i0ZOALFLvGBK0E71tnaEG', 0, 'Riniu Msiinu', '2023-12-09 15:00:38'),
(85, 'Test', 'Gashiiii', 'testinjo@gmail.com', 'blankProfileImage.webp', '$2y$10$5de37JGXDGPQR.99Wg8c1uB9KXsj.mdGBVzMo5B1aoHID8T343VpK', 0, 'testinjoooooooooooo', '2023-12-09 18:11:55'),
(86, 'Rini', 'Bossi', 'bbaholli@gmail.com', 'Screen Shot 2023-12-12 at 19.48.46.png', '$2y$10$T96q.o6cPj9w9sP2NRbQve.MmA9C7FgUgr6BT1RmUUnpPEKTCLJNK', 0, 'riniumisini', '2023-12-09 23:13:11'),
(87, 'Arben', 'Hoda', 'arbenibeni@gmail.com', 'profiletest.jpg', '$2y$10$BGClb/nJN5u14UBV4HFzDOliG8n8SJgflmDXRLqIvtAfvxlaCgaNq', 0, 'beni6969', '2023-12-15 23:58:43'),
(88, 'Blerando', 'Thiu', 'blerando@gmail.com', 'blankProfileImage.webp', '$2y$10$S0X6P5Xug32iINBpun5CpOY7fmPK04BZCFAsXfoRKjGPHn.2WSbEO', 0, 'blerandoBBK', '2023-12-16 00:01:30'),
(89, 'Alpella', 'Bar', 'alpella@gmail.com', 'blankProfileImage.webp', '$2y$10$pF1GT5mu6kYI8xS17aVSP.FyE2ISAZ9Mpu/3o/sEPTLj/MUd9XQFG', 0, 'alpellaQoko', '2023-12-16 00:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `viewed_users`
--

CREATE TABLE `viewed_users` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `viewed_users_id` int NOT NULL,
  `viewed_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `viewed_users`
--

INSERT INTO `viewed_users` (`id`, `user_id`, `viewed_users_id`, `viewed_at`) VALUES
(23, 89, 86, '2023-12-20 13:46:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments_tags`
--
ALTER TABLE `comments_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`);

--
-- Indexes for table `posts_tags`
--
ALTER TABLE `posts_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `viewed_users`
--
ALTER TABLE `viewed_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `comments_tags`
--
ALTER TABLE `comments_tags`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `posts_tags`
--
ALTER TABLE `posts_tags`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `viewed_users`
--
ALTER TABLE `viewed_users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `fk_posts_user_id` FOREIGN KEY (`user_id`) REFERENCES `followers` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
