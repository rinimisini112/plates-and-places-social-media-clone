var container = $("#post-image-container");
var inputImage = $('input[name="image"]');
var inputVideo = $('input[name="video"]');
var imageIcon = $("#imageIcon");
var videoIcon = $("#videoIcon");
$('input[name="image"], input[name="video"]').change(function () {
  if (inputImage.val()) {
    // Image selected
    container.html(
      '<img src="' +
        URL.createObjectURL(inputImage[0].files[0]) +
        '" alt="Selected Image" class="w-full h-auto object-cover aspect-auto rounded-2xl shadow-lg"/>'
    );
    inputVideo.prop("disabled", true);
    videoIcon.addClass("opacity-50 cursor-not-allowed");
    videoIcon.removeClass("cursor-pointer");
    container.addClass("pb-1 pt-3");
    inputImage.prop("disabled", false);
    imageIcon.removeClass("opacity-50 cursor-not-allowed");
    imageIcon.addClass("cursor-pointer");
    appendCloseButton(container);
  } else if (inputVideo.val()) {
    // Video selected
    container.html(
      '<video controls class="w-full h-auto object-cover aspect-auto rounded-2xl shadow-lg"><source src="' +
        URL.createObjectURL(inputVideo[0].files[0]) +
        '" type="video/mp4"></video>'
    );
    inputImage.prop("disabled", true);
    imageIcon.addClass("opacity-50 cursor-not-allowed");
    imageIcon.removeClass("cursor-pointer");
    inputVideo.prop("disabled", false);
    videoIcon.removeClass("opacity-50 cursor-not-allowed");
    container.addClass("pb-1 pt-3");
    videoIcon.addClass("cursor-pointer");
    appendCloseButton(container);
  } else {
    // No file selected
    container.html("");
    inputImage.prop("disabled", false);
    imageIcon.removeClass("opacity-50 cursor-not-allowed");
    imageIcon.addClass("cursor-pointer");
    inputVideo.prop("disabled", false);
    videoIcon.removeClass("opacity-50 cursor-not-allowed");
    videoIcon.addClass("cursor-pointer");
  }
});

// Function to append the close button
function appendCloseButton(container) {
  container.append(
    '<svg id="closeImage" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle hover:scale-110 duration-200 cursor-pointer absolute right-4 top-6 "><circle stroke="none" fill="#00000073" cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>'
  );

  // Add click event listener to close button
  container.find("#closeImage").on("click", function () {
    container.html("");
    inputImage.prop("disabled", false);
    imageIcon.removeClass("opacity-50 cursor-not-allowed");
    inputVideo.prop("disabled", false);
    videoIcon.removeClass("opacity-50 cursor-not-allowed");
  });
}
$(document).ready(function () {
  function updateTextareaHeight() {
    var textarea = $("#expandingTextarea");
    textarea.css("height", "35px");
    textarea.height(textarea[0].scrollHeight);
  }

  $("#expandingTextarea").on("input", updateTextareaHeight);

  updateTextareaHeight();

  function updateReplyTextareaHeight() {
    var textarea = $("#replyTextarea");
    textarea.css("height", "30px");
    textarea.height(textarea[0].scrollHeight);
  }

  $("#replyTextarea").on("input", updateReplyTextareaHeight);

  updateReplyTextareaHeight();
});
$(document).ready(function () {
  $(".like-button").on("click", function () {
    var postId = $(this).data("post-id");
    var likeCountElement = $(this).siblings(".like-count");

    // Store reference to 'this' for later use
    var clickedButton = $(this);

    $.ajax({
      type: "POST",
      url: "../public/user_profile/like_post.php",
      data: {
        postId: postId,
      },
      success: function (response) {
        // Update like count
        likeCountElement.text(response.likeCount);

        // Toggle fill color based on like status
        var isLiked = response.isLiked;
        var fillColor = isLiked ? "red" : "none";
        var strokeColor = isLiked ? "red" : "currentColor";

        // Use the stored reference to 'this'
        clickedButton.toggleClass("liked", isLiked);
        clickedButton.find("#like-icon").css("fill", fillColor);
        clickedButton.find("#like-icon").css("stroke", strokeColor);
      },
      error: function (error) {
        console.log("Ajax request failed:", error);
      },
    });
  });
});
$(document).ready(function () {
  // Wait for the document to be ready
  setTimeout(function () {
    $("#notification").fadeOut("slow"); // Hide the element with a fade-out effect
  }, 4000); // 5000 milliseconds = 5 seconds
});
