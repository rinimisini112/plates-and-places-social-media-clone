document.addEventListener("DOMContentLoaded", function () {
  const searchToggle = document.getElementById("searchToggle");
  const searchSidebar = document.getElementById("searchSidebar");
  const brandTitle = document.getElementById("brandTitle");
  const brandLogo = document.getElementById("brandLogo");

  searchToggle.addEventListener("click", function () {
    const isOpen = parseFloat(searchSidebar.style.maxWidth) > 0;

    gsap.to(searchSidebar, {
      maxWidth: isOpen ? 0 : "25rem",
      borderInline: isOpen ? "none" : "1px solid #2D2D2D",
      duration: 0.2,
      onComplete: () => {
        // Toggle visibility of brand title and logo
        brandTitle.style.display = isOpen ? "block" : "none";
        brandLogo.style.display = isOpen ? "none" : "block";
      },
    });
  });

  // Click event for the document to close the search sidebar
  document.addEventListener("click", function (event) {
    if (
      !searchSidebar.contains(event.target) &&
      event.target !== searchToggle
    ) {
      gsap.to(searchSidebar, {
        maxWidth: 0,
        border: "none",
        duration: 0.2,
        onComplete: () => {
          // Toggle visibility of brand title and logo
          brandTitle.style.display = "block";
          brandLogo.style.display = "none";
        },
      });
    }
  });
});
document.addEventListener("DOMContentLoaded", function () {
  const liElement = document.getElementById("toggleMore");
  const menuElement = document.getElementById("settingsBlock");
  const lineOne = document.getElementById("l-one");
  const lineTwo = document.getElementById("l-two");
  const lineThree = document.getElementById("l-three");

  function closeSettingsMenu() {
    gsap.to(menuElement, {
      display: "none",
      opacity: 0,
      duration: 0.2,
    });
    gsap.to(lineTwo, {
      rotation: 0,
      duration: 0.2,
    });

    gsap.to(lineOne, {
      x: 0,
      duration: 0.2,
    });
    gsap.to(lineThree, {
      rotation: 0,
      duration: 0.2,
    });
    liElement.classList.remove("bg-dark-active");
  }
  liElement.addEventListener("click", function () {
    // Toggle the menu visibility
    if (window.getComputedStyle(menuElement).display === "none") {
      gsap.to(menuElement, {
        display: "block",
        opacity: 1,
        duration: 0.2,
      });
      gsap.to(lineTwo, {
        rotation: 45,
        duration: 0.2,
      });

      gsap.to(lineOne, {
        x: 100,
        duration: 0.2,
      });
      gsap.to(lineThree, {
        rotation: -45,
        duration: 0.2,
      });
      liElement.classList.add("bg-dark-active");
    } else {
      closeSettingsMenu();
    }
  });

  // Click event for the document to close the menu
  document.addEventListener("click", function (event) {
    if (
      !liElement.contains(event.target) &&
      !menuElement.contains(event.target)
    ) {
      closeSettingsMenu();
    }
  });
});

$(document).ready(function () {
  let searchTimeout;
  const loadingSpinner =
    "<div class='w-full py-4 text-2xl gap-2 flex flex-col items-center justify-center'><img class='animate-spin w-[40px] h-[40px]' src='../resources/images/tube-spinner.png' alt=''/><p>Loading<span class='loading-dots'></span></p></div>";
  const resultBox = $("#resultBox");

  function showLoadingState() {
    resultBox.html(loadingSpinner);
  }

  function hideLoadingState() {
    resultBox.html("");
  }

  function updateResults(results) {
    resultBox.empty();

    if (results.length === 0) {
      resultBox.append(
        '<div class="w-full py-3 text-2xl text-center">Nothing Found</div>'
      );
    } else {
      results.forEach(function (result) {
        var resultItem =
          '<div class="w-full px-3 hover:bg-dark-active py-1.5 duration-200">';
        resultItem +=
          '<a href="account.php?user=' +
          result.username +
          '&searched" class="flex items-center gap-3">';

        if (result.image && result.image.trim() !== "") {
          resultItem +=
            '<img src="../resources/images/' +
            result.image +
            '" alt="" class="w-[50px] h-[50px] rounded-full object-cover">';
        } else {
          resultItem +=
            '<img src="../resources/images/blankProfileImage.webp" alt="" class="w-[50px] h-[50px] rounded-full object-cover">';
        }

        resultItem += "<div>";
        resultItem +=
          '<p class="text-neutral-400">@' + result.username + "</p>";
        resultItem +=
          '<p class="-translate-y-1 pl-3">' + result.fullName + "</p>";
        resultItem += "</div></a></div>";

        resultBox.append(resultItem);
      });
    }
  }

  $("#clearAllButton").on("click", function (event) {
    event.preventDefault();

    showLoadingState();

    $.ajax({
      url: "../public/user_profile/get_search_history.php",
      method: "GET",
      data: { clearAll: true },
      dataType: "json",
      success: function (response) {
        hideLoadingState();

        if (response.success) {
          // Clear the content in real-time
          resultBox.html(response.content);
        } else {
          console.error("Error clearing searches:", response.message);
          // Handle error if needed
        }
      },
      error: function () {
        hideLoadingState();
        console.error("Error clearing searches");
        // Handle error if needed
      },
    });
  });

  // Initial AJAX request to fetch PHP-generated content
  showLoadingState();
  $.ajax({
    url: "../public/user_profile/get_search_history.php",
    method: "GET",
    dataType: "json",
    success: function (response) {
      hideLoadingState();
      resultBox.html(response.content);
    },
    error: function () {
      hideLoadingState();
      resultBox.html(
        '<div class="w-full px-3 py-1.5 text-xl">Error Loading Results</div>'
      );
    },
  });

  // Search input event handling
  $("#search").on("input", function () {
    var query = $(this).val();

    clearTimeout(searchTimeout);

    if (query.trim() === "") {
      // Show PHP-generated search history when the search query is empty
      showLoadingState();
      $.ajax({
        url: "../public/user_profile/get_search_history.php",
        method: "GET",
        dataType: "json",
        success: function (response) {
          hideLoadingState();
          resultBox.html(response.content);
        },
        error: function () {
          hideLoadingState();
          resultBox.html(
            '<div class="w-full px-3 py-1.5 text-xl">Error Loading Results</div>'
          );
        },
      });
      return;
    }

    // Show loading state immediately
    showLoadingState();

    // Set a timeout for AJAX request
    searchTimeout = setTimeout(function () {
      $.ajax({
        url: "../public/user_profile/search.php",
        method: "GET",
        data: { query: query },
        dataType: "json",
        success: function (results) {
          clearTimeout(searchTimeout);
          hideLoadingState();
          updateResults(results);
        },
        error: function () {
          clearTimeout(searchTimeout);
          hideLoadingState();
          resultBox.html(
            '<div class="w-full px-3 py-1.5 text-xl">Error Loading Results</div>'
          );
        },
      });
    }, 600); // 2 seconds timeout
  });
  $("#clearAllButton").on("click", function (event) {
    event.preventDefault(); // Prevent the default behavior of the anchor tag

    // Send an asynchronous request to get_search_history.php to clear all searches
    $.ajax({
      url: "../public/user_profile/get_search_history.php",
      method: "GET",
      data: { clearAll: true }, // Add a parameter to indicate clearing all searches
      dataType: "json",
      success: function (response) {
        // Handle success (if needed)
        console.log("All searches cleared successfully");
        // Optionally, you can update the resultBox with the cleared content
        resultBox.html(response.content);
      },
      error: function () {
        // Handle error (if needed)
        console.error("Error clearing searches");
      },
    });
  });
});
