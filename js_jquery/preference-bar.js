function checkPreferenceBarMessage() {
  $.ajax({
    url: "../public/user_profile/preferenceBarMessage.php",
    method: "GET",
    dataType: "json",
    success: function (response) {
      if (response.success && response.message) {
        $("#prefBarNotif").text(response.message);
        setTimeout(function () {
          $("#prefBarNotif").text("Who to follow");
        }, 5000);
      }
    },
    error: function () {
      console.error("Failed to check preference bar message");
    },
  });
}

$(document).ready(function () {
  function loadWhoToFollowContent() {
    $.ajax({
      url: "../public/user_profile/preferenceBarFollow.php",
      method: "GET",
      dataType: "json",
      success: function (response) {
        if (response.success) {
          $("#whoToFollowContainer").html("");
          if (response.userContent.length === 0) {
            $("#whoToFollowContainer").append(
              '<div class="w-full py-3 text-2xl text-center text-adm-lgrey border-b border-b-neutral-700">You followed them all!</div>'
            );
          } else {
            response.userContent.forEach(function (userContent) {
              var truncatedUsername = userContent.truncatedUsername;
              var userId = userContent.userId;
              var profilePicture = userContent.profilePicture;
              var username = userContent.username;

              var userHtml =
                '<div class="border-b border-b-neutral-700 pb-3 pt-2 text-lg hover:bg-dark-active duration-200">';
              userHtml +=
                '<div class="flex items-center justify-between gap-2 py-2 px-4 w-full">';
              userHtml +=
                '<a href="account.php?user=' +
                username +
                '" class="flex group items-center gap-2">';
              userHtml +=
                '<img src="../resources/images/' +
                profilePicture +
                '" class="w-[40px] aspect-square h-[40px] object-cover rounded-full" alt="">';
              userHtml +=
                '<p class="text-adm-lgrey">@<span class="font-bold transform group-hover:underline group-hover:underline-offset-4 duration-200 -translate-x-3">' +
                truncatedUsername +
                "</span></p>";
              userHtml += "</a>";
              userHtml +=
                '<form action="../public/user_profile/follow.php" method="post">';
              userHtml +=
                '<input type="submit" name="followFromPrefBar" value="Follow" data-user-id="' +
                userId +
                '" class="py-1 text-lg rounded-3xl px-6 border hover:bg-dark-active border-adm-lgrey bg-dark duration-200 cursor-pointer text-adm-lgrey follow-button">';
              userHtml += "</form>";
              userHtml += "</div>";
              userHtml += "</div>";

              $("#whoToFollowContainer").append(userHtml);
            });
          }
        } else {
          console.error("Follow request failed:", response.message);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.error("AJAX request failed:", textStatus, errorThrown);
      },
    });
  }

  loadWhoToFollowContent();

  $(document).on("click", ".follow-button", function (e) {
    e.preventDefault();
    var userId = $(this).data("user-id");

    $.ajax({
      url: "../public/user_profile/preferenceBarFollow.php",
      method: "POST",
      data: {
        userId: userId,
      },
      dataType: "json",
      success: function (response) {
        if (response.success) {
          loadWhoToFollowContent();
          checkPreferenceBarMessage();
        } else {
          console.error("Follow request failed");
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.error("AJAX request failed:", textStatus, errorThrown);
      },
    });
  });
});
