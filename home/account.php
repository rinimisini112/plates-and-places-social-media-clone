<?php
date_default_timezone_set('Europe/Skopje');
require '../vendor/autoload.php';

session_start();

use ProjektiBlog\public\classes\User;
use ProjektiBlog\public\classes\Profile;
use ProjektiBlog\public\classes\Follower;
use ProjektiBlog\public\classes\TimeFormatter;
use ProjektiBlog\public\classes\ViewedUser;

include "include/head.php";

$user = new User();
$profile = new Profile();
$you = new User();
$you = $user->fetchWithId($_SESSION['userid']);

if (isset($_GET['user'])) {
    $user->setUsername($_GET['user']);
    $user = $user->fetchUserWithUsername();
    $profile = $profile->findProfileWithUserId($user->getId());

    if (isset($_GET['searched'])) {
        $searched_user = new ViewedUser();
        $searched_user->setUserId($you->getId());
        $searched_user->setViewedUsersId($user->getId());
        $searched_user->setSearchHistory();
    }
}
$followers = new Follower();
$followers->setUserId($user->getId());
$followers->setFollowerUserId($_SESSION['userid']);
if (isset($_SESSION['followed'])) {
    $follower = $followers->getFollowedAtTime();
    $time = $follower->getFollowedAt();

    $time_ago = TimeFormatter::formatTimeAgo($time);
}
?>

<body class="bg-img">
    <main class="w-full relative">
        <?php if (isset($_SESSION['follow_message'])) : ?>
            <div id="notification" class=" fixed bottom-6 p-4 right-6 w-[30%] flex flex-col justify-between rounded-xl h-[130px] bg-dark-active bg-opacity-75 backdrop-blur-sm shadow-xl text-adm-white">
                <div class="flex gap-4 items-center">
                    <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                    <p class="text-3xl">Hi <?= $you->getName() ?>!</p>
                </div>
                <p class="flex items-center justify-between">
                    <?php
                    echo $_SESSION['follow_message'];
                    if (isset($_SESSION['followed'])) {
                        echo "<span class='underline'>" . $time_ago . "</span>";
                        unset($_SESSION['followed']);
                    }
                    ?>
                </p>
            </div>
        <?php
            unset($_SESSION['follow_message']);
        endif;
        require 'include/sidenav.php'; ?>
        <div class="w-4/5 ml-auto flex">
            <div class="w-[65%]">
                <div class="w-full">
                    <div class="flex text-adm-white px-4 py-2 justify-between items-center sticky top-0 bg-[#2023299d] backdrop-blur-md z-10 ">
                        <div class="flex gap-8">
                            <a href="profile.php" onclick="javascript:history.back();" class=" rounded-full w-[36px] h-[36px] hover:bg-dark-active duration-300 flex items-center justify-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left">
                                    <line x1="19" y1="12" x2="5" y2="12"></line>
                                    <polyline points="12 19 5 12 12 5"></polyline>
                                </svg>
                            </a>
                            <h1 class="text-2xl flex flex-col">Profile
                                <p class="text-lg -mt-2 text-neutral-500">1 post</p>
                            </h1>
                        </div>
                        <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                    </div>
                    <div class="bg-dark-active h-[250px]">
                        <?php if (!empty($profile->getCoverImage()) && $profile->getCoverImage() !== '') : ?>
                            <img src="../resources/images/<?= $profile->getCoverImage() ?>" alt="" class="max-w-full max-h-[250px] w-full h-full object-cover">
                        <?php endif ?>
                    </div>
                    <div>
                        <div class="flex items-start pl-12 pr-4 justify-between">
                            <?php if (!empty($user->getProfilePicture())) : ?>
                                <img src="../resources/images/<?= $user->getProfilePicture() ?>" alt="" class="rounded-full transform -translate-y-1/2 border-4 border-darker w-[170px] h-[170px] max-w-[170px] max-h-[170px] object-cover">
                            <?php else : ?>
                                <img src="../resources/images/blankProfileImage.webp" alt="" class="rounded-full transform -translate-y-1/2 border-4 border-darker w-[170px] h-[170px] max-w-[170px] max-h-[170px] object-cover">
                            <?php endif;
                            $isFollowing = $followers->youFollowUser($user->getId());
                            $followText = $isFollowing ? 'Unfollow' : 'Follow';
                            $followStyle = $isFollowing ? 'bg-adm-lgrey text-darker' : 'text-adm-lgrey hover:bg-dark-active';
                            ?>
                            <form action="../public/user_profile/follow.php" method="post">
                                <input type='submit' name="follow" class="mt-4 cursor-pointer rounded-3xl border border-adm-lgrey <?= $followStyle ?> px-12 py-2 
                            duration-200 follow-btn" value='<?php echo $followText; ?>'>
                                <input type="hidden" name="userid" value="<?= $user->getId(); ?>">
                                <input type="hidden" name='action' value="<?= $isFollowing ? 'unfollow' : 'follow'; ?>">
                                <input type="hidden" name='username' value="<?= $user->getUsername() ?>">
                            </form>
                        </div>
                        <div class="text-adm-lgrey text-lg transform pt-4 -translate-y-1/2 pl-16">
                            <p class="text-2xl font-bold"><?= $user->getName() . ' ' . $user->getSurname() ?></p>
                            <p class=" -mt-1 text-neutral-400 text-lg">@<span><?= $user->getUsername() ?></span></p>
                            <?php if (!empty($profile->getBio())) : ?>
                                <p style="font-family: 'segoeUI';" class="my-3"><?= $profile->getBio() ?></p>
                            <?php else : ?>
                                <p style="font-family: 'segoeUI';" class="my-3">Looks like this is not set yet.</p>
                            <?php endif ?>
                            <div style="font-family: 'segoeUI';" class="flex gap-3">
                                <?php if (!empty($profile->getWebsiteLink())) : ?>
                                    <a target="_blank" href="<?= $profile->getWebsiteLink() ?>" class=" text-blue-600 underline underline-offset-2"><?= $profile->getWebsiteLink() ?></a>
                                <?php endif ?>
                                <?php if (!empty($profile->getCreatedAt())) : ?>
                                    <p>• Joined <?= $profile->formatCreatedAt() ?></p>
                                <?php endif ?>
                            </div>
                            <div style="font-family: 'segoeUI';" class="mt-3 flex gap-6 items-center">
                                <?php
                                $followers->setUserId($user->getId());
                                $followers->setFollowerUserId($user->getId());
                                $followers_count = $followers->getFollowersCount();
                                $following_count = $followers->getFollowingCount();
                                if (!$followers_count) {
                                    $followers_count = 0;
                                } else {
                                    $followers_count = $followers_count;
                                }

                                if (!$following_count) {
                                    $following_count = 0;
                                } else {
                                    $following_count = $following_count;
                                }
                                ?>

                                <a href="acc.php?user=<?= $user->getUsername() ?>&following" class="text-neutral-400 hover:underline">
                                    <span class="text-adm-lgrey font-bold"><?= $following_count ?></span> Following
                                </a>

                                <a href="acc.php?user=<?= $user->getUsername() ?>&followers" class="text-neutral-400 hover:underline">
                                    <span class="text-adm-lgrey font-bold"><?= $followers_count ?></span> Followers
                                </a>
                            </div>
                        </div>
                        <ul class="flex items-center justify-between w-full transform -translate-y-full border-b border-b-neutral-700 text-xl text-adm-lgrey">
                            <a href="" class=" inline-block w-[33.33%]">
                                <li class="hover:bg-dark-active duration-300 py-4 text-center">
                                    <span class="relative">Posts
                                        <span class="absolute -bottom-[1.30rem] rounded-3xl -left-0.5 w-full h-[4px] bg-adm-active"></span>
                                    </span>
                                </li>
                            </a>
                            <a href="" class=" inline-block w-[33.33%]">
                                <li class="hover:bg-dark-active duration-300 py-4 text-center">
                                    <span class="relative">Media
                                    </span>
                                </li>
                            </a>
                            <a href="" class=" inline-block w-[33.33%]">
                                <li class="hover:bg-dark-active duration-300 py-4 text-center">
                                    <span class="relative">Likes
                                    </span>
                                </li>
                            </a>
                        </ul>
                    </div>
                </div>
                </section>
            </div>
            <?php
            require 'include/preference_bar.php';
            ?>
        </div>
    </main>

    <script>
        // Function to remove a specific query parameter from the URL
        function removeQueryParam(paramKey) {
            var url = new URL(window.location.href);
            url.searchParams.delete(paramKey);
            window.history.replaceState({}, document.title, url.href);
        }

        // Call the function after a delay of 2000 milliseconds (2 seconds)
        setTimeout(function() {
            // Specify the query parameter to remove, e.g., 'searched'
            removeQueryParam('searched');
        }, 2000);
        $(document).ready(function() {
            // Wait for the document to be ready
            setTimeout(function() {
                $('#notification').fadeOut('slow'); // Hide the element with a fade-out effect
            }, 4500); // 5000 milliseconds = 5 seconds
        });
        let phoneMenu = document.getElementById('phoneMenu');
        let isOpen = false;

        function togglePhoneMenu() {
            if (!isOpen) {
                // Using GSAP to animate the opening of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: '100%', // This is equivalent to max-w-auto
                    onComplete: () => {
                        phoneMenu.classList.add('duration-200');
                    },
                });
            } else {
                // Using GSAP to animate the closing of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: 0, // This is equivalent to max-w-0
                    onComplete: () => {
                        phoneMenu.classList.remove('duration-200');
                    },
                });
            }

            isOpen = !isOpen;
        }

        let phoneMenuIcon = document.getElementById('openPhoneMenuIcon');
        phoneMenuIcon.addEventListener('click', togglePhoneMenu);
        let open = false;

        function toggleMenu() {
            let lines = document.querySelectorAll('.burger_line');

            gsap.to(lines[0], {
                duration: 0.05,
                rotate: open ? 0 : 45
            });
            gsap.to(lines[1], {
                duration: 0.2,
                x: open ? 0 : 45,
                opacity: open ? 1 : 5
            });
            gsap.to(lines[2], {
                duration: 0.05,
                rotate: open ? 0 : -45
            });
            open = !open;
        }
        window.onclick = function() {
            let button = document.getElementById('openPhoneMenuIcon');
            if (!button.contains(event.target) && open) {
                togglePhoneMenu();
                toggleMenu();
            }
        }
    </script>
</body>

</html>