<?php
require '../vendor/autoload.php';

use ProjektiBlog\public\classes\Comment;
use ProjektiBlog\public\classes\Like;
use ProjektiBlog\public\classes\Post;
use ProjektiBlog\public\classes\User;
use ProjektiBlog\public\classes\Likes;
use ProjektiBlog\public\classes\Follower;
use ProjektiBlog\public\classes\TimeFormatter;
use ProjektiBlog\public\classes\SessionManager;

$session = new SessionManager();

include "include/head.php";

$user = new User();
$user = $user->fetchWithId($_SESSION['userid']);
if (isset($_GET['id'])) {
    $postId = $_GET['id'];
} else {
    header('Location: index.php');
}
?>

<body class="bg-img">

    <main class="w-full">
        <?php if ($session->message()) : ?>
            <div id="notification" class=" fixed bottom-6 p-4 z-50 right-6 w-[30%] flex flex-col justify-between rounded-xl h-[130px] bg-dark-active bg-opacity-75 backdrop-blur-sm shadow-xl text-adm-white">
                <div class="flex gap-4 items-center">
                    <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                    <p class="text-3xl">Hi <?= $user->getName() ?>!</p>
                </div>
                <p class=""><?= $session->message() ?></p>
            </div>
        <?php
        endif;
        require 'include/sidenav.php';
        ?>
        <div class="w-4/5 ml-auto flex">
            <div class="w-[65%]">
                <div class="flex border-b w-full sticky top-0 z-30 backdrop-blur-lg border-b-neutral-700 text-adm-white px-4 py-3 justify-between items-center">
                    <div class="flex items-center gap-5">
                        <a href="Back" onclick="javascript:history.back();" class=" rounded-full w-[36px] h-[36px] hover:bg-dark-active duration-300 flex items-center justify-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left">
                                <line x1="19" y1="12" x2="5" y2="12"></line>
                                <polyline points="12 19 5 12 12 5"></polyline>
                            </svg>
                        </a>
                        <h1 class="text-2xl ">Post</h1>
                    </div>
                    <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                </div>
                <section id="main-section" class="w-full">
                    <?php
                    $post = new Post();
                    $post = $post->fetchWithId($postId);

                    $published_at = $post->getCreatedAt();
                    $dateTime = new DateTime($published_at);
                    $formattedTime = $dateTime->format("g:i A · M j, Y");

                    $like = new Like();
                    $like->setPostId($post->getId());
                    $like->setUserId($_SESSION['userid']);
                    if ($like->isLikedByUser()) {
                        $fill = 'red';
                        $stroke = 'red';
                    } else {
                        $fill = 'none';
                        $stroke = 'currentColor';
                    }
                    $like_count = $like->getLikeCountForPost();
                    $content = $post->getContent();

                    $post_author = new User();
                    $post_author = $post_author->fetchWithId($post->getUserId());
                    preg_match_all('/#\w+/', $content, $matches);

                    $hashtags = $matches[0];
                    foreach ($hashtags as $tag) {
                        $anchorTag = '<a href="hashtag.php?tag=' . $tag . '" class="text-blue-500 duration-200 hover:underline hover:underline-offset-2">' . $tag . '</a>';
                        $content = str_replace($tag, $anchorTag, $content);
                    }
                    if ($post_author->getId() === $_SESSION['userid']) {
                        $direct_url = 'profile.php';
                    } else {
                        $direct_url = 'account.php?user=' . $post_author->getUsername();
                    }
                    ?>
                    <div class="flex gap-4 items-start pt-4 backdrop-blur-sm px-4 pb-4 ">
                        <a href="<?= $direct_url ?>" class="group">
                            <div class="w-[50px] h-[50px] shrink-0">
                                <img src="../resources/images/<?= $post_author->getProfilePicture() ?>" alt="" class="w-full h-full aspect-square object-cover rounded-full group-hover:opacity-80 duration-200">
                            </div>
                            <div class="text-white text-lg w-full">
                                <div>
                                    <p class=" text-neutral-400"><strong class="text-white  open-sans"><?= $post_author->getName() ?></strong> <span class="group-hover:text-blue-500 duration-200 ">@<?= $post_author->getUsername() ?></span></p>
                                </div>
                        </a>
                        <div class="py-3 pr-4 ">
                            <p class=" leading-snug">
                                <?= $content ?>
                            </p>
                            <?php if (!empty($post->getMedia())) : ?>
                                <?php
                                $mediaPath = "../resources/images/" . $post->getMedia();
                                $mediaType = pathinfo($mediaPath, PATHINFO_EXTENSION);
                                ?>

                                <div class="w-full my-3 rounded-3xl overflow-hidden">
                                    <?php if (in_array($mediaType, ['jpg', 'jpeg', 'png', 'gif'])) : ?>
                                        <img src="<?= $mediaPath ?>" alt="" class="w-full  aspect-auto object-cover">
                                    <?php elseif (in_array($mediaType, ['mp4', 'webm', 'ogg'])) : ?>
                                        <video controls class="w-full aspect-video object-cover">
                                            <source src="<?= $mediaPath ?>" type="video/<?= $mediaType ?>">
                                            Your browser does not support the video tag.
                                        </video>
                                    <?php else : ?>
                                        <p>Unsupported media type</p>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div>
                            <p>Posted • <?= $formattedTime ?></p>
                        </div>
                    </div>
            </div>
            <div class="px-4">
                <div class="w-full border-y border-y-neutral-700 text-white flex justify-between items-center px-16 py-1">
                    <span class="flex items-center gap-2">
                        <div class='like-button hover:bg-red-400 hover:bg-opacity-20 rounded-full hover:shadow-round hover:shadow-red-500 cursor-pointer duration-200 w-[35px] flex items-center justify-center h-[35px]' data-post-id="<?= $post->getId() ?>">
                            <svg id="like-icon" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24" fill="<?= $fill ?>" stroke="<?= $stroke ?>" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather duration-300 rounded-xl feather-heart">
                                <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
                            </svg>
                        </div>
                        <span class="like-count"><?= $like_count ?></span>
                    </span>

                    <span class="flex items-center gap-2 ">
                        <div class='hover:bg-blue-400 relative group hover:bg-opacity-20 rounded-full hover:shadow-round hover:shadow-blue-500 cursor-pointer  duration-200 w-[35px] flex items-center justify-center h-[35px]'>
                            <span class=" absolute top-10  delay-300 -left-10 w-[7.8rem] text-center py-0.5 rounded-xl bg-[#cacaca47] group-hover:block hidden text-white  duration-200">Add a comment</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle">
                                <path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path>
                            </svg>
                        </div>
                        0
                    </span>
                    <span>
                        <div class='hover:bg-amber-400 hover:bg-opacity-20 rounded-full hover:shadow-round hover:shadow-amber-500 cursor-pointer relative group  duration-200 w-[35px] flex items-center justify-center h-[35px]'>
                            <span class=" absolute top-10  delay-300 -left-10 w-[7rem] text-center py-0.5 rounded-xl bg-[#cacaca47] group-hover:block hidden text-white  duration-200">Toggle more</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-align-right">
                                <line x1="21" y1="10" x2="7" y2="10"></line>
                                <line x1="21" y1="6" x2="3" y2="6"></line>
                                <line x1="21" y1="14" x2="3" y2="14"></line>
                                <line x1="21" y1="18" x2="7" y2="18"></line>
                            </svg>
                        </div>
                    </span>
                </div>
            </div>
            <div class=" w-full text-adm-lgrey px-4 flex py-2">
                <div class="w-[10%] flex items-center">
                    <img src="../resources/images/<?= $user->getProfilePicture() ?>" class="w-[50px] aspect-square object-cover rounded-full" alt="">
                </div>
                <div class="w-[90%]">
                    <p class="text-lg text-neutral-400">Replying to <a href="account.php?user=<?= $post_author->getUsername() ?>" class="text-blue-500 hover:underline hover:underline-offset-2 hover:decoration-blue-500 duration-200">@<?= $post_author->getUsername() ?></a></p>
                    <form action="../public/user_profile/add_comment.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="postId" class="opacity-0" value="<?= $postId ?>">
                        <div class="relative mt-3 mb-1">
                            <textarea id="expandingTextarea" name="content" spellcheck="false" type="text" placeholder="Write your reply" class="peer text-adm-lgrey outline-none text-xl w-full max-w-full break-words mt-2 px-2 resize-none bg-darker border-b-2 border-b-neutral-700 focus:border-b-adm-lgrey"></textarea>
                        </div>
                        <div id="post-image-container" class=" w-full relative peer-focus-within:mt-6"></div>
                        <div class="flex justify-between pt-2 pb-1 sticky bottom-0 bg-darker">
                            <div class="flex gap-3">
                                <label for="image" class="relative">
                                    <input id="image" name="image" type="file" class="hidden" accept="image/*" />
                                    <svg id="imageIcon" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-image cursor-pointer hover:scale-110 hover:stroke-adm-active duration-200">
                                        <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
                                        <circle cx="8.5" cy="8.5" r="1.5"></circle>
                                        <polyline points="21 15 16 10 5 21"></polyline>
                                    </svg>
                                </label>
                                <label for="video">
                                    <input id="video" name="video" type="file" class="hidden" accept="video/*" />
                                    <svg id='videoIcon' xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-video cursor-pointer hover:scale-110 hover:stroke-adm-active duration-200">
                                        <polygon points="23 7 16 12 23 17 23 7"></polygon>
                                        <rect x="1" y="5" width="15" height="14" rx="2" ry="2"></rect>
                                    </svg>
                                </label>
                            </div>
                            <div>
                                <input type="submit" value="Reply" name="add_comment" class="relative disabled:opacity-40 cursor-pointer disabled:cursor-not-allowed disabled:hover:bg-adm-lgrey group py-1 text-dark text-lg rounded-3xl px-8 bg-adm-lgrey hover:bg-adm-active duration-200">
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="comment-section" class="w-full border-t border-t-neutral-700">
            <?php
            $comment = new Comment();
            $comment->setPostId($postId);
            $comments = $comment->fetchCommentsForPost();
            $counter = 0;
            foreach ($comments as $c) :
                $commenting_user = new User();
                $commenting_user =  $commenting_user->fetchWithId($c->getUserId());
                $commented_at = $c->getCreatedAt();
                $formated_comment_time = TimeFormatter::formatRoundedTimeAgo($commented_at);
                if ($commenting_user->getId() === $_SESSION['userid']) {
                    $url = 'profile.php';
                } else {
                    $url = 'account.php?user=' . $commenting_user->getUsername();
                }
            ?>
                <div class="flex px-4 py-1.5 border-b border-b-neutral-700">
                    <div class="w-[10%] flex items-start pt-3">
                        <img src="../resources/images/<?= $commenting_user->getProfilePicture() ?>" class="fancybox w-[50px] aspect-square object-cover rounded-full" alt="">
                    </div>
                    <div class="w-[90%] text-lg text-adm-lgrey">
                        <p class=""><strong class="open-sans"><?= $commenting_user->getName() ?></strong> <a class=" text-neutral-400 hover:text-blue-500 duration-200" href="<?= $url ?>">@<?= $commenting_user->getUsername() ?></a><span class="text-neutral-400"> • <?= $formated_comment_time ?></span></p>
                        <p class="font-normal"><?= $c->getContent() ?></p>
                        <?php if (!empty($c->getMedia())) : ?>
                            <?php
                            $mediaPath = "../resources/images/" . $c->getMedia();
                            $mediaType = pathinfo($mediaPath, PATHINFO_EXTENSION);
                            ?>
                            <div class="w-full mt-3">
                                <?php if (in_array($mediaType, ['jpg', 'jpeg', 'png', 'gif'])) : ?>
                                    <img src="<?= $mediaPath ?>" alt="comments image" class="w-full fancybox aspect-auto object-cover rounded-xl">
                                <?php elseif (in_array($mediaType, ['mp4', 'webm', 'ogg'])) : ?>
                                    <video controls class="w-full aspect-video object-cover rounded-xl">
                                        <source src="<?= $mediaPath ?>" type="video/<?= $mediaType ?>">
                                        Your browser does not support the video tag.
                                    </video>
                                <?php else : ?>
                                    <p>Unsupported media type</p>
                                <?php endif; ?>
                            </div>
                        <?php endif;
                        $counter++
                        ?>
                        <div class="w-full flex justify-end items-center pr-3 ">
                            <a href="comment.php?post_nr=<?= $postId ?>&comment_nr=<?= $c->getId() ?>" class="w-[40px] h-[40px] rounded-full relative group hover:bg-adm-active hover:bg-opacity-25 hover:shadow-round hover:shadow-adm-active flex items-center justify-center cursor-pointer">
                                <span class="absolute top-11 rounded-xl hidden group-hover:block delay-500 -left-8 w-[7rem] text-center text-sm py-1 bg-neutral-400 bg-opacity-40">Reply to comment</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-up-left group-hover:stroke-adm-active group-hover:scale-110 duration-200">
                                    <polyline points="9 14 4 9 9 4"></polyline>
                                    <path d="M20 20v-7a4 4 0 0 0-4-4H4"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        </section>
        </div>
        <?php
        require 'include/preference_bar.php';
        ?>
        </div>
    </main>
    <script src="../js_jquery/home-controls.js"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var commentProfilePic = document.getElementById("commentProfilePic");
            var replyProfilePic = document.getElementById("replyProfilePic");

            var commentPosition = commentProfilePic.getBoundingClientRect();
            var replyPosition = replyProfilePic.getBoundingClientRect();
            console.log(replyPosition);
            console.log(commentPosition);
            var distance = Math.abs(commentPosition.bottom - replyPosition.top) - 17;

            var line = document.getElementById("connectorLine");
            line.style.height = distance + "px";

            /*====== show image preview ========*/
            var contentInput = document.getElementById('expandingTextarea');
            var imageInput = document.getElementById('image');
            var videoInput = document.getElementById('video');
            var replyButton = document.getElementsByName('add_comment')[0];

            replyButton.disabled = true;

            contentInput.addEventListener('input', checkInputs);
            imageInput.addEventListener('change', checkInputs);
            videoInput.addEventListener('change', checkInputs);

            function checkInputs() {
                if (contentInput.value.trim() !== '' || imageInput.files.length > 0 || videoInput.files.length > 0) {
                    replyButton.disabled = false;
                } else {
                    replyButton.disabled = true;
                }
            }
        });

        /* let phoneMenu = document.getElementById('phoneMenu');
        let isOpen = false;

        function togglePhoneMenu() {
            if (!isOpen) {
                // Using GSAP to animate the opening of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: '100%', // This is equivalent to max-w-auto
                    onComplete: () => {
                        phoneMenu.classList.add('duration-200');
                    },
                });
            } else {
                // Using GSAP to animate the closing of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: 0, // This is equivalent to max-w-0
                    onComplete: () => {
                        phoneMenu.classList.remove('duration-200');
                    },
                });
            }

            isOpen = !isOpen;
        }

        let phoneMenuIcon = document.getElementById('openPhoneMenuIcon');
        phoneMenuIcon.addEventListener('click', togglePhoneMenu);
        let open = false;

        function toggleMenu() {
            let lines = document.querySelectorAll('.burger_line');

            gsap.to(lines[0], {
                duration: 0.05,
                rotate: open ? 0 : 45
            });
            gsap.to(lines[1], {
                duration: 0.2,
                x: open ? 0 : 45,
                opacity: open ? 1 : 5
            });
            gsap.to(lines[2], {
                duration: 0.05,
                rotate: open ? 0 : -45
            });
            open = !open;
        }
        window.onclick = function() {
            let button = document.getElementById('openPhoneMenuIcon');
            if (!button.contains(event.target) && open) {
                togglePhoneMenu();
                toggleMenu();
            }
        } */
    </script>
</body>

</html>