<?php
require '../vendor/autoload.php';

use ProjektiBlog\public\classes\Comment;
use ProjektiBlog\public\classes\Like;
use ProjektiBlog\public\classes\Post;
use ProjektiBlog\public\classes\User;
use ProjektiBlog\public\classes\Likes;
use ProjektiBlog\public\classes\Follower;
use ProjektiBlog\public\classes\TimeFormatter;
use ProjektiBlog\public\classes\SessionManager;

$session = new SessionManager();

include "include/head.php";

$user = new User();
$user = $user->fetchWithId($_SESSION['userid']);
if (isset($_GET['post_nr']) && isset($_GET['comment_nr'])) {
    $postId = $_GET['post_nr'];
    $commentId = $_GET['comment_nr'];
} else {
    header('Location: index.php');
}
?>

<body class="bg-img">

    <main class="w-full">
        <?php if ($session->message()) : ?>
            <div id="notification" class=" fixed bottom-6 p-4 z-50 right-6 w-[30%] flex flex-col justify-between rounded-xl h-[130px] bg-dark-active bg-opacity-75 backdrop-blur-sm shadow-xl text-adm-white">
                <div class="flex gap-4 items-center">
                    <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                    <p class="text-3xl">Hi <?php $user->getName() ?>!</p>
                </div>
                <p class=""><?= $session->message() ?></p>
            </div>
        <?php
        endif;
        require 'include/sidenav.php';
        ?>
        <div class="w-4/5 ml-auto flex">
            <div class="w-[65%]">
                <div class="flex border-b w-full sticky top-0 z-30 backdrop-blur-lg border-b-neutral-700 text-adm-white px-4 py-3 justify-between items-center">
                    <div class="flex items-center gap-5">
                        <a href="post.php?id=" class=" rounded-full w-[36px] h-[36px] hover:bg-dark-active duration-300 flex items-center justify-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left">
                                <line x1="19" y1="12" x2="5" y2="12"></line>
                                <polyline points="12 19 5 12 12 5"></polyline>
                            </svg>
                        </a>
                        <h1 class="text-2xl ">Comment</h1>
                    </div>
                    <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                </div>
                <section id="main-section" class="w-full">
                    <div class="relative">
                        <div id="connectorLine" class="w-[2px] bg-neutral-400 absolute left-9  top-14 ">

                        </div>
                        <!--------- the comment ----------->
                        <div class="w-full text-lg text-adm-lgrey flex gap-4 px-4 mt-3">
                            <div>
                                <img id="commentProfilePic" src="../resources/images/<?php /* $commenting_user->getProfilePicture() */ ?>" class="fancybox w-[45px] aspect-square object-cover rounded-full" alt="">
                            </div>
                            <div>
                                <p class=""><strong class="open-sans"><?php /* $commenting_user->getName() */ ?></strong> <a class=" text-neutral-400 hover:text-blue-500 duration-200" href="<?php /* $url */ ?>">@<?php /* $commenting_user->getUsername() */ ?></a><span class="text-neutral-400"> • <?php $formated_comment_time ?></span></p>
                                <p class="font-normal"><?php /* $c->getContent() */ ?></p>
                            </div>
                        </div>
                        <!---------- original post ---------->
                        <div class="w-[85%] ml-auto text-lg text-adm-lgrey flex gap-4 border border-neutral-700 rounded-2xl mt-1 px-2 mr-6">
                            <div>
                                <img src="../resources/images/<?php /* $commenting_user->getProfilePicture() */ ?>" class="fancybox w-[35px] mt-2 aspect-square object-cover rounded-full" alt="">
                            </div>
                            <div>
                                <p class=""><strong class="open-sans"><?php /* $commenting_user->getName() */ ?></strong> <a class=" text-neutral-400 hover:text-blue-500 duration-200" href="<?php $url ?>">@<?php /* $commenting_user->getUsername() */ ?></a><span class="text-neutral-400"> • <?php $formated_comment_time ?></span></p>
                                <p class="font-normal"><?php /* $c->getContent() */ ?></p>
                            </div>
                        </div>
                        <p class="w-[90%] ml-auto pl-3 pt-3 pb-1 text-lg text-neutral-400 cursor-default">Replying to <span class="text-blue-500 ">@<?php /* $post_author->getUsername() */ ?> and @<?php /* $post_author->getUsername() */ ?></span></p>
                    </div>
                    <div class="w-full">
                        <form action="../public/user_profile/add_comment.php" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="postId" class="opacity-0" value="<?php /* $postId */ ?>">
                            <div class="relative mb-1 flex items-start px-4 gap-4">
                                <img id="replyProfilePic" src="../resources/images/<?php /* $post_author->getProfilePicture() */ ?>" alt="" class="w-[45px] h-[45px] mt-4 aspect-square object-cover rounded-full group-hover:opacity-80 duration-200">
                                <textarea id="expandingTextarea" name="replyContent" spellcheck="false" type="text" placeholder="Write your reply" class="peer text-adm-lgrey outline-none text-xl w-full max-w-full break-words mt-2 px-2 resize-none bg-darker border-b-2 border-b-neutral-700 focus:border-b-adm-lgrey"></textarea>
                            </div>
                            <div id="post-image-container" class=" w-full relative peer-focus-within:mt-6"></div>
                            <div class="w-[90%] ml-auto flex justify-between pt-2 pb-1 sticky bottom-0 px-4">
                                <div class="flex gap-3">
                                    <label for="image" class="relative">
                                        <input id="image" name="image" type="file" class="hidden" accept="image/*" />
                                        <svg id="imageIcon" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-image cursor-pointer hover:scale-110 hover:stroke-adm-active duration-200">
                                            <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
                                            <circle cx="8.5" cy="8.5" r="1.5"></circle>
                                            <polyline points="21 15 16 10 5 21"></polyline>
                                        </svg>
                                    </label>
                                    <label for="video">
                                        <input id="video" name="video" type="file" class="hidden" accept="video/*" />
                                        <svg id='videoIcon' xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-video cursor-pointer hover:scale-110 hover:stroke-adm-active duration-200">
                                            <polygon points="23 7 16 12 23 17 23 7"></polygon>
                                            <rect x="1" y="5" width="15" height="14" rx="2" ry="2"></rect>
                                        </svg>
                                    </label>
                                </div>
                                <div>
                                    <input type="submit" value="Reply" name="add_comment" class="relative disabled:opacity-40 cursor-pointer disabled:cursor-not-allowed disabled:hover:bg-adm-lgrey group py-1 text-dark text-lg rounded-3xl px-8 bg-adm-lgrey hover:bg-adm-active duration-200">
                                </div>
                        </form>
                    </div>
                </section>
            </div>
            <?php
            require 'include/preference_bar.php';
            ?>
        </div>
    </main>
    <script src="../js_jquery/home-controls.js"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var commentProfilePic = document.getElementById("commentProfilePic");
            var replyProfilePic = document.getElementById("replyProfilePic");

            var commentPosition = commentProfilePic.getBoundingClientRect();
            var replyPosition = replyProfilePic.getBoundingClientRect();
            console.log(replyPosition);
            console.log(commentPosition);
            var distance = Math.abs(commentPosition.bottom - replyPosition.top) - 17;

            var line = document.getElementById("connectorLine");
            line.style.height = distance + "px";

            /*====== show image preview ========*/
            var contentInput = document.getElementById('expandingTextarea');
            var imageInput = document.getElementById('image');
            var videoInput = document.getElementById('video');
            var replyButton = document.getElementsByName('add_comment')[0];

            replyButton.disabled = true;

            contentInput.addEventListener('input', checkInputs);
            imageInput.addEventListener('change', checkInputs);
            videoInput.addEventListener('change', checkInputs);

            function checkInputs() {
                if (contentInput.value.trim() !== '' || imageInput.files.length > 0 || videoInput.files.length > 0) {
                    replyButton.disabled = false;
                } else {
                    replyButton.disabled = true;
                }
            }
        });

        /* let phoneMenu = document.getElementById('phoneMenu');
        let isOpen = false;

        function togglePhoneMenu() {
            if (!isOpen) {
                // Using GSAP to animate the opening of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: '100%', // This is equivalent to max-w-auto
                    onComplete: () => {
                        phoneMenu.classList.add('duration-200');
                    },
                });
            } else {
                // Using GSAP to animate the closing of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: 0, // This is equivalent to max-w-0
                    onComplete: () => {
                        phoneMenu.classList.remove('duration-200');
                    },
                });
            }

            isOpen = !isOpen;
        }

        let phoneMenuIcon = document.getElementById('openPhoneMenuIcon');
        phoneMenuIcon.addEventListener('click', togglePhoneMenu);
        let open = false;

        function toggleMenu() {
            let lines = document.querySelectorAll('.burger_line');

            gsap.to(lines[0], {
                duration: 0.05,
                rotate: open ? 0 : 45
            });
            gsap.to(lines[1], {
                duration: 0.2,
                x: open ? 0 : 45,
                opacity: open ? 1 : 5
            });
            gsap.to(lines[2], {
                duration: 0.05,
                rotate: open ? 0 : -45
            });
            open = !open;
        }
        window.onclick = function() {
            let button = document.getElementById('openPhoneMenuIcon');
            if (!button.contains(event.target) && open) {
                togglePhoneMenu();
                toggleMenu();
            }
        } */
    </script>
</body>

</html>