<?php
require '../vendor/autoload.php';

use ProjektiBlog\public\classes\Like;
use ProjektiBlog\public\classes\Post;
use ProjektiBlog\public\classes\User;
use ProjektiBlog\public\classes\Likes;
use ProjektiBlog\public\classes\Profile;
use ProjektiBlog\public\classes\Follower;
use ProjektiBlog\public\classes\TimeFormatter;
use ProjektiBlog\public\classes\SessionManager;

$session = new SessionManager();

include "include/head.php";

$user = new User();
$profile = new Profile();
$user = $user->fetchWithId($_SESSION['userid']);
$profile = $profile->findProfileWithUserId($_SESSION['userid']);
?>

<body class="bg-img">

    <main class="w-full">
        <?php if ($session->message()) : ?>
            <div id="notification" class=" fixed bottom-6 p-4 z-50 right-6 w-[30%] flex flex-col justify-between rounded-xl h-[130px] bg-dark-active bg-opacity-75 backdrop-blur-sm shadow-xl text-adm-white">
                <div class="flex gap-4 items-center">
                    <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                    <p class="text-3xl">Hi <?= $user->getName() ?>!</p>
                </div>
                <p class=""><?= $session->message() ?></p>
            </div>
        <?php
        endif;
        require 'include/sidenav.php';
        ?>
        <div class="w-4/5 ml-auto flex">
            <div class="w-[65%]">
                <div class="w-full">
                    <div class="flex border-b border-b-neutral-700 text-adm-white px-4 py-3 justify-between items-center">
                        <h1 class="text-2xl ">Home</h1>
                        <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                    </div>
                    <div class="px-4 py-2">
                        <div class="flex w-full items-center gap-4">
                            <div class="w-[55px] h-[50px]">
                                <?php if (!empty($user->getProfilePicture()) && !is_null($user->getProfilePicture())) : ?>
                                    <img src="../resources/images/<?= $user->getProfilePicture() ?>" alt="" class="w-full h-full object-cover rounded-full">
                                <?php endif ?>
                            </div>
                            <div class="w-full">
                                <!---------------- starti i formes per postim  ------------------->
                                <form action="../public/user_profile/add_post.php" method="post" enctype="multipart/form-data">
                                    <div class="relative ">
                                        <textarea id="expandingTextarea" name="content" spellcheck="false" type="text" placeholder="Feeling artistic?" class="peer text-adm-lgrey outline-none text-xl w-full max-w-full break-words mt-2 px-2 resize-none bg-darker border-b-2 border-b-neutral-700 focus:border-b-adm-lgrey"></textarea>
                                        <span class="absolute -top-8 bg-opacity-60 bg-neutral-400 left-0 w-full peer-focus-within:opacity-100 opacity-0 -z-30 peer-focus-within:z-30 duration-75 text-white rounded-lg px-2 py-0.5">Add a hashtag # before a text to make it a tag!</span>
                                    </div>
                            </div>
                        </div>
                        <div id="post-image-container" class="pl-[4.1rem] w-full relative peer-focus-within:mt-6"></div>
                        <div class="flex justify-between pt-2 pb-1 sticky bottom-0 bg-darker">
                            <div class="flex gap-3 pl-16">
                                <label for="image" class="relative">
                                    <input id="image" name="image" type="file" class="hidden" accept="image/*" />
                                    <svg id="imageIcon" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-image cursor-pointer hover:scale-110 duration-200">
                                        <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
                                        <circle cx="8.5" cy="8.5" r="1.5"></circle>
                                        <polyline points="21 15 16 10 5 21"></polyline>
                                    </svg>
                                </label>
                                <label for="video">
                                    <input id="video" name="video" type="file" class="hidden" accept="video/*" />
                                    <svg id='videoIcon' xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-video cursor-pointer hover:scale-110 duration-200">
                                        <polygon points="23 7 16 12 23 17 23 7"></polygon>
                                        <rect x="1" y="5" width="15" height="14" rx="2" ry="2"></rect>
                                    </svg> </label>
                            </div>
                            <div>
                                <input type="submit" value="Reply" name="add_post" class="relative disabled:opacity-40 cursor-pointer disabled:cursor-not-allowed disabled:hover:bg-adm-lgrey group py-1 text-dark text-lg rounded-3xl px-8 bg-adm-lgrey hover:bg-adm-active duration-200">


                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="w-full py-4 bg-dark"></div>
                <section id="main-section" class="w-full">
                    <?php
                    $userPosts = new Post();
                    $userPosts->setUserId($_SESSION['userid']);
                    $userPosts = $userPosts->fetchPostsForHome();
                    foreach ($userPosts as $p) {
                        $published_at = $p->getCreatedAt();
                        $time_ago = TimeFormatter::formatRoundedTimeAgo($published_at);
                        $like = new Like();
                        $like->setPostId($p->getId());
                        $like->setUserId($_SESSION['userid']);
                        if ($like->isLikedByUser()) {
                            $fill = 'red';
                            $stroke = 'red';
                        } else {
                            $fill = 'none';
                            $stroke = 'currentColor';
                        }
                        $like_count = $like->getLikeCountForPost();
                        $content = $p->getContent();

                        $users = new User();
                        $users = $users->fetchWithId($p->getUserId());

                        preg_match_all('/#\w+/', $content, $matches);

                        $hashtags = $matches[0];
                        foreach ($hashtags as $tag) {
                            $anchorTag = '<a href="hashtag.php?tag=' . $tag . '" class="text-blue-500 underline underline-offset-2">' . $tag . '</a>';
                            $content = str_replace($tag, $anchorTag, $content);
                        }
                        if ($users->getId() === $_SESSION['userid']) {
                            $direct_url = 'profile.php';
                        } else {
                            $direct_url = 'account.php?user=' . $users->getUsername();
                        }
                    ?>
                        <div class="flex gap-4 items-start pt-6 border-b px-4 pb-4 border-b-neutral-700 hover:bg-darker duration-200 hover:shadow-round hover:shadow-white">
                            <a href="<?= $direct_url ?>" class="group">
                                <div class="w-[50px] h-[50px] shrink-0">
                                    <img src="../resources/images/<?= $users->getProfilePicture() ?>" alt="" class="w-full h-full aspect-square object-cover rounded-full group-hover:opacity-80 duration-200">
                                </div>
                                <div class="text-white text-lg w-full">
                                    <div>
                                        <p class=" text-neutral-400"><strong class="text-white"><?= $users->getName() ?></strong> @<span class="group-hover:underline group-hover:underline-offset-2 duration-200"><?= $users->getUsername() ?></span> • <?= $time_ago ?></p>
                                    </div>
                            </a>
                            <div class="pt-2 pr-4">
                                <p class=" leading-snug">
                                    <?= $content ?>
                                </p>
                                <?php if (!empty($p->getMedia())) : ?>
                                    <?php
                                    $mediaPath = "../resources/images/" . $p->getMedia();
                                    $mediaType = pathinfo($mediaPath, PATHINFO_EXTENSION);
                                    ?>

                                    <div class="w-full my-3 rounded-3xl overflow-hidden">
                                        <?php if (in_array($mediaType, ['jpg', 'jpeg', 'png', 'gif'])) : ?>
                                            <img src="<?= $mediaPath ?>" alt="" class="w-full aspect-auto object-cover">
                                        <?php elseif (in_array($mediaType, ['mp4', 'webm', 'ogg'])) : ?>
                                            <video controls class="w-full aspect-video object-cover">
                                                <source src="<?= $mediaPath ?>" type="video/<?= $mediaType ?>">
                                                Your browser does not support the video tag.
                                            </video>
                                        <?php else : ?>
                                            <p>Unsupported media type</p>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="w-full flex justify-between items-center px-8 pt-3">
                                <span class="flex items-center gap-2">
                                    <div class='like-button hover:bg-red-400 hover:bg-opacity-20 rounded-full hover:shadow-round hover:shadow-red-500 cursor-pointer duration-200 w-[42px] flex items-center justify-center h-[42px]' data-post-id="<?= $p->getId() ?>">
                                        <svg id="like-icon" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="<?= $fill ?>" stroke="<?= $stroke ?>" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather duration-300 rounded-xl feather-heart">
                                            <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
                                        </svg>
                                    </div>
                                    <span class="like-count"><?= $like_count ?></span>
                                </span>

                                <span class="flex items-center gap-2">
                                    <div class='hover:bg-blue-400 hover:bg-opacity-20 rounded-full hover:shadow-round hover:shadow-blue-500 cursor-pointer   duration-200 w-[42px] flex items-center justify-center h-[42px]'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle">
                                            <path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path>
                                        </svg>
                                    </div>
                                    0
                                </span>
                                <a href="post.php?id=<?= $p->getId() ?>" class="flex items-center px-6 py-1 rounded-3xl bg-adm-lgrey text-dark hover:bg-neutral-300 duration-200">
                                    View Post
                                </a>
                            </div>
                        </div>
            </div>
        <?php
                    }
        ?>
        </section>

        </div>
        <?php
        require 'include/preference_bar.php';
        ?>
        </div>
    </main>
    <script src="../js_jquery/home-controls.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // Get references to the form elements
            var contentInput = document.getElementById('expandingTextarea');
            var imageInput = document.getElementById('image');
            var videoInput = document.getElementById('video');
            var replyButton = document.getElementsByName('add_post')[0];

            // Initially disable the "Reply" button
            replyButton.disabled = true;

            // Add event listeners to the input elements
            contentInput.addEventListener('input', checkInputs);
            imageInput.addEventListener('change', checkInputs);
            videoInput.addEventListener('change', checkInputs);

            // Function to check inputs and enable/disable the button accordingly
            function checkInputs() {
                // Check if either content, image, or video inputs have data
                if (contentInput.value.trim() !== '' || imageInput.files.length > 0 || videoInput.files.length > 0) {
                    // Enable the "Reply" button
                    replyButton.disabled = false;
                } else {
                    // Disable the "Reply" button
                    replyButton.disabled = true;
                }
            }
        });
        /* let phoneMenu = document.getElementById('phoneMenu');
        let isOpen = false;

        function togglePhoneMenu() {
            if (!isOpen) {
                // Using GSAP to animate the opening of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: '100%', // This is equivalent to max-w-auto
                    onComplete: () => {
                        phoneMenu.classList.add('duration-200');
                    },
                });
            } else {
                // Using GSAP to animate the closing of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: 0, // This is equivalent to max-w-0
                    onComplete: () => {
                        phoneMenu.classList.remove('duration-200');
                    },
                });
            }

            isOpen = !isOpen;
        }

        let phoneMenuIcon = document.getElementById('openPhoneMenuIcon');
        phoneMenuIcon.addEventListener('click', togglePhoneMenu);
        let open = false;

        function toggleMenu() {
            let lines = document.querySelectorAll('.burger_line');

            gsap.to(lines[0], {
                duration: 0.05,
                rotate: open ? 0 : 45
            });
            gsap.to(lines[1], {
                duration: 0.2,
                x: open ? 0 : 45,
                opacity: open ? 1 : 5
            });
            gsap.to(lines[2], {
                duration: 0.05,
                rotate: open ? 0 : -45
            });
            open = !open;
        }
        window.onclick = function() {
            let button = document.getElementById('openPhoneMenuIcon');
            if (!button.contains(event.target) && open) {
                togglePhoneMenu();
                toggleMenu();
            }
        } */
    </script>
</body>

</html>