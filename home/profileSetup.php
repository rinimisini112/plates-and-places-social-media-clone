<?php
session_start();
require '../vendor/autoload.php';

use ProjektiBlog\public\classes\Profile;
use ProjektiBlog\public\classes\User;

include "include/head.php";


$user = new User();
$profile = new Profile();
$user = $user->fetchWithId($_SESSION['userid']);
$profile = $profile->findProfileWithUserId($_SESSION['userid']);

$profile->confirmSetup();
?>

<body class="bg-img">
    <main class="w-full relative">
        <?php if (isset($_SESSION['err_message'])) : ?>
            <div id="notification" class=" fixed bottom-6 p-4 right-6 w-[30%] flex flex-col justify-between rounded-xl h-[130px] bg-dark-active bg-opacity-75 backdrop-blur-sm shadow-xl text-adm-white">
                <div class="flex gap-4 items-center">
                    <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                    <p class="text-3xl">Hi <?= $user->getName() ?>!</p>
                </div>
                <p class=""><?= $_SESSION['err_message'] ?></p>
            </div>
        <?php
            unset($_SESSION['err_message']);
        endif;
        require 'include/sidenav.php' ?>
        <div class="w-4/5 ml-auto flex">
            <div class="w-[65%]">
                <div class="w-full">
                    <div class="flex text-adm-white px-4 py-2 justify-between items-center sticky top-0 bg-[#2023299d] backdrop-blur-md z-10 ">
                        <div class="flex gap-8">
                            <a href="profile.php" class=" rounded-full w-[36px] h-[36px] hover:bg-dark-active duration-300 flex items-center justify-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left">
                                    <line x1="19" y1="12" x2="5" y2="12"></line>
                                    <polyline points="12 19 5 12 12 5"></polyline>
                                </svg>
                            </a>
                            <h1 class="text-2xl flex flex-col">Finish setting your profile up</h1>
                        </div>
                        <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                    </div>
                    <div id="coverImageContainer" class="bg-dark-active h-[250px] relative">
                        <?php if (!is_null($profile->getCoverImage())) : ?>
                            <img id="existingCoverImg" src="../resources/images/<?= $profile->getCoverImage() ?>" alt="" class="max-w-full max-h-[250px] w-full h-full object-cover">
                        <?php endif ?>
                        <form action="../public/user_profile/modify_profile.php" method="post" id="coverImageForm" enctype="multipart/form-data">
                            <label for="coverImage" class="cursor-pointer absolute left-[35%] top-[40%] px-6 py-2 backdrop-blur-lg text-xl bg-[2023299d] hover:bg-darker duration-200 text-white">
                                <input type="file" name="cover_image" id="coverImage" class="hidden" onchange="handleImageChange()">
                                Change cover photo
                            </label>
                    </div>
                    <div class="">
                        <div class="pl-12 pr-4">
                            <div id="profilePictureContainer" class="relative w-[170px] h-[170px] transform -translate-y-1/2">
                                <?php if (!is_null($user->getProfilePicture())) : ?>
                                    <img src="../resources/images/<?= $user->getProfilePicture() ?>" alt="" class="rounded-full border-4 border-darker w-[170px] h-[170px] max-w-[170px] max-h-[170px] object-cover" id="profilePicture">
                                <?php else : ?>
                                    <img src="../resources/images/blankProfileImage.webp" alt="" class="rounded-full border-4 border-darker w-[170px] h-[170px] max-w-[170px] max-h-[170px] object-cover" id="profilePicture">
                                <?php endif ?>
                                <label id="profileButton" for="profilePictureInput" class="cursor-pointer absolute left-[25%] top-[40%] px-3 py-1 backdrop-blur-lg text-xl bg-[#20232933] hover:bg-darker duration-200 text-white">
                                    <input type="file" name="profilePicture" id="profilePictureInput" class="hidden" onchange="handleProfilePictureChange()">
                                    Change
                                </label>
                            </div>
                        </div>
                        <div class="text-adm-lgrey border-b border-b-neutral-700 text-lg transform pt-4 flex flex-col pl-16 -translate-y-1/4 mt-6">
                            <label for="fullName" class="text-adm-lgrey">
                                Change your name
                                <input type="text" name="fullName" id="fullName" class="text-neutral-400 outline-none pl-3 bg-dark rounded-t-lg w-[80%] pt-2 focus:py-2 border-b-2 border-b-neutral-700 focus:border-b-adm-active duration-200" value="<?= $user->getName() . ' ' . $user->getSurname(); ?>">
                            </label>
                            <label for="userName" class="text-adm-lgrey mt-6">
                                Change your username
                                <span class="relative"><input type="text" name="userName" id="userName" class="text-neutral-400 outline-none pl-6 bg-dark rounded-t-lg w-[80%] pt-2 focus:py-2 border-b-2 border-b-neutral-700 focus:border-b-adm-active duration-200" value="<?= $user->getUsername(); ?>">
                                    <span class="absolute -top-1 left-1 text-neutral-500 font-thin">@</span>
                                </span>
                            </label>
                            <label for="biography" class="text-adm-lgrey mt-6">
                                Enter a bio tell people your story
                                <input type="text" name="biography" id="biography" class="text-neutral-400 outline-none pl-3 bg-dark rounded-t-lg w-[80%] pt-2 focus:py-2 border-b-2 border-b-neutral-700 focus:border-b-adm-active duration-200" value="<?= $profile->getBio() !== null ? $profile->getBio() : null; ?>" placeholder="Enter a bio tell your story">
                            </label>
                            <label for="webLink" class="text-adm-lgrey mt-6">
                                You can add a website link or link it to other social medias
                                <input type="text" name="webLink" id="webLink" class="text-neutral-400 outline-none pl-3 bg-dark rounded-t-lg w-[80%] pt-2 focus:py-2 border-b-2 border-b-neutral-700 focus:border-b-adm-active duration-200" value="<?= $profile->getWebsiteLink() !== null ? $profile->getWebsiteLink() : null ?>" placeholder="Paste a link that works!">
                            </label>
                            <div style="font-family: 'segoeUI';" class="flex gap-3 pb-4 pt-8 justify-between items-center w-[80%]">
                                <?php if (!is_null($profile->getCreatedAt())) : ?>
                                    <p>• Joined <?= $profile->formatCreatedAt() ?></p>
                                <?php endif ?>
                                <input type="submit" name="update_profile" id="update_profile" value="Save changes" class="py-2 px-8 hover:bg-dark-active duration-200 cursor-pointer bg-darker border border-adm-lgrey rounded-3xl">
                            </div>

                            </form>
                        </div>
                    </div>
                </div>
                </section>
            </div>
            <?php
            require 'include/preference_bar.php';
            ?>
        </div>
    </main>

    <script>
        function handleProfilePictureChange() {
            var profilePictureContainer = document.getElementById('profilePictureContainer');
            var profileButton = document.getElementById('profileButton');
            var existingImage = profilePictureContainer.querySelector('img');

            profileButton.style.opacity = 0.2;
            if (existingImage) {
                existingImage.parentNode.removeChild(existingImage);
            }

            var input = document.getElementById('profilePictureInput');
            var newImage = document.createElement('img');
            newImage.src = URL.createObjectURL(input.files[0]);
            newImage.alt = 'New Profile Picture';
            newImage.className = 'rounded-full border-4 border-darker w-[170px] h-[170px] max-w-[170px] max-h-[170px] object-cover';
            newImage.id = 'profilePicture';

            profilePictureContainer.appendChild(newImage);
        }

        function handleImageChange() {
            const container = document.getElementById('coverImageContainer');
            const form = document.getElementById('coverImageForm');
            const fileInput = document.getElementById('coverImage');
            let existingCoverImage = document.getElementById('existingCoverImg');
            if (existingCoverImage) {
                existingCoverImage.parentNode.removeChild(existingCoverImage);
            }
            var newImage = document.createElement('img');
            newImage.src = URL.createObjectURL(fileInput.files[0]);
            newImage.alt = 'New Cover image';
            newImage.className = 'max-w-full max-h-[250px] w-full h-full object-cover';
            newImage.id = 'coverImageImg';

            container.appendChild(newImage);


        }
        $(document).ready(function() {
            // Wait for the document to be ready
            setTimeout(function() {
                $('#notification').fadeOut('slow'); // Hide the element with a fade-out effect
            }, 4000); // 5000 milliseconds = 5 seconds
        });
        let phoneMenu = document.getElementById('phoneMenu');
        let isOpen = false;

        function togglePhoneMenu() {
            if (!isOpen) {
                // Using GSAP to animate the opening of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: '100%', // This is equivalent to max-w-auto
                    onComplete: () => {
                        phoneMenu.classList.add('duration-200');
                    },
                });
            } else {
                // Using GSAP to animate the closing of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: 0, // This is equivalent to max-w-0
                    onComplete: () => {
                        phoneMenu.classList.remove('duration-200');
                    },
                });
            }

            isOpen = !isOpen;
        }

        let phoneMenuIcon = document.getElementById('openPhoneMenuIcon');
        phoneMenuIcon.addEventListener('click', togglePhoneMenu);
        let open = false;

        function toggleMenu() {
            let lines = document.querySelectorAll('.burger_line');

            gsap.to(lines[0], {
                duration: 0.05,
                rotate: open ? 0 : 45
            });
            gsap.to(lines[1], {
                duration: 0.2,
                x: open ? 0 : 45,
                opacity: open ? 1 : 5
            });
            gsap.to(lines[2], {
                duration: 0.05,
                rotate: open ? 0 : -45
            });
            open = !open;
        }
        window.onclick = function() {
            let button = document.getElementById('openPhoneMenuIcon');
            if (!button.contains(event.target) && open) {
                togglePhoneMenu();
                toggleMenu();
            }
        }
    </script>
</body>

</html>