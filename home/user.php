<?php
require '../vendor/autoload.php';

session_start();

use ProjektiBlog\public\classes\Follower;
use ProjektiBlog\public\classes\Profile;
use ProjektiBlog\public\classes\User;

include "include/head.php";


$user = new User();
$profile = new Profile();
$user = $user->fetchWithId($_SESSION['userid']);
$profile = $profile->findProfileWithUserId($_SESSION['userid']);

$follower = new Follower();
$activeUnderline = "<span class='absolute -bottom-[1.30rem] rounded-3xl -left-0.5 w-full h-[4px] bg-adm-active'></span>";

if (isset($_GET['followers'])) {
    $follower->setUserId($_SESSION['userid']);
    $followers_ids = $follower->getFollowersNum();
    $users_list = [];
    foreach ($followers_ids as $single_id) {
        $follower = new User();
        $follower = $follower->fetchWithId($single_id);
        $users_list[] = $follower;
    }
}
if (isset($_GET['following'])) {
    $follower->setFollowerUserId($_SESSION['userid']);
    $following_ids = $follower->getFollowingNum();
    $users_list = [];
    foreach ($following_ids as $single_id) {
        $follower = new User();
        $follower = $follower->fetchWithId($single_id);
        $users_list[] = $follower;
    }
}
?>

<body class="bg-img">
    <main class="w-full relative">
        <?php if (isset($_SESSION['success_message'])) : ?>
            <div id="notification" class=" fixed bottom-6 p-4 right-6 w-[30%] flex flex-col justify-between rounded-xl h-[130px] bg-dark-active bg-opacity-75 backdrop-blur-sm shadow-xl text-adm-white">
                <div class="flex gap-4 items-center">
                    <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                    <p class="text-3xl">Hi <?= $user->getName() ?>!</p>
                </div>
                <p class=""><?= $_SESSION['success_message'] ?></p>
            </div>
        <?php
            unset($_SESSION['success_message']);
        endif;
        require 'include/sidenav.php'; ?>
        <div class="w-4/5 ml-auto flex">
            <div class="w-[65%]">
                <div class="w-full">
                    <div class="flex text-adm-white px-4 py-2 justify-between items-center sticky top-0 bg-[#2023299d] backdrop-blur-md z-10 ">
                        <div class="flex gap-8">
                            <a href="profile.php" class=" rounded-full w-[36px] h-[36px] hover:bg-dark-active duration-300 flex items-center justify-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left">
                                    <line x1="19" y1="12" x2="5" y2="12"></line>
                                    <polyline points="12 19 5 12 12 5"></polyline>
                                </svg>
                            </a>
                            <h1 class="text-2xl flex flex-col"><?= $user->getName() . ' ' . $user->getSurname() ?>
                                <p class="text-lg -mt-2 text-neutral-500">@<span><?= $user->getUsername() ?></span></p>
                            </h1>
                        </div>
                        <img class="invert" src="../resources/images/pngwing.com.png" alt="">
                    </div>
                    <ul class="flex items-center justify-between w-full  border-b border-b-neutral-700 text-xl text-adm-lgrey">
                        <a href="?followers" class=" inline-block w-[50%]">
                            <li class="hover:bg-dark-active duration-300 py-4 text-center">
                                <span class="relative">
                                    Your Followers
                                    <?php if (isset($_GET['followers'])) echo $activeUnderline; ?>
                                </span>
                            </li>
                        </a>
                        <a href="?following" class=" inline-block w-[50%]">
                            <li class="hover:bg-dark-active duration-300 py-4 text-center">
                                <span class="relative">
                                    People you follow
                                    <?php if (isset($_GET['following'])) echo $activeUnderline; ?>
                                </span>
                            </li>
                        </a>
                    </ul>

                    <?php
                    if (isset($users_list) && !empty($users_list)) {
                        foreach ($users_list as $single_user) {
                            echo "<div class='w-full bg-darker text-adm-lgrey py-3 hover:bg-dark-active duration-200 px-16 flex justify-between items-center'>";
                            echo "<div class='flex items-center gap-3'>";
                            echo "<div class='w-[50px] h-[50px]'>";
                            if (!is_null($single_user->getProfilePicture())) {
                                echo "<img src='../resources/images/{$single_user->getProfilePicture()}' class='w-full h-full object-cover rounded-full' alt=''>";
                            }
                            echo "</div>";
                            echo "<div>";
                            $fullName = $single_user->getName() . ' ' . $single_user->getSurname();
                            echo "<a href='account.php?user="  . $single_user->getUsername() .  "' class=' hover:underline underline-offset-2 duration-200'>{$fullName}</a>";
                            echo "<p class=' cursor-default'>@{$single_user->getUsername()}</p>";
                            echo "</div>";
                            echo "</div>";
                            $follower = new Follower();
                            $follower->setFollowerUserId($_SESSION['userid']);
                            if (!$follower->youFollowUser($single_user->getId())) {
                                echo "<form action='' method='post'>";
                                echo "<input type='submit' value='Follow' class='hover:bg-neutral-400 text-lg font-bold cursor-pointer duration-200 bg-adm-lgrey text-darker rounded-3xl px-10 py-1.5'>";
                                echo "</form>";
                            }
                            echo "</div>";
                        }
                    } else {
                        echo "<p style='font-family:kenyan-coffee' class='pt-16 text-adm-lgrey px-24 text-5xl font-bold'>Nothing to show here, go out there make some friends.</p>";
                    }
                    ?>
                </div>
            </div>
            <?php
            require 'include/preference_bar.php';
            ?>
        </div>
    </main>

    <script>
        $(document).ready(function() {
            // Wait for the document to be ready
            setTimeout(function() {
                $('#notification').fadeOut('slow'); // Hide the element with a fade-out effect
            }, 4000); // 5000 milliseconds = 5 seconds
        });
        let phoneMenu = document.getElementById('phoneMenu');
        let isOpen = false;

        function togglePhoneMenu() {
            if (!isOpen) {
                // Using GSAP to animate the opening of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: '100%', // This is equivalent to max-w-auto
                    onComplete: () => {
                        phoneMenu.classList.add('duration-200');
                    },
                });
            } else {
                // Using GSAP to animate the closing of the menu
                gsap.to(phoneMenu, {
                    duration: 0.4, // Adjust the duration as needed
                    maxWidth: 0, // This is equivalent to max-w-0
                    onComplete: () => {
                        phoneMenu.classList.remove('duration-200');
                    },
                });
            }

            isOpen = !isOpen;
        }

        let phoneMenuIcon = document.getElementById('openPhoneMenuIcon');
        phoneMenuIcon.addEventListener('click', togglePhoneMenu);
        let open = false;

        function toggleMenu() {
            let lines = document.querySelectorAll('.burger_line');

            gsap.to(lines[0], {
                duration: 0.05,
                rotate: open ? 0 : 45
            });
            gsap.to(lines[1], {
                duration: 0.2,
                x: open ? 0 : 45,
                opacity: open ? 1 : 5
            });
            gsap.to(lines[2], {
                duration: 0.05,
                rotate: open ? 0 : -45
            });
            open = !open;
        }
        window.onclick = function() {
            let button = document.getElementById('openPhoneMenuIcon');
            if (!button.contains(event.target) && open) {
                togglePhoneMenu();
                toggleMenu();
            }
        }
    </script>
</body>

</html>