<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../dist/output.css">
    <link rel="stylesheet" href="../dist/custom.css">
    <script src="../js_jquery/jquery.js"></script>
    <script src="../js_jquery/jquery.validate.js"></script>
    <script src="../js_jquery/jquery.validate.min.js"></script>
    <script src="../js_jquery/minified/gsap.min.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=League+Spartan:wght@300;400;700&family=Open+Sans:wght@300;500;700&family=Oswald:wght@600&family=PT+Mono&family=Playfair+Display:wght@400;600;700&family=Roboto+Mono:wght@700&family=Roboto+Slab:wght@300;400;700&family=Roboto:wght@300;500;700&family=Slabo+27px&family=Source+Sans+3:wght@300&family=Ysabeau+SC:wght@300;600&display=swap');
    </style>
</head>