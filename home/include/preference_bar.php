<?php

use ProjektiBlog\public\classes\Follower;
use ProjektiBlog\public\classes\User;

?>
<div id="sidePreferenceBar" class="w-[35%] border-l border-l-neutral-700 py-8 ">
    <div id="mostPopular" class="w-[85%] rounded-2xl overflow-hidden bg-dark mx-8 mr-auto">
        <p class="pl-4 text-adm-lgrey text-xl border-b border-b-neutral-700 pt-3 pb-1">Most Popular at the moment</p>
        <div class="border-b border-b-neutral-700 pb-3">
            <div class="flex items-center gap-2 py-2 px-2">
                <img src="../resources/images/riniProfile.jpg" class="w-[40px] rounded-full" alt="">
                <p class="text-adm-lgrey">@<strong class="underline underline-offset-2">rinimisini</strong> posted :</p>
            </div>
            <div class="w-[90%] flex justify-between mx-auto border overflow-hidden shadow-xl  border-neutral-700 rounded-3xl">
                <div class="w-3/5 py-2 text-adm-lgrey pl-4 pr-2">
                    <h2 class="text-lg text-neutral-400 pt-2">Design</h2>
                    <p class=" text-sm">Hey check out my design i made in collab with @<a class="underline underline-offset-2" href="">creativeWebStudios</a></p>
                </div>
                <img class="w-2/5  object-cover" src="../resources/images/blog_img3.avif" alt="">
            </div>

        </div>
        <div class="border-b border-b-neutral-700 pb-3">
            <div class="flex items-center gap-2 py-2 px-2">
                <img src="../resources/images/riniProfile.jpg" class="w-[40px] rounded-full" alt="">
                <p class="text-adm-lgrey">@<strong class="underline underline-offset-2">rinimisini</strong> posted :</p>
            </div>
            <div class="w-[90%] flex justify-between mx-auto border overflow-hidden shadow-xl  border-neutral-700 rounded-3xl">
                <div class="w-full py-2 text-adm-lgrey px-4 ">
                    <h2 class="text-lg text-neutral-400 pt-2">Food</h2>
                    <p class=" text-sm">Qeky veni i ri nbreg tdillit picat mat mira nsheher i paska @<a class="underline underline-offset-2" href="">Napoli</a>
                        une per vete e morra chef pica edhe ty njejt ta kisha sygjery
                    </p>
                </div>
            </div>
        </div>
        <div class="pb-6 border-b border-b-neutral-700">
            <div class="flex items-center gap-2 py-2 px-2">
                <img src="../resources/images/riniProfile.jpg" class="w-[40px] rounded-full" alt="">
                <p class="text-adm-lgrey">@<strong class="underline underline-offset-2">rinimisini</strong> posted :</p>
            </div>
            <div class="w-[90%] flex justify-between mx-auto border overflow-hidden shadow-xl  border-neutral-700 rounded-3xl">
                <div class="w-3/5 py-2 text-adm-lgrey pl-4 pr-2">
                    <h2 class="text-lg text-neutral-400 pt-2">Food</h2>
                    <p class=" text-sm">Best baked pies in kosovo atm check it out @<a class="underline underline-offset-2" href="">tartinePr</a></p>
                </div>
                <img class="w-2/5  object-cover" src="../resources/images/Baked-Peaches-iPhone.jpg" alt="">
            </div>
        </div>
        <a href="" class="">
            <div class="w-full py-3 px-5 text-lg text-adm-lgrey hover:bg-dark-active duration-200">
                Show more
            </div>
        </a>
    </div>
    <div id="popularTags" class="w-[85%] rounded-2xl overflow-hidden bg-dark mx-8 mt-8 mr-auto">
        <p class="pl-4 text-adm-lgrey text-xl border-b border-b-neutral-700 pt-3 pb-1">Trending tags #</p>
        <!---------------- who to follow users ---------------->
        <a href="">
            <div class="border-b hover:bg-dark-active  duration-200 border-b-neutral-700 pb-2 pt-2 text-lg ">
                <div class=" px-4 w-full">
                    <div class="flex items-center gap-2">
                        <p class="text-adm-lgrey">#<strong class="underline underline-offset-2">heregoesnothing</strong>
                        </p>
                    </div>
                    <p class="px-2 text-white">135 Posts</p>
                </div>
            </div>
        </a>
        <a href="">
            <div class="border-b hover:bg-dark-active  duration-200 border-b-neutral-700 pb-2 pt-2 text-lg ">
                <div class=" px-4 w-full">
                    <div class="flex items-center gap-2">
                        <p class="text-adm-lgrey">#<strong class="underline underline-offset-2">heregoesnothing</strong>
                        </p>
                    </div>
                    <p class="px-2 text-white">135 Posts</p>
                </div>
            </div>
        </a>
        <a href="">
            <div class="border-b hover:bg-dark-active  duration-200 border-b-neutral-700 pb-2 pt-2 text-lg ">
                <div class=" px-4 w-full">
                    <div class="flex items-center gap-2">
                        <p class="text-adm-lgrey">#<strong class="underline underline-offset-2">heregoesnothing</strong>
                        </p>
                    </div>
                    <p class="px-2 text-white">135 Posts</p>
                </div>
            </div>
        </a>
        <a href="">
            <div class="border-b hover:bg-dark-active  duration-200 border-b-neutral-700 pb-2 pt-2 text-lg ">
                <div class=" px-4 w-full">
                    <div class="flex items-center gap-2">
                        <p class="text-adm-lgrey">#<strong class="underline underline-offset-2">heregoesnothing</strong>
                        </p>
                    </div>
                    <p class="px-2 text-white">135 Posts</p>
                </div>
            </div>
        </a>
        <a href="">
            <div class="border-b hover:bg-dark-active  duration-200 border-b-neutral-700 pb-2 pt-2 text-lg ">
                <div class=" px-4 w-full">
                    <div class="flex items-center gap-2">
                        <p class="text-adm-lgrey">#<strong class="underline underline-offset-2">heregoesnothing</strong>
                        </p>
                    </div>
                    <p class="px-2 text-white">135 Posts</p>
                </div>
            </div>
        </a>
        <a href="">
            <div class="border-b hover:bg-dark-active duration-200 border-b-neutral-700 pb-2 pt-2 text-lg ">
                <div class=" px-4 w-full">
                    <div class="flex items-center gap-2">
                        <p class="text-adm-lgrey">#<strong class="underline underline-offset-2">heregoesnothing</strong>
                        </p>
                    </div>
                    <p class="px-2 text-white">135 Posts</p>
                </div>
            </div>
        </a>
        <a href="" class="">
            <div class="w-full py-3 px-5 text-lg text-adm-lgrey hover:bg-dark-active duration-200">
                Show more
            </div>
        </a>
    </div>
    <div class="sticky top-8">
        <div id="whoToFollow" class="w-[85%] rounded-2xl overflow-hidden bg-dark mx-8 mt-8 mr-auto">
            <p id='prefBarNotif' class='pl-4 text-adm-lgrey text-xl border-b border-b-neutral-700 pt-3 pb-1'>Who to follow</p>
            <!---------------- who to follow users ---------------->
            <div id="whoToFollowContainer">

            </div>
            <a href="" class="">
                <div class="w-full py-3 px-5 text-lg text-adm-lgrey hover:bg-[#353a45] duration-200">
                    Show more
                </div>
            </a>
        </div>
    </div>
    <div class="" id="footer">

    </div>
</div>
</div>
<script src="../js_jquery/preference-bar.js"></script>