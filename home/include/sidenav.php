<?php

use ProjektiBlog\public\classes\User;

$sidenav_profile = new User();
$sidenav_profile = $sidenav_profile->fetchWithId($_SESSION['userid']);
?>
<div class="w-1/5 bg-darker h-full flex flex-col justify-between fixed left-0 right-0 border-r z-50 border-r-neutral-700 pt-12 pb-6">
    <div>
        <a id="brandTitle" style="font-family:'amsterdam';" class=" px-6 back-font text-adm-white text-xl" href="index.php">Plates&Places</a>
        <img id="brandLogo" class="invert px-4 w-[70px] hidden" src="../resources/images/pngwing.com.png" alt="">
    </div>
    <div class="text-2xl text-adm-white">
        <ul>
            <a href="index.php" class="w-[90%] group rounded-2xl duration-200 cursor-pointer flex items-center mx-3 px-2 gap-8 hover:bg-dark py-3 my-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                    <path fill='#f6f6f6' d="M3 9l9-7.5 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline class="duration-100 group-hover:fill-dark group-hover:stroke-dark" fill='#08090a' stroke='#08090a' points="9 12 15 12 15 22 9 22 9 12"></polyline>
                </svg>
                Home
            </a>
            <li id="searchToggle" class="w-[90%] rounded-2xl duration-200 cursor-pointer flex items-center mx-3 px-2 gap-8 hover:bg-dark py-3 my-2 peer">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class=" feather feather-search">
                    <circle cx="11" cy="11" r="8"></circle>
                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                </svg>
                Search
            </li>
            <div id="searchSidebar" class="absolute top-0 left-16 z-50 bg-darker rounded-e-2xl w-[25rem] h-full duration-75 overflow-y-scroll" style="max-width: 0; border:none;">
                <p class="p-8 text-3xl">Search</p>
                <form action="" class=" duration-150 w-[90%] mx-auto pb-6">
                    <label for="search" class="cursor-pointer flex items-center overflow-hidden group">
                        <input type="search" name="search" id="search" class="w-full focus:py-2 duration-200 py-1.5 rounded-xl pl-6 text-adm-lgrey outline-none bg-dark text-xl" placeholder="Search Users">
                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24" fill="none" stroke="#999999" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="  group-focus-within:translate-x-12 group-focus-within:opacity-0 duration-150 transform -translate-x-9  feather feather-search">
                            <circle cx="11" cy="11" r="8"></circle>
                            <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                        </svg>
                    </label>
                </form>
                <div class="border-t border-t-neutral-700 w-full" id="searchResults">
                    <div class="text-xl flex items-center justify-between pl-4 pr-6 pt-6 pb-2">
                        <p>Recent History</p>
                        <a href="#" id="clearAllButton" class="hover:text-adm-lgrey duration-100 text-blue-600">Clear All</a>
                    </div>
                    <div id="resultBox" class="text-xl">
                        <?php if (isset($_SESSION['history_cleaned'])) {
                            echo "<div id='historyMessage' class='w-full py-3 text-2xl text-center'>" . $_SESSION['history_cleaned'] . "</div>";
                            unset($_SESSION['history_cleaned']);
                        } ?>
                    </div>
                </div>

            </div>
            <li class="w-[90%] rounded-2xl duration-200 cursor-pointer flex items-center mx-3 px-2 gap-8 hover:bg-dark py-3 my-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-compass">
                    <circle cx="12" cy="12" r="10"></circle>
                    <polygon fill='white' points="16.24 7.76 14.12 14.12 7.76 16.24 9.88 9.88 16.24 7.76"></polygon>
                </svg>
                Explore
            </li>
            <li class="w-[90%] rounded-2xl duration-200 cursor-pointer flex items-center mx-3 px-2 gap-8 hover:bg-dark py-3 my-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather rounded-xl feather-heart">
                    <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
                </svg>
                Notifications
            </li>
            <li class="w-[90%] rounded-2xl duration-200 cursor-pointer flex items-center mx-3 px-2 gap-8 hover:bg-dark py-3 my-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-square">
                    <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
                    <line x1="12" y1="8" x2="12" y2="16"></line>
                    <line x1="8" y1="12" x2="16" y2="12"></line>
                </svg>
                Create
            </li>
            <a href="profile.php" class="w-[90%] rounded-2xl duration-200 cursor-pointer flex items-center mx-3 px-2 gap-5 hover:bg-dark py-3 my-2">
                <?php
                if (!is_null($sidenav_profile->getProfilePicture())) : ?>
                    <img src="../resources/images/<?= $sidenav_profile->getProfilePicture() ?>" alt="" class="object-cover w-[35px] rounded-full border-2 border-adm-lgrey h-[35px]">
                <?php else : ?>
                    <img src="../resources/images/blankProfileImage.webp" alt="" class="object-cover w-[35px] rounded-full border-2 border-adm-grey h-[35px]">
                <?php endif ?>
                Profile</a>
            <li id="toggleMore" class="w-[90%] rounded-2xl duration-200 cursor-pointer flex items-center mx-3 px-2 gap-8 hover:bg-dark py-3 mt-4 relative">
                <?php require_once 'settings_block.php' ?>
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu">
                    <line id="l-one" x1="3" y1="12" x2="21" y2="12"></line>
                    <line id="l-two" x1="3" y1="6" x2="21" y2="6"></line>
                    <line id="l-three" x1="3" y1="18" x2="21" y2="18"></line>
                </svg>
                More
            </li>
        </ul>
    </div>
</div>
<script src="../js_jquery/sidenav.js"></script>