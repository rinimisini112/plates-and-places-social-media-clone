<?php
require_once '../../vendor/autoload.php';

use ProjektiBlog\public\classes\Like;
use ProjektiBlog\public\classes\SessionManager;

$session = new SessionManager();

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['postId'])) {
    $postId = $_POST['postId'];

    $like = new Like();
    $like->setPostId($postId);
    $like->setUserId($_SESSION['userid']);

    if (!$like->isLikedByUser()) {
        $like->create();
    } else {
        $like->unlike();
    }
    $isLiked = $like->isLikedByUser();
    $likeCount = $like->getLikeCountForPost();

    // Return JSON response
    header('Content-Type: application/json');
    echo json_encode(['likeCount' => $likeCount, 'isLiked' => $isLiked]);
    exit;
}
