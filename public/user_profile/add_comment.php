<?php
require_once '../../vendor/autoload.php';
require_once '../classes/Dbconn.php';

use ProjektiBlog\public\classes\Comment;
use ProjektiBlog\public\classes\CommentTag;
use ProjektiBlog\public\classes\SessionManager;
use ProjektiBlog\public\classes\Tag;

$session = new SessionManager();
$comment = new Comment();
$tags = new Tag();
$comment_tags = new CommentTag();
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['add_comment'])) {
    try {
        if (isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
            $media = $_FILES['image'];
        } elseif (isset($_FILES['video']) && $_FILES['video']['size'] > 0) {
            $media = $_FILES['video'];
        }

        if (isset($media)) {
            $comment->setUserId($_SESSION['userid'])
                ->setPostId($_POST['postId'])
                ->setContent($_POST['content'])
                ->setMediaImage($media);
        } else {
            $comment->setUserId($_SESSION['userid'])
                ->setPostId($_POST['postId'])
                ->setContent($_POST['content']);
        }


        if (!$comment->create()) {
            header('Location: ../../home/post.php?id=' . $_POST['postId']);
            die;
        }
        $comment = $comment->fetchSingleComment();

        if (!empty($_POST['content'])) {
            $comment_tags->setComment_id($comment->getId());

            preg_match_all('/#\w+/', $_POST['content'], $matches);
            $hashtags = $matches[0];
            foreach ($hashtags as $tag) {
                $tag_exists = $tags->getTagIdByName($tag);
                if ($tag_exists) {
                    $tagId = $tag_exists->getId();
                } else {
                    $tags->setName($tag);
                    $tags->create();
                    $tags = $tags->getTagIdByName($tag);
                    $tagId = $tags->getId();
                }

                $comment_tags->setTag_id($tagId);
                $comment_tags->create();
            }
        }
        $session->message('Post created succesfully');
        header('Location: ../../home/post.php?id=' . $_POST['postId']);
        exit;
    } catch (Exception $e) {
    }
}
