<?php

session_start();
require_once '../../vendor/autoload.php';

use ProjektiBlog\public\classes\Follower;

if (isset($_POST['follow'])) {
    $userId = $_POST['userid'];
    $action = $_POST['action'];
    $username = $_POST['username'];
    $follower = new Follower(); // Replace with your actual user class
    $follower->setUserId($userId);
    $follower->setFollowerUserId($_SESSION['userid']);
    var_dump($follower);
    if ($action == 'follow') {
        if (!$follower->youFollowUser($follower->getFollowerUserId())) {
            $follower->create();
            $_SESSION['follow_message'] = "You just followed, {$username}.";
            echo 'Sucess';
            $_SESSION['followed'] = '';
            var_dump($follower);
        }
    } elseif ($action === 'unfollow') {
        $follower->unfollow();
        echo 'Fail';
        $_SESSION['follow_message'] = "You just unfollowed, {$username}.";
    }

    header("Location: ../../home/account.php?user=" . $username);
} else {
    $_SESSION['message'] = 'Could not proccess action at the moment.';
    header("Location: ../../home/account.php?user=" . $username);
}
