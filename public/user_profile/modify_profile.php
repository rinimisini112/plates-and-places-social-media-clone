<?php
session_start();

use ProjektiBlog\public\classes\User;
use ProjektiBlog\public\classes\Profile;

require '../../vendor/autoload.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['update_profile'])) {
    try {
        $full_name = $_POST['fullName'] ? $_POST['fullName'] : null;
        $username = $_POST['userName'] ? $_POST['userName'] : null;
        $bio = $_POST['biography'] ? $_POST['biography'] : null;
        $webLink = $_POST['webLink'] ? $_POST['webLink'] : null;

        $user = new User();
        $profile = new Profile();

        $user = $user->fetchWithId($_SESSION['userid']);
        $profile = $profile->findProfileWithUserId($_SESSION['userid']);
        if (!is_null($full_name)) {
            $fullName = $full_name;
            $nameParts = preg_split('/\s+/', trim($fullName), 2);
            $user->setName($nameParts[0]);
            $user->setSurname(isset($nameParts[1]) ? $nameParts[1] : '');
            $profile->setFullName($full_name);
        }
        if (isset($_FILES['profilePicture']) && $_FILES['profilePicture']['size'] > 0) {
            $user->setImage($_FILES['profilePicture']);
        }
        if (isset($_FILES['cover_image']) && $_FILES['cover_image']['size'] > 0) {
            $profile->setImage($_FILES['cover_image']);
        } else {
            $profile->setImage(NULL);
        }
        $user->setUsername($username);

        if (!is_null($bio)) {
            $profile->setBio($bio);
        }

        if (!is_null($webLink)) {
            $profile->setWebsiteLink($webLink);
        }

        if (!$user->update()) {
            throw new Exception($user->getErrors());
        }

        if (!$profile->update()) {
            throw new Exception($profile->getErrors());
        }
        $info = $profile->fetchWithId($profile->getId());
        $_SESSION['success_message'] = 'Profile updated successfully';
        header('Location: ../../home/profile.php');
        exit;
    } catch (Exception $e) {
        $_SESSION['err_message'] = $e->getMessage();
        header('Location: ../../home/profileSetup.php');

        exit;
    }
}
