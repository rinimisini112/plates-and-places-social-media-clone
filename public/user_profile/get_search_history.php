<?php
ob_start();
session_start();
include '../classes/ImageHelper.php';
include '../classes/User.php';
include '../classes/ViewedUser.php';

use ProjektiBlog\public\classes\User;
use ProjektiBlog\public\classes\ViewedUser;

$sidenav_profile = new User();
$viewedUser = new ViewedUser();
$viewedUser->setUserId($_SESSION['userid']);
$search_history = $viewedUser->getSearchHistory();

$response = [];

if (isset($_GET['clearAll']) && $_GET['clearAll'] == 'true') {
    if ($viewedUser->clear()) {
        $_SESSION['history_cleaned'] = 'Search History Deleted!';
        $response['success'] = true;
        $response['message'] = 'Search History Deleted!';
    } else {
        $response['success'] = false;
        $response['message'] = 'Failed to delete search history.';
    }
}

if (!$search_history) {
    $response['content'] = '<div class="w-full py-3 text-2xl text-center">Nothing here yet!</div>';
} else {
    $phpGeneratedResults = '';
    foreach ($search_history as $search) {
        $user_id = $search->getViewedUsersId();
        $searchedUserInfo = $sidenav_profile->fetchWithId($user_id);
        $phpGeneratedResults .= '<div class="w-full px-3 hover:bg-dark-active py-1.5 duration-200">';
        $phpGeneratedResults .= '<a href="account.php?user=' . $searchedUserInfo->getUsername() . '&searched" class="flex items-center gap-3">';
        $phpGeneratedResults .= '<img src="../resources/images/' . $searchedUserInfo->getProfilePicture() .  '" alt="" class="w-[50px] h-[50px] rounded-full object-cover">';
        $phpGeneratedResults .= "<div>";
        $phpGeneratedResults .= '<p class="text-neutral-400">@' . $searchedUserInfo->getUsername() . "</p>";
        $phpGeneratedResults .= '<p class="-translate-y-1 pl-3">' . $searchedUserInfo->getName() . ' ' . $searchedUserInfo->getSurname() . "</p>";
        $phpGeneratedResults .= "</div></a></div>";
    }
    $response['content'] = $phpGeneratedResults;
}

echo json_encode($response);
