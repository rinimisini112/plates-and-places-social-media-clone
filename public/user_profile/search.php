<?php
require '../../vendor/autoload.php';

use ProjektiBlog\public\classes\User;

if (isset($_GET['query'])) {
    try {
        $searchResult = $_GET['query'];
        $user = new User();
        $users = $user->searchUsers($searchResult);
        if (!empty($users)) {
            $users_results = [];
            foreach ($users as $singleUser) {
                $singleUserArray = [
                    'fullName' => $singleUser->getName() . ' ' . $singleUser->getSurname(),
                    'username' => $singleUser->getUsername(),
                    'image' => $singleUser->getProfilePicture(),
                ];
                $users_results[] = $singleUserArray;
            }
        } else {
            $users_results = [];
        }
        // Assuming $users_results is the array of formatted results
        echo json_encode($users_results);
    } catch (Exception $e) {
        echo 'Error fetching users ' . $e->getMessage();
    }
}
