<?php
require_once '../../vendor/autoload.php';
require_once '../classes/Dbconn.php';

use ProjektiBlog\public\classes\Post;
use ProjektiBlog\public\classes\PostTag;
use ProjektiBlog\public\classes\SessionManager;
use ProjektiBlog\public\classes\Tag;

$session = new SessionManager();
$post = new Post();
$tags = new Tag();
$post_tags = new PostTag;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        if (isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
            $media = $_FILES['image'];
        } elseif (isset($_FILES['video']) && $_FILES['video']['size'] > 0) {
            $media = $_FILES['video'];
        }

        if (isset($media)) {
            $post->setUserId($_SESSION['userid'])
                ->setContent($_POST['content'])
                ->setMediaImage($media);
        } else {
            $post->setUserId($_SESSION['userid'])
                ->setContent($_POST['content']);
        }
        if ($post->create()) {

            $post = $post->fetchPostForUser($_POST['content']);

            if (!empty($_POST['content'])) {
                $post_tags->setPostId($post->getId());

                preg_match_all('/#\w+/', $_POST['content'], $matches);
                $hashtags = $matches[0];
                foreach ($hashtags as $tag) {
                    $tag_exists = $tags->getTagIdByName($tag);
                    if ($tag_exists) {
                        $tagId = $tag_exists->getId();
                    } else {
                        $tags->setName($tag);
                        $tags->create();
                        $tags = $tags->getTagIdByName($tag);
                        $tagId = $tags->getId();
                    }

                    $post_tags->setTagId($tagId);
                    $post_tags->create();
                }
            }
            $session->message('Post created succesfully');
            header('Location: ../../home/index.php');
            exit;
        } else {
            header('Location: ../../home/index.php');
        }
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
}
