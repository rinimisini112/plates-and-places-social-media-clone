<?php
session_start();
require_once '../../vendor/autoload.php';

use ProjektiBlog\public\classes\User;
use ProjektiBlog\public\classes\Follower;

$userContent = [];

$randomUser = new User();
$randomUsers = $randomUser->getRandomUsers();
$follower = new Follower();
$follower->setFollowerUserId($_SESSION['userid']);
$currentUser = new User();
$currentUser = $currentUser->fetchWithId($_SESSION['userid']);
foreach ($randomUsers as $randomU) {
    if ($follower->youFollowUser($randomU->getId())) {
        continue;
    }
    if ($currentUser->getId() === $randomU->getId()) {
        continue;
    }
    $truncated_username = $randomU->getUsername();
    if (strlen($truncated_username) > 13) {
        $truncated_username = substr($truncated_username, 0, 11) . '...';
    }
    $userContent[] = [
        'truncatedUsername' => $truncated_username,
        'username' => $randomU->getUsername(),
        'userId' => $randomU->getId(),
        'profilePicture' => $randomU->getProfilePicture()
    ];
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $user_being_followed = new User();
    $follow = new Follower();
    $follow->setFollowerUserId($_SESSION['userid']);
    $follow->setUserId($_POST['userId']);
    $user_being_followed = $user_being_followed->fetchWithId($_POST['userId']);
    $truncated_username = $user_being_followed->getUsername();
    if (strlen($truncated_username) > 18) {
        $truncated_username = substr($truncated_username, 0, 15) . '...';
    }
    if ($follow->create()) {
        $_SESSION['preference_bar_message'] = 'You followed @' . $truncated_username;
        echo json_encode(['success' => true, 'userContent' => $userContent, 'message' => 'Follow action successful']);
        exit; // Exit to prevent additional JSON encoding
    } else {
        echo json_encode(['success' => false, 'userContent' => $userContent, 'message' => 'Follow action failed']);
        exit; // Exit to prevent additional JSON encoding
    }
}
// If not a POST request, return the userContent JSON
echo json_encode(['success' => true, 'userContent' => $userContent]);
