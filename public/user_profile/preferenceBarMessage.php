<?php
session_start();

if (isset($_SESSION['preference_bar_message'])) {
    echo json_encode(['success' => true, 'message' => $_SESSION['preference_bar_message']]);
    unset($_SESSION['preference_bar_message']);
} else {
    echo json_encode(['success' => false]);
}
