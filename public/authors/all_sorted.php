<?php 
include "../include/functions.php";
?>
<tr>
                            <th>Application ID</th>
                            <th>Username</th>
                            <th>Application Date</th>
                            <th>Email</th>
                            <th>Details</th>
                            <th>Actions</th>
                        </tr>
                        <?php
                        if (isset($_GET['sort'])) {
                            $sort = $_GET['sort'];
                            switch ($sort) {
                                case 'name-asc':
                                    // Sort by name in ascending order
                                    $orderBy = 'name ASC';
                                    $sortingMessage = 'Name A-Z';
                                    break;
                                case 'name-desc':
                                    // Sort by name in descending order
                                    $orderBy = 'name DESC';
                                    $sortingMessage = 'Name Z-A';
                                    break;
                                case 'date-asc':
                                    // Sort by date in ascending order
                                    $orderBy = 'application_date ASC';
                                    $sortingMessage = 'Apply Date ASC';
                                    break;
                                case 'date-desc':
                                    // Sort by date in descending order
                                    $orderBy = 'application_date DESC';
                                    $sortingMessage = 'Apply Date DESC';
                                    break;
                                default:
                                    // Handle invalid sort criteria here, e.g., show the default sorting.
                                    $orderBy = 'name ASC';
                                    $sortingMessage = 'Sort By <i class="fa-solid fa-arrow-down"></i>';
                                    break;
                            }
                            $sortedApplications = getSortedApplications($orderBy); // Modify this function based on your database schema


                            // Separate applications by status
                            $unacceptedApplications = array();
                            $acceptedApplications = array();
                            $rejectedApplications = array();


                            // Define a counter variable
                            $rowCount = 0;



                            // Output accepted applications
                            foreach ($sortedApplications as $application) {
                                renderApplicationRow($application, $rowCount, 'Accepted');
                                $rowCount++;
                            }
                        }