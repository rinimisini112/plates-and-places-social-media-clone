<div id="sideNav" class="w-1/5 transition-all duration-200 overflow-hidden fixed top-0 left-0 shadow-2xl drop-shadow-md shadow-slate-600 h-full bg-dark text-white flex pt-4 flex-col justify-between ">
                <div class=" flex flex-col">
                <p class=" border-b border-b-adm-lgrey w-[70%] mx-auto py-3 text-xl text-center">Navigate</p>
                <a href="#" class="text-xl pl-4 py-5 w-[82%] mb-2 mt-4 shadow-lg bg-[rgb(127,145,183)] mx-auto rounded-xl text-left drop-shadow-md tracking-wide" id="apps-link">
                    <h1 class="flex gap-4 items-start"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather drop-shadow-xl feather-check-square">
                            <polyline points="9 11 12 14 22 4"></polyline>
                            <path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path>
                        </svg>
                        Applications
                    </h1>
                </a>
                <a href="active_authors.php" class="text-xl pl-4 py-4 w-[80%] bg-[rgb(53,58,68)] hover:bg-[rgb(62,69,77)] duration-200 my-2 drop-shadow-md shadow-lg mx-auto rounded-xl text-left tracking-wide" id="authors-link">
                    <h1 class="flex gap-4 items-start"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="rgb(200,200,200)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather drop-shadow-xl feather-users">
                            <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                            <circle cx="9" cy="7" r="4"></circle>
                            <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                        </svg>
                        Users

                    </h1>
                </a>
                <a href="active_authors.php" class="text-xl pl-4 py-4 w-[80%] bg-[rgb(53,58,68)] hover:bg-[rgb(62,69,77)] duration-200 my-2 drop-shadow-md shadow-lg mx-auto rounded-xl text-left tracking-wide" id="authors-link">
                    <h1 class="flex gap-4 items-start"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="rgb(200,200,200)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather drop-shadow-xl feather-list">
                            <line x1="8" y1="6" x2="21" y2="6"></line>
                            <line x1="8" y1="12" x2="21" y2="12"></line>
                            <line x1="8" y1="18" x2="21" y2="18"></line>
                            <line x1="3" y1="6" x2="3.01" y2="6"></line>
                            <line x1="3" y1="12" x2="3.01" y2="12"></line>
                            <line x1="3" y1="18" x2="3.01" y2="18"></line>
                        </svg>
                        Categories

                    </h1>
                </a>
                <a href="active_authors.php" class="text-xl pl-4 py-4 w-[80%] bg-[rgb(53,58,68)] hover:bg-[rgb(62,69,77)] duration-200 my-2 drop-shadow-md shadow-lg mx-auto rounded-xl text-left tracking-wide" id="authors-link">
                    <h1 class="flex gap-4 items-start"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="rgb(200,200,200)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather drop-shadow-xl feather-layers">
                            <polygon points="12 2 2 7 12 12 22 7 12 2"></polygon>
                            <polyline points="2 17 12 22 22 17"></polyline>
                            <polyline points="2 12 12 17 22 12"></polyline>
                        </svg>
                        Posts

                    </h1>
                </a>
                <a href="active_authors.php" class="text-xl pl-4 py-4 w-[80%] bg-[rgb(53,58,68)] hover:bg-[rgb(62,69,77)] duration-200 my-2 drop-shadow-md shadow-lg mx-auto rounded-xl text-left tracking-wide" id="authors-link">
                    <h1 class="flex gap-4 items-start"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="rgb(200,200,200)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather drop-shadow-xl feather-clipboard">
                            <path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path>
                            <rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect>
                        </svg>
                        Authors Board

                    </h1>
                </a>
            </div>
            <div class="w-full py-6 bg-[#2f333c] flex items-center justify-center">
                <?php if(isset($_SESSION['username'])) :?>
                <p class="text-lg text-adm-lgrey">Logged in as : <?= $_SESSION['username']?></p>
                <?php else :?>
                <p class="text-lg text-adm-lgrey">Logged in as : <?= $_SESSION['fullName']?></p>
                <?php endif ?>
            </div>
            </div>