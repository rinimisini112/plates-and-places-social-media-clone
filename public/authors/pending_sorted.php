<?php
include "../include/functions.php";
?>
<tr>
                            <th>Application ID</th>
                            <th>Username</th>
                            <th>Application Date</th>
                            <th>Email</th>
                            <th>Details</th>
                            <th>Actions</th>

                            <?php
                             if (isset($_GET['sort'])) {
                                $sort = $_GET['sort'];
                                switch ($sort) {
                                    case 'name-asc':
                                        // Sort by name in ascending order
                                        $orderBy = 'name ASC';
                                        $sortingMessage = 'Name A-Z';
                                        break;
                                    case 'name-desc':
                                        // Sort by name in descending order
                                        $orderBy = 'name DESC';
                                        $sortingMessage = 'Name Z-A';
                                        break;
                                    case 'date-asc':
                                        // Sort by date in ascending order
                                        $orderBy = 'application_date ASC';
                                        $sortingMessage = 'Apply Date ASC';
                                        break;
                                    case 'date-desc':
                                        // Sort by date in descending order
                                        $orderBy = 'application_date DESC';
                                        $sortingMessage = 'Apply Date DESC';
                                        break;
                                    default:
                                        // Handle invalid sort criteria here, e.g., show the default sorting.
                                        $orderBy = 'name ASC';
                                        break;
                                }
                                $sortedAccepted = getSortedPenginApplications($orderBy);
                                 // Define a counter variable
                            $rowCount = 0;

                            // Output accepted applications
                            foreach ($sortedAccepted as $application) {
                                $appId = (int)$application['applyid']; // Cast to integer
                                $name = $application['name'];
                                $surName = $application['surname'];
                                $appDate = $application['application_date'];
                                $email = $application['email'];
                                $details = $application['details'];

                                // Determine the row class based on the counter
                                $rowClass = ($rowCount % 2 == 0) ? 'l-gray' : 'd-gray';

                                // Output the row with data and class
                                echo "<tr class='$rowClass'>";
                                echo "<td>$appId</td>";
                                echo "<td>" . $name . " " . $surName . "</td>";
                                echo "<td>$appDate</td>";
                                echo "<td>$email</td>";
                                echo "<td>$details</td>";
                                echo "<td class='status-cell'>";
                                echo "<i class='fa-solid fa-square-check accept-button' data-application-id='" . $appId . "' data-status='accepted'></i>";
                                echo "<i class='fa-solid fa-trash-can-arrow-up reject-button' data-application-id='" . $appId . "' data-status='rejected'></i>";
                                echo "</td>";
                                echo "</tr>";

                                $rowCount++;
                            }
                            } 