<?php
include "../functions.php";

$sortingMessage = 'Sort By <i class="fa-solid fa-arrow-down"></i>'; // Set a default value

if (isset($_GET['argument']) && $_GET['argument'] == 'logout') {
    session_destroy();
    header("Location: ../index.php");
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $applicationId = $_POST['applicationId'];
    $newStatus = $_POST['newStatus'];
    updateApplicationStatus($applicationId, $newStatus);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../../dist/output.css">
    <script src="https://kit.fontawesome.com/1fc3754b96.js" crossorigin="anonymous"></script>
    <script src="../../js_jquery/jquery.js"></script>
    <script src="../../js_jquery/minified/TweenMax.min.js"></script>
    <script src="../../js_jquery/minified/gsap.min.js"></script>
    <script src="../../js_jquery/jquery.validate.js"></script>
    <script src="../../js_jquery/jquery.validate.min.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=League+Spartan:wght@300;400;700&family=Open+Sans:wght@300;500;700&family=Oswald:wght@600&family=PT+Mono&family=Playfair+Display:wght@400;600;700&family=Roboto+Mono:wght@700&family=Roboto+Slab:wght@300;400;700&family=Roboto:wght@300;500;700&family=Slabo+27px&family=Source+Sans+3:wght@300&family=Ysabeau+SC:wght@300;600&display=swap');

        body {
            overflow-y: unset;
        }

        .login_wrapper {
            justify-content: center;
            width: 40%;
        }

        .menu li:nth-child(3) a::after {
            transform: scaleX(1);
        }
    </style>
</head>

<body class="h-full">
    <header id="w-full h-full relative text-black bg-[rgb(245, 245, 245)] flex items-center justify-center pt-[10rem]">
        <article class="w-full h-full bg-white flex mb-12">
            <?php include 'inc/sidenav.php' ?>
            <div id="panelBody" class="w-4/5 transition-all duration-200 ml-auto bg-adm-lgrey relative">
                <div class="flex justify-between items-center py-3 px-6 sticky top-0 z-30 bg-adm-lgrey">
                    <p class="text-3xl drop-shadow-sm font-thin text-[#a5a5a5]">Plates&Dates</p>
                    <div class='flex items-center'>
                        <form action="" class="relative flex items-center group">
                            <input placeholder="Search Panel" id='searchBtn' class="relative placeholder:text-gray-400 h-8 border-none drop-shadow-lg bg-adm-white" type="search" name="search" id="search">
                            <span class=" bg-dark w-10 h-10 rounded-full transform shadow-sm -translate-x-6 group-hover:bg-[rgb(127,145,183)] duration-300 flex items-center justify-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="rgb(200,200,200)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather drop-shadow-xl feather-search">
                                    <circle cx="11" cy="11" r="8"></circle>
                                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                </svg>
                            </span>
                            <span class="absolute left-0 bottom-[2px] w-[78%] drop-shadow-md  h-[2px] bg-[#cb7bbfe4] duration-300 transform scale-0 group-hover:scale-100 group-active:scale-100 group-focus:scale-100"></span>
                        </form>
                        <svg id="toggleSideNav" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="rgb(53,58,68)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather mr-4 hover:stroke-[rgb(127,145,183)] hover:fill-[rgb(127,145,183)] duration-200 cursor-pointer hover:scale-110 feather-align-left">
                            <line x1="17" y1="10" x2="3" y2="10"></line>
                            <line x1="21" y1="6" x2="3" y2="6"></line>
                            <line x1="21" y1="14" x2="3" y2="14"></line>
                            <line x1="17" y1="18" x2="3" y2="18"></line>
                        </svg>
                        <div class="relative group">
                            <svg id="toggleAdminDropdown" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="rgb(53,58,68)" stroke="rgb(53,58,68)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user hover:stroke-adm-active hover:fill-adm-active cursor-pointer hover:scale-110 duration-200">
                                <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                <circle cx="12" cy="7" r="4"></circle>
                            </svg>
                            <ul id="dropdownList" class="absolute right-0 block w-32 bg-dark shadow-2xl text-adm-white z-50">
                                <li class="py-2 px-3 flex items-center justify-between border-b border-b-adm-white hover:bg-[#2f333c] duration-200 cursor-pointer"><a href="">Profile</a>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user">
                                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="12" cy="7" r="4"></circle>
                                    </svg>
                                </li>
                                <li class="py-2 px-3 flex items-center justify-between border-b border-b-adm-white hover:bg-[#2f333c] duration-200 cursor-pointer"><a href="">Settings</a>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings">
                                        <circle cx="12" cy="12" r="3"></circle>
                                        <path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path>
                                    </svg>
                                </li>
                                <a href="../auth/logout.inc.php?arg=logout">
                                    <li class="py-2 px-3 flex items-center justify-between hover:bg-[#2f333c] duration-200 cursor-pointer">Logout
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out">
                                            <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                                            <polyline points="16 17 21 12 16 7"></polyline>
                                            <line x1="21" y1="12" x2="9" y2="12"></line>
                                        </svg>
                                    </li>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class=" w-[90%] mt-12 mx-auto flex items-center justify-evenly">
                    <a href="#" class="h-[6.5rem] w-[22%] " id="all-link">
                        <li class="text-center flex flex-col items-start py-2 gap-3 w-full h-full  bg-gradient-to-r from-[#cb7bbfe4] via-[#bf72b4] to-[#a36199] shadow-inner shadow-[#ffffff5a] text-adm-white rounded-sm drop-shadow-lg px-4 ">
                            <div class="border-b border-b-adm-lgrey flex items-start gap-8 w-full h-1/2">
                                <span class="w-14 h-14 transform -translate-y-4 flex items-center justify-center ml-4 rounded-sm drop-shadow-md bg-gradient-to-r from-blue-400 via-blue-500 to-blue-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-square">
                                        <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
                                        <line x1="12" y1="8" x2="12" y2="16"></line>
                                        <line x1="8" y1="12" x2="16" y2="12"></line>
                                    </svg>
                                </span>
                                All apps
                            </div>
                            <p class="flex gap-3 items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="blue" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-circle">
                                    <circle cx="12" cy="12" r="10"></circle>
                                    <line x1="12" y1="8" x2="12" y2="12"></line>
                                    <line x1="12" y1="16" x2="12.01" y2="16"></line>
                                </svg>
                                58 Total
                            </p>
                        </li>
                    </a>
                    <a href="#" class="h-24 w-1/5 group" id="pending-link">
                        <li class="text-center flex py-2 flex-col hover:bg-[rgb(217,215,215)] duration-200  items-start gap-2 bg-adm-white rounded-sm drop-shadow-lg px-4 ">
                            <div class="border-b border-b-adm-lgrey group-hover:border-b-adm-grey duration-300 flex items-start gap-8 w-full h-1/2">
                                <span class="w-12 h-12 transform -translate-y-4 flex items-center justify-center ml-4 rounded-sm drop-shadow-md bg-gradient-to-r from-gray-400 via-gray-400 to-gray-500">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-minus-square">
                                        <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
                                        <line x1="8" y1="12" x2="16" y2="12"></line>
                                    </svg>
                                </span>
                                Pending
                            </div>
                            <p class="flex gap-3 items-center"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="gray" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-circle">
                                    <circle cx="12" cy="12" r="10"></circle>
                                    <line x1="12" y1="8" x2="12" y2="12"></line>
                                    <line x1="12" y1="16" x2="12.01" y2="16"></line>
                                </svg>
                                12 Pending</p>
                        </li>
                    </a>
                    <a href="#" class=" h-24 w-1/5 group" id="accepted-link">
                        <li class="text-center flex py-2 flex-col hover:bg-[rgb(217,215,215)] duration-200 items-start gap-2 bg-adm-white rounded-sm drop-shadow-lg px-4 ">
                            <div class="border-b border-b-adm-lgrey group-hover:border-b-adm-grey duration-300 flex items-start gap-8 w-full h-1/2">
                                <span class="w-12 h-12 transform -translate-y-4 flex items-center justify-center ml-4 rounded-sm drop-shadow-md bg-gradient-to-r from-green-500 via-green-500 to-green-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-square">
                                        <polyline points="9 11 12 14 22 4"></polyline>
                                        <path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path>
                                    </svg>
                                </span>
                                Accepted
                            </div>
                            <p class="flex gap-3 items-center"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="green" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-circle">
                                    <circle cx="12" cy="12" r="10"></circle>
                                    <line x1="12" y1="8" x2="12" y2="12"></line>
                                    <line x1="12" y1="16" x2="12.01" y2="16"></line>
                                </svg>
                                30 Accepted
                                </p>
                        </li>
                    </a>
                    <a href="#" class=" h-24 w-1/5 group" id="rejected-link">
                        <li class="text-center flex py-2 flex-col hover:bg-[rgb(217,215,215)] duration-200 items-start gap-2 bg-adm-white rounded-sm drop-shadow-lg px-4 ">
                            <div class="border-b border-b-adm-lgrey group-hover:border-b-adm-grey duration-300 flex items-start gap-8 w-full h-1/2">
                                <span class="w-12 h-12 transform -translate-y-4 flex items-center justify-center ml-4 rounded-sm drop-shadow-md bg-gradient-to-r from-red-400 via-red-500 to-red-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-square">
                                        <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
                                        <line x1="9" y1="9" x2="15" y2="15"></line>
                                        <line x1="15" y1="9" x2="9" y2="15"></line>
                                    </svg>
                                </span>
                                Rejected
                            </div>
                            <p class="flex gap-3 items-center"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-circle">
                                    <circle cx="12" cy="12" r="10"></circle>
                                    <line x1="12" y1="8" x2="12" y2="12"></line>
                                    <line x1="12" y1="16" x2="12.01" y2="16"></line>
                                </svg>
                                16 Rejected</p>
                        </li>
                    </a>
                </ul>
                <div class="w-[90%] mx-auto flex items-center px-2 justify-between py-1  mt-8 bg-dark shadow-lg rounded-t-lg">
                    <form action="" method="post" class="flex items-center group">
                        <input class="border-none drop-shadow-md h-8 rounded-tl-md bg-adm-lgrey placeholder:text-gray-400" type="search" name="" id="" placeholder="Search Applications">
                        <span class=" bg-[rgb(127,145,183)] w-10 h-10 rounded-full transform shadow-lg -translate-x-6 duration-200 flex items-center justify-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="rgb(200,200,200)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather drop-shadow-xl feather-search">
                                <circle cx="11" cy="11" r="8"></circle>
                                <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                            </svg>
                        </span>
                    </form>
                    <div class="relative w-1/5 flex items-center justify-center py-1 rounded-tr-md bg-adm-lgrey  duration-200">
                        <p id="sortButton" class="w-full text-center"><?php echo $sortingMessage; ?></p>
                        <div id="sortOptions" class=" absolute right-0 top-8 flex flex-col w-full h-auto overflow-hidden bg-adm-grey text-adm-lgrey">
                            <a class="py-2 px-3" href="#" data-sort="default">Default</a>
                            <a class="py-2 px-3" href="#" data-sort="name-asc">Name A-Z</a>
                            <a class="py-2 px-3" href="#" data-sort="name-desc">Name Z-A</a>
                            <a class="py-2 px-3" href="#" data-sort="date-asc">Apply Date ASC</a>
                            <a class="py-2 px-3" href="#" data-sort="date-desc">Apply Date DESC</a>
                        </div>
                    </div>
                </div>

                <div id="dynamic-content">
                    <table id="applications">
                        <!-- Table headers and rows will be loaded here -->
                    </table>
                </div>
            </div>
        </article>
    </header>
    <script src="../../js_jquery/authors.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const sortOptions = document.getElementById('sortOptions');
            const sortOptionsButton = document.getElementById('sortButton');

            // GSAP animation
            const tl = gsap.timeline({
                paused: true
            });
            tl.to(sortOptions, {
                opacity: 1,
                display: 'flex',
                duration: 0.2
            });
            tl.to(sortOptions, {
                opacity: 0,
                display: 'none',
                duration: 0.2,
                delay: 0.2
            });

            // Event listener
            if (sortOptionsButton) {
                sortOptionsButton.addEventListener('click', () => tl.reversed(!tl.reversed()));
            }
            const sideNavToggle = document.getElementById('toggleSideNav');
            const sideNav = document.getElementById('sideNav');
            const panelBody = document.getElementById('panelBody');

            function toggleSideNav() {
                if (sideNav.classList.contains('w-1/5')) {
                    sideNav.classList.remove('w-1/5');
                    sideNav.classList.add('w-0');
                    panelBody.classList.remove('w-4/5');
                    panelBody.classList.add('w-full');
                } else {
                    sideNav.classList.add('w-1/5');
                    sideNav.classList.remove('w-0');
                    panelBody.classList.add('w-4/5');
                    panelBody.classList.remove('w-full');
                }
            }
            sideNavToggle.addEventListener('click', toggleSideNav);
            const dropdownContainer = document.getElementById('dropdownList');
            const svgIcon = document.getElementById('toggleAdminDropdown');

            // Initially hide the dropdown
            gsap.set(dropdownContainer, {
                opacity: 0,
                display: 'none'
            });

            let isOpen = false;

            function toggleDropdown() {
                if (isOpen) {
                    // Close dropdown
                    gsap.to(dropdownContainer, {
                        opacity: 0,
                        display: 'none',
                        duration: 0.1
                    });
                } else {
                    // Open dropdownContainer
                    gsap.to(dropdownContainer, {
                        opacity: 1,
                        display: 'block',
                        duration: 0.2
                    });
                }

                isOpen = !isOpen;
            }

            function closeDropdown(e) {
                if (!dropdownContainer.contains(e.target) && isOpen && !svgIcon.contains(e.target)) {
                    toggleDropdown();
                }
            }

            // Click event for the SVG icon
            svgIcon.addEventListener('click', toggleDropdown);

            // Click event for the document to close dropdown when clicking outside
            document.addEventListener('click', closeDropdown);
        });
        $('#logout').click(function(event) {
            $.ajax({
                url: 'authors_dashboard.php?argument=logout',
                success: function(data) {
                    window.location.href = '../index.php';
                },
                error: function(xhr, status, error) {
                    console.log(error);
                }
            });
        });
        // Get the sortingMessage from PHP
        var sortingMessage = <?php echo json_encode($sortingMessage); ?>;

        // Set the button's text
        document.getElementById('sortButton').innerHTML = sortingMessage;
    </script>

</body>

</html>