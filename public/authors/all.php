<?php 
include "../include/functions.php";
?>
<div id="all_applications">
                <div class="info_table">
                    <table id="applications">
                        <tr>
                            <th>Application ID</th>
                            <th>Username</th>
                            <th>Application Date</th>
                            <th>Email</th>
                            <th>Details</th>
                            <th>Actions</th>
                        </tr>
                        <?php
                       
                            renderNonSortedApps();
                        
                        ?>
                    </table>
                </div>
            </div>
            <script>
                    // Get the sortingMessage from PHP
        var sortingMessage = <?php echo json_encode($sortingMessage); ?>;

// Set the button's text
document.getElementById('sortButton').innerHTML = sortingMessage;
            </script>