<?php
include "../include/functions.php";
$sortingMessage = 'Sort By <i class="fa-solid fa-arrow-down"></i>'; // Set a default value

if (isset($_GET['argument']) && $_GET['argument'] == 'logout') {
    session_destroy();
    header("Location: ../index.php");
    exit;
}

if (!isset($_SESSION['user'])) {
    header("Location: ../index.php");
    exit;
} elseif ($_SESSION['user']['role'] != 2) {
    header("Location: ../index.php");
    exit;
}



// Check if there is a sort parameter in the URL

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/authors.css">
    <script src="https://kit.fontawesome.com/1fc3754b96.js" crossorigin="anonymous"></script>
    <script src="../js_jquery/jquery.js"></script>
    <script src="../js_jquery/jquery.validate.js"></script>
    <script src="../js_jquery/jquery.validate.min.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=League+Spartan:wght@300;400;700&family=Open+Sans:wght@300;500;700&family=Oswald:wght@600&family=PT+Mono&family=Playfair+Display:wght@400;600;700&family=Roboto+Mono:wght@700&family=Roboto+Slab:wght@300;400;700&family=Roboto:wght@300;500;700&family=Slabo+27px&family=Source+Sans+3:wght@300&family=Ysabeau+SC:wght@300;600&display=swap');

        body {
            overflow-y: unset;
        }

        .login_wrapper {
            justify-content: center;
            width: 40%;
        }

        .menu li:nth-child(3) a::after {
            transform: scaleX(1);
        }
    </style>
</head>

<body>
    <header id="header_container">
        <?php
        include "../include/white_navigation.php";
        ?>
        <article class="form_container">
            <div class="dash_title">
                <a href="authors_dashboard.php" id="apps-link"><h1>Applications</h1></a>
                <a href="#" class="reactive" id="authors-link"><h1>Authors Board</h1></a>
                </div>
                <div class="info_table">
                    <table id="applications">
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Posts</th>
                            <th>Total Likes</th>
                            <th>Actions</th>
                        </tr>
                        <?php
                       $authors = getAuthorsAdmin();
                       foreach($authors as $application) {
                        $rowCount=0;
                       $name = $application['name'];
                       $surName = $application['surname'];
                       $email = $application['email'];
                       $blogPosts = $application['postid'];
                       $likes = $application['likes'];
                   
                       // Determine the row class based on the counter
                       $rowClass = ($rowCount % 2 == 0) ? 'l-gray' : 'd-gray';
                   
                       // Output the row with data and class
                       echo "<tr class='$rowClass'>";
                       echo "<td>" . $name . " " . $surName . "</td>";
                       echo "<td>$email</td>";
                       echo "<td>$blogPosts</td>";
                       echo "<td>$likes</td>";
                       
                       echo "</tr>";                        
                    }
                        ?>
                    </table>
            </div>
        </article>
    </header>
    <script>
       
        $('#logout').click(function (event) {
    $.ajax({
        url: 'active_authors.php?argument=logout',
        success: function (data) {
            window.location.href = '../index.php';
        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
});
    </script>

</body>

</html>