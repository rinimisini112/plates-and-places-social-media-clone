<?php
include "../include/functions.php";
?>
            <div id="all_applications">
                <div class="info_table">
                    <table id="applications">
                    <tr>
                            <th>Application ID</th>
                            <th>Username</th>
                            <th>Application Date</th>
                            <th>Email</th>
                            <th>Details</th>
                            <th>Actions</th>

<?php
                                
                                $applications = getPendingAdmin();

                            // Define a counter variable
                            $rowCount = 0;

                            // Output accepted applications
                            foreach ($applications as $application) {
                                $appId = (int)$application['applyid']; // Cast to integer
                                $name = $application['name'];
                                $surName = $application['surname'];
                                $appDate = $application['application_date'];
                                $email = $application['email'];
                                $details = $application['details'];

                                // Determine the row class based on the counter
                                $rowClass = ($rowCount % 2 == 0) ? 'l-gray' : 'd-gray';

                                // Output the row with data and class
                                echo "<tr class='$rowClass'>";
                                echo "<td>$appId</td>";
                                echo "<td>" . $name . " " . $surName . "</td>";
                                echo "<td>$appDate</td>";
                                echo "<td>$email</td>";
                                echo "<td>$details</td>";
                                echo "<td class='status-cell'>";
                                echo "<i class='fa-solid fa-square-check accept-button' data-application-id='" . $appId . "' data-status='accepted'></i>";
                                echo "<i class='fa-solid fa-trash-can-arrow-up reject-button' data-application-id='" . $appId . "' data-status='rejected'></i>";
                                echo "</td>";
                                echo "</tr>";

                                $rowCount++;
                            }
                            ?>
                    </table>
                </div>
            </div>
        </article>
    </header>
    <script src="../js_jquery/authors.js"></script>
    <script>
    // Get the sortingMessage from PHP
    var sortingMessage = <?php echo json_encode($sortingMessage); ?>;

    // Set the button's text
    document.getElementById('sortButton').innerHTML = sortingMessage;
</script>
</body>

</html>