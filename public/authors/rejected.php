<?php
include "../include/functions.php";
$sortingMessage = 'Sort By <i class="fa-solid fa-arrow-down"></i>'; // Set a default value

if (isset($_GET['argument']) && $_GET['argument'] == 'logout') {
    session_destroy();
    echo "../index.php";
    exit;
}
if (!isset($_SESSION['user'])) {
    header("Location: ../index.php");
} elseif ($_SESSION['user']['role'] != 2) {
    header("Location: ../index.php");
}
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['applicationId']) && isset($_POST['newStatus'])) {
    // Get the applicationId and newStatus from POST data
    $applicationId = (int)$_POST['applicationId'];
    $newStatus = (int)$_POST['newStatus'];

    // Call the updateApplicationStatus function
    $result = updateApplicationStatus($applicationId, $newStatus);

    // Send a JSON response back to JavaScript
    header('Content-Type: application/json');
    if ($result) {
        echo json_encode(['success' => true]);
    } else {
        echo json_encode(['success' => false]);
    }
    exit; // Don't proceed with HTML rendering after handling the AJAX request.
}
?>
            <div id="all_applications">
                <div class="info_table">
                    <table id="applications">
                        <tr>
                            <th>Application ID</th>
                            <th>Username</th>
                            <th>Application Date</th>
                            <th>Email</th>
                            <th>Details</th>
                            <th>Actions</th>

                            <?php
                                
                                $applications = getRejectedAdmin();

                            // Define a counter variable
                            $rowCount = 0;

                            // Output accepted applications
                            foreach ($applications as $application) {
                                $appId = (int)$application['applyid']; // Cast to integer
                                $name = $application['name'];
                                $surName = $application['surname'];
                                $appDate = $application['application_date'];
                                $email = $application['email'];
                                $details = $application['details'];

                                // Determine the row class based on the counter
                                $rowClass = ($rowCount % 2 == 0) ? 'l-gray' : 'd-gray';

                                // Output the row with data and class
                                echo "<tr class='$rowClass'>";
                                echo "<td>$appId</td>";
                                echo "<td>" . $name . " " . $surName . "</td>";
                                echo "<td>$appDate</td>";
                                echo "<td>$email</td>";
                                echo "<td>$details</td>";
                                echo "<td class='rejected_data'>Rejected</td>";
                                echo "</tr>";

                                $rowCount++;
                            }
                        
                            ?>
                    </table>
                </div>
            </div>
        </article>
    </header>
    <script src="../js_jquery/authors.js"></script>
    <script>
    // Get the sortingMessage from PHP
    var sortingMessage = <?php echo json_encode($sortingMessage); ?>;

    // Set the button's text
    document.getElementById('sortButton').innerHTML = sortingMessage;
</script>
</body>

</html>