<?php

use ProjektiBlog\public\classes\SessionManager;

require '../classes/SessionManager.php';
$session = new SessionManager();
if (isset($_GET['arg'])) {

    $session->logout();
    header("Location: ../../index.php");
    exit;
}
