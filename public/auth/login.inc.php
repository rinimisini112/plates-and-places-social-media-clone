    <?php
    require "../../vendor/autoload.php";

    use ProjektiBlog\public\classes\Profile;
    use ProjektiBlog\public\classes\LoginUser;
    use ProjektiBlog\public\classes\SessionManager;

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $login_user = new LoginUser();
        $session = new SessionManager();

        $login_user->setPassword($_POST['pass']);
        $login_user->setUsernameOrEmail($_POST['username_or_email']);

        $user = $login_user->completeAuthentication();

        if ($user) {
            $session->login($user);
            if ($user->isAdmin()) {
                header("Location: ../authors/authors_dashboard.php");
            } else {
                $userId = $user->getId();
                $profile = new Profile();
                $profile = $profile->findProfileWithUserId($userId);
                if (!$profile->isProfileSetUp()) {
                    header("Location: ../../home/profileSetup.php");
                } else {
                    header("Location: ../../home/index.php");
                }
            }
            exit;
        }
    }
