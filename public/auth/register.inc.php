<?php
session_start();

require_once '../../vendor/autoload.php';


use ProjektiBlog\public\classes\Profile;
use ProjektiBlog\public\classes\Validator;
use ProjektiBlog\public\classes\RegisterUser;

if ($_SERVER['REQUEST_METHOD'] === "POST") {
  try {
    $register = new RegisterUser();
    $validator = new Validator();
    if (!$validatedEmail = $validator->validateEmail($_POST['email'])) {
      throw new Exception('Email format is not valid');
    }
    if (!$validatedPasswordConfirm = $validator->validateConfirmPassword($_POST['pass'], $_POST['passConfirm'])) {
      throw new Exception('Passwords do not match, please try again');
    }
    $register->setUsername($_POST['username']);
    $name = explode(' ', $_POST['name']);
    $register->setName($name[0]);
    $register->setSurname($name[1]);
    $register->setEmail($_POST['email']);
    $register->setPassword($_POST['pass']);
    $register->setProfilePicture('blankProfileImage.webp');

    if ($register->userExists()) {
      throw new Exception("A user with this username or email already exists.");
    }

    if (!$register->create()) {
      throw new Exception("Could not register you! Please refresh your page and try again.");
    }

    $email = $register->getEmail();
    $registeredUser = $register->getUserIdByEmail($register->getEmail());
    $userId = $registeredUser->getId();
    $full_name = $registeredUser->getName() . ' ' . $register->getSurname();

    $profile = new Profile();

    $profile->setUserId($userId);
    $profile->setFullName($full_name);

    if (!$profile->createUserProfile()) {
      throw new Exception('Error creating user profile please try again later');
    }

    $_SESSION['registerSuccesful'] = "Registration successful, Login with your credentials";
    header('Location: ../../user/login.php');
  } catch (Exception $e) {
    $_SESSION["userExists"] = $e->getMessage();
    header("Location: ../../user/register.php");
  }
}
