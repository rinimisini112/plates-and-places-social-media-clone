<?php

namespace ProjektiBlog\public\classes;

use PDO;
use \Exception;
use ProjektiBlog\public\classes\Dbconn;
use ProjektiBlog\public\classes\ImageHelper;

include 'Dbconn.php';

class User extends Dbconn
{
    use ImageHelper;

    protected $id;
    protected $name;
    protected $surname;
    protected $email;
    protected $username;
    protected $profile_picture_path;
    protected $password;
    protected $role;
    protected $created_at;
    protected $image;

    public static $db_table = 'users';
    public static $db_table_fields = ['name', 'surname', 'email', 'username', 'password', 'profile_picture_path'];


    public function setName($name)
    {
        $this->name = $this->cleanInput($name);
    }

    public function setSurname($surname)
    {
        $this->surname = $this->cleanInput($surname);
    }

    public function setEmail($email)
    {
        $this->email = $this->cleanInput($email);
    }

    public function setUsername($username)
    {
        $this->username = $this->cleanInput($username);
    }

    public function setPassword($password)
    {
        $hashedPassword = password_hash($password, PASSWORD_BCRYPT);
        $this->password = $hashedPassword;
    }
    public function setRole($role)
    {
        $this->role = $role;
    }
    public function setId($id)
    {
        $this->id = $id;
    }
    protected function cleanInput($name)
    {
        $name = trim($name);
        $name = stripslashes($name);
        $name = htmlspecialchars($name);
        return $name;
    }
    //register user user


    public function getSurname()
    {
        return $this->surname;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRole()
    {
        return $this->role;
    }
    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }
    public function getProfilePicture()
    {
        return $this->profile_picture_path;
    }
    public function setImage($image)
    {
        $this->image = $image;
    }
    public function getimage()
    {
        return $this->image;
    }

    public function setProfilePicture($profile_picture_path)
    {
        $this->profile_picture_path = $profile_picture_path;
    }
    public function getRandomUsers($limit = 4)
    {
        $query = "SELECT * FROM users ORDER BY RAND() LIMIT :limit";

        $stmt = $this->prepare($query);
        $stmt->bindParam(':limit', $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\{$this->getClassName()}");

        return $stmt->fetchAll();
    }

    public function update()
    {
        try {
            $this->setSrc("../../resources/images/");
            if (isset($this->image)) {
                $this->uploadfile = $this->src . $this->profile_picture_path;
                if (!empty($this->uploadfile) && $this->size > 0) {
                    unlink($this->uploadfile);
                }
                $this->startupLoad($this->image);
                $this->profile_picture_path = $this->filename;
                $uploadFile = $this->uploadFile();
                if ($uploadFile) {
                    if (parent::update()) {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                if (parent::update()) {
                    return true;
                }
            }
        } catch (Exception $e) {
            echo "User " . $e->getMessage();
        }
    }
    public function fetchUserWithUsername()
    {
        try {
            $query = 'SELECT * FROM users WHERE username = :username';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':username', $this->username);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . '\\User');
            return $stmt->fetch();
        } catch (Exception $e) {
            echo "COuld not find user " . $e->getMessage();
        }
    }

    public function searchUsers($searchResult)
    {
        try {
            $query = "SELECT * FROM users 
            WHERE CONCAT(name, ' ', surname) LIKE :searchTerm 
            OR username LIKE :searchTerm";
            $stmt = $this->prepare($query);
            $stmt->bindValue(':searchTerm', "%" . $searchResult . "%");
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\User");

            return $stmt->fetchAll();
        } catch (Exception $e) {
            echo 'Error fetching users ' . $e->getMessage();
        }
    }
}
