<?php

namespace ProjektiBlog\public\classes;

use PDO;
use ProjektiBlog\public\classes\Dbconn;

class PostTag extends Dbconn
{
    protected $id;
    protected $post_id;
    protected $tag_id;

    public static $db_table = 'posts_tags';
    public static $db_table_fields = ['post_id', 'tag_id'];

    public function getId()
    {
        return $this->id;
    }

    public function getPostId()
    {
        return $this->post_id;
    }

    public function setPostId($post_id)
    {
        $this->post_id = $post_id;

        return $this;
    }
    public function getTagId()
    {
        return $this->tag_id;
    }

    public function setTagId($tag_id)
    {
        $this->tag_id = $tag_id;

        return $this;
    }
}
