<?php

namespace ProjektiBlog\public\classes;

require_once __DIR__ . '/../../config/config.php';

use Exception;
use \PDO;
use \PDOException;
use ReflectionClass;

class Dbconn
{
    private $host = DB_HOST;
    private $dbname = DB_NAME;
    private $dbuser = DB_USER;
    private $dbpassword = DB_PASS;


    protected function connect()
    {
        try {
            $pdo = new PDO(
                'mysql:host=' . $this->host . ";dbname=" . $this->dbname,
                $this->dbuser,
                $this->dbpassword,
                array(PDO::ATTR_PERSISTENT => true)
            );
            $pdo->setAttribute(
                PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION
            );
        } catch (PDOException $e) {
            die('Connection failed: ' . $e->getMessage());
        }
        return $pdo;
    }
    protected function prepare($sql)
    {
        return $this->connect()->prepare($sql);
    }
    protected function getClassName()
    {
        $class_name = new ReflectionClass($this);
        return ucfirst($class_name->getShortName());
    }
    private function properties()
    {
        $properties = [];
        foreach (static::$db_table_fields as $db_field) {
            if (property_exists($this, $db_field)) {
                $properties[$db_field] = $this->$db_field;
            }
        }
        return $properties;
    }

    public function create()
    {
        try {
            $properties = $this->properties();

            $sql = "INSERT INTO " . static::$db_table . "(" . implode(',', array_keys($properties)) . ")";
            $sql .= " VALUES('" . implode("','", array_values($properties)) . "')";
            $stmt = $this->prepare($sql);

            return $stmt->execute();
        } catch (\Throwable $th) {
            echo 'Error inserting :' . $th->getMessage();
        }
    }

    public function update()
    {
        try {
            $properties = $this->properties();
            $properties_pair = [];

            foreach ($properties as $key => $value) {
                $properties_pair[] = "{$key} = '{$value}'";
            }

            $sql = "UPDATE " . static::$db_table . " SET ";
            $sql .= implode(", ", $properties_pair);

            $sql .= " WHERE id=:id";
            $stmt = $this->prepare($sql);
            $stmt->bindParam(":id", $this->id);
            $stmt->execute();
            return true;
        } catch (\Throwable $th) {
            die("Error during the modification process" . $th->getMessage());
        }
    }

    public function fetchAll()
    {
        $sql = "SELECT * FROM " . static::$db_table;
        $stmt = $this->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\{$this->getClassName()}");
        return $stmt->fetchAll();
    }

    public function fetchWithId($id)
    {
        try {
            $this->id = $id;
            $sql = "SELECT * FROM " . static::$db_table . " WHERE id=:id";
            $stmt = $this->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\{$this->getClassName()}");
            return $stmt->fetch();
        } catch (Exception $e) {
            echo "Error finding specific " . $this->getClassName();
        }
    }

    public function delete()
    {
        try {
            $sql = "DELETE FROM " . static::$db_table . " WHERE id=:id";
            $stmt = $this->prepare($sql);
            $stmt->bindParam(':id', $this->id);
            $stmt->execute();

            return true;
        } catch (\Throwable $th) {
            die("Error during the modification process" . $th->getMessage());
        }
    }
}
