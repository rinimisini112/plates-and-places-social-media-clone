<?php

namespace ProjektiBlog\public\classes;

use PDO;
use ProjektiBlog\public\classes\Dbconn;

class ViewedUser extends Dbconn
{
    protected $id;
    protected $user_id;
    protected $viewed_users_id;
    protected $viewed_at;
    public static $db_table = 'viewed_users';
    public static $db_table_fields = ['user_id', 'viewed_users_id'];

    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getViewedUsersId()
    {
        return $this->viewed_users_id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function setViewedUsersId($viewed_users_id)
    {
        $this->viewed_users_id = $viewed_users_id;

        return $this;
    }
    public function getSearchHistory()
    {
        try {
            $query = 'SELECT * FROM viewed_users ';
            $query .= 'WHERE user_id = :user_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_INT);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\{$this->getClassName()}");

            return $stmt->fetchAll();
        } catch (\Throwable $th) {
            echo 'Error getting search history - ' . $th->getMessage();
            return false;
        }
    }
    public function setSearchHistory()
    {
        try {
            if ($this->searchedUserBefore() == 0) {
                parent::create();
            } else {
                $this->searchedUserAgain();
            }
        } catch (\Throwable $th) {
            echo 'Error setting viewed history';
        }
    }
    private function searchedUserBefore()
    {
        try {
            $query = 'SELECT * FROM viewed_users ';
            $query .= 'WHERE user_id = :user_id AND viewed_users_id = :viewed_users_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_INT);
            $stmt->bindParam(':viewed_users_id', $this->viewed_users_id, PDO::PARAM_INT); // Assuming you have a property named viewed_users_id
            $stmt->execute();

            $rowCount = $stmt->rowCount();
            return $rowCount;
        } catch (\Throwable $th) {
            echo 'Error getting search history';
        }
    }
    private function searchedUserAgain()
    {
        try {
            $updateQuery = 'UPDATE viewed_users SET viewed_at = NOW() ';
            $updateQuery .= 'WHERE user_id = :user_id AND viewed_users_id = :viewed_users_id';
            $updateStmt = $this->prepare($updateQuery);
            $updateStmt->bindParam(':user_id', $this->user_id, PDO::PARAM_INT);
            $updateStmt->bindParam(':viewed_users_id', $this->viewed_users_id, PDO::PARAM_INT);
            $updateStmt->execute();

            return \true;
        } catch (\Throwable $th) {
            echo 'Error updating viewed history';
            return false;
        }
    }
    public function clear()
    {
        try {

            if ($this->getSearchHistory()) {
                $query = 'DELETE FROM viewed_users WHERE user_id = :user_id';
                $stmt = $this->prepare($query);
                $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_INT);
                $stmt->execute();
                return true;
            } else {
                return false;
            }
        } catch (\Throwable $th) {
            echo 'Could not erase search history - ' . $th->getMessage();
            return false;
        }
    }
}
