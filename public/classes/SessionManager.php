<?php

namespace ProjektiBlog\public\classes;

class SessionManager
{
    private $signedIn = false;
    private $user_id;
    private $user_role;
    private $message;

    public function __construct()
    {
        \session_start();
        $this->checkLogin();
        $this->checkMessage();
    }
    public function isSignedIn()
    {
        return $this->signedIn;
    }

    public function getUserRole()
    {
        return $this->user_role;
    }
    public function checkLogin()
    {
        if (isset($_SESSION['userid']) && isset($_SESSION['role'])) {
            $this->user_id = $_SESSION['userid'];
            $this->user_role = $_SESSION['role'];
            $this->signedIn = true;
        } else {
            unset($this->user_id);
            unset($this->user_role);
            $this->signedIn = false;
        }
    }
    public function login($user)
    {
        if ($user) {
            $this->user_id = $user->getId();
            $_SESSION['userid'] = $user->getId();
            $this->user_role = $user->getRole();
            $_SESSION['role'] = $user->getRole();
            $this->signedIn = true;
        }
    }
    public function checkMessage()
    {
        if (isset($_SESSION['message'])) {
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = '';
        }
    }
    public function message($msg = '')
    {
        if (!empty($msg)) {
            $this->message = $_SESSION['message'] = $msg;
        } else {
            return $this->message;
        }
    }
    public function logout()
    {
        unset($_SESSION['userid']);
        unset($_SESSION['role']);
        unset($this->user_id);
        unset($this->user_role);
        $this->signedIn = false;
    }
}
