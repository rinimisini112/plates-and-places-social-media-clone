<?php

namespace ProjektiBlog\public\classes;

use PDO;
use ProjektiBlog\public\classes\Dbconn;

class Like extends Dbconn
{
    protected $id;
    protected $user_id;
    protected $post_id;
    protected $liked_at;
    public static $db_table = 'likes';
    public static $db_table_fields = ['user_id', 'post_id'];
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getPostId()
    {
        return $this->post_id;
    }

    public function setPostId($post_id)
    {
        $this->post_id = $post_id;

        return $this;
    }

    public function getLikedAt()
    {
        return $this->liked_at;
    }

    public function setLikedAt($liked_at)
    {
        $this->liked_at = $liked_at;

        return $this;
    }
    public function getLikeCountForPost()
    {
        try {
            $query = 'SELECT COUNT(user_id) AS like_count FROM likes WHERE post_id = :post_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':post_id', $this->post_id);
            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return isset($result['like_count']) ? (int)$result['like_count'] : 0;
        } catch (\Throwable $th) {
            return 0;
        }
    }
    public function isLikedByUser()
    {
        try {
            $query = 'SELECT * FROM ' . self::$db_table;
            $query .= ' WHERE user_id = :user_id AND post_id = :post_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':user_id', $this->user_id);
            $stmt->bindParam(':post_id', $this->post_id);
            $stmt->execute();

            return $stmt->rowCount() > 0;
        } catch (\Throwable $th) {
            return false;
        }
    }
    public function unlike()
    {
        try {
            $query = 'DELETE FROM ' . self::$db_table;
            $query .= ' WHERE user_id = :user_id AND post_id = :post_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':user_id', $this->user_id);
            $stmt->bindParam(':post_id', $this->post_id);

            return $stmt->execute();
        } catch (\Throwable $th) {
            echo 'Error unliking post ' . $th->getMessage();
        }
    }
}
