<?php
namespace ProjektiBlog\public\classes;

use ProjektiBlog\public\classes\Dbconn;

include 'Dbconn.php';

class Replies extends Dbconn
{
    protected $id;
    protected $user_id;
    protected $comment_id;
    protected $content;
    protected $created_at;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
    }

    public function getCommentId() {
        return $this->comment_id;
    }

    public function setCommentId($comment_id) {
        $this->comment_id = $comment_id;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = $content;
    }

    public function getCreatedAt(){
        return $this->created_at;
    }

    public function setCreatedAt($created_at){
        $this->created_at = $created_at;
    }
}