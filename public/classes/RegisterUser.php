<?php
namespace ProjektiBlog\public\classes;

use Exception;
use PDO;
use ProjektiBlog\public\classes\User;

class RegisterUser extends User
{
    public function getUserIdByEmail()
    {
        try { 
            $query = "SELECT * FROM users WHERE email = :email";
         $stmt = $this->prepare($query);
         $stmt->bindParam(':email', $this->email);
         $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . '\\RegisterUser');
         return $stmt->fetch();
        } catch(Exception $e) {
         echo "Error creating user profile" . $e->getMessage(); 
     }
    }


    public function userExists()
    {
        try {
            // Check if a user with the same username or email already exists
            $query = "SELECT id,username, email FROM users WHERE username = :username OR email = :email";
            $stmt = $this->prepare($query);
            $stmt->bindParam(':username', $this->username);
            $stmt->bindParam(':email', $this->email);
            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return ($result !== false);
        } catch (Exception $e) {
            echo "Error confirming if user exists: " . $e->getMessage();
            return false;
        }
    }
}

?>
