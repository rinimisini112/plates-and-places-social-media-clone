<?php

namespace ProjektiBlog\public\classes;

use PDO;
use \Exception;
use ProjektiBlog\public\classes\Dbconn;
use ProjektiBlog\public\classes\ImageHelper;

class Post extends Dbconn
{
    use ImageHelper;
    protected $id;
    protected $user_id;
    protected $content;
    protected $created_at;
    protected $edited_at;
    protected $media;
    protected $media_image;
    public static $db_table = 'posts';
    public static $db_table_fields = ['user_id', 'content', 'media'];
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getEditedAt()
    {
        return $this->edited_at;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }
    public function getMediaImage()
    {
        return $this->media_image;
    }

    public function setMediaImage($media_image)
    {
        $this->media_image = $media_image;

        return $this;
    }

    public function setEdited_at($edited_at)
    {
        $this->edited_at = $edited_at;

        return $this;
    }

    public function create()
    {
        try {
            $this->setSrc("../../resources/images/");

            if (!empty($this->media_image)) {
                $this->startupLoad($this->media_image);
                $this->media = $this->filename;
                $uploadFile = $this->uploadFile();
                if ($uploadFile) {
                    parent::create();
                    return true;
                }
            } else if (empty($this->media_image)) {
                parent::create();
                return true;
            } else {
                $error_string = '';
                foreach ($this->errors as $error) {
                    $error_string .= $error;
                }
                throw new Exception($error_string);
            }
        } catch (Exception  $e) {
            $_SESSION['message'] = $e->getMessage();
        }
    }
    public function fetchPostForUser($content = '')
    {
        try {
            $query = 'SELECT * FROM ' . self::$db_table . ' WHERE user_id = :user_id';
            if (!empty($content)) {
                $query .= ' AND content = :content';
            }
            $stmt = $this->prepare($query);
            $stmt->bindParam(':user_id', $this->user_id);

            if (!empty($content)) {
                $stmt->bindParam(':content', $content);
            }
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\{$this->getClassName()}");
            if (!empty($content)) {
                return $stmt->fetch();
            } else {
                return $stmt->fetchAll();
            }
        } catch (\Exception $e) {
            'Error getting post - ' . $e->getMessage();
            return false;
        }
    }
    public function fetchPostsForHome()
    {
        try {
            $query = 'SELECT DISTINCT posts.*
            FROM posts
            LEFT JOIN followers ON posts.user_id = followers.user_id
            WHERE followers.follower_user_id = :user_id OR posts.user_id = :user_id
            ORDER BY posts.created_at DESC';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':user_id', $this->user_id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\{$this->getClassName()}");

            return $stmt->fetchAll();
        } catch (\Exception $e) {
            echo 'Error getting posts for homepage ' . $e->getMessage();
        }
    }
}
