<?php

namespace ProjektiBlog\public\classes;

use PDO;
use \Exception;
use ProjektiBlog\public\classes\Dbconn;
use ProjektiBlog\public\classes\ImageHelper;

class Comment extends Dbconn
{
    use ImageHelper;

    protected $id;
    protected $user_id;
    protected $post_id;
    protected $content;
    protected $media;
    protected $mediaImage;
    protected $created_at;

    public static $db_table = 'comments';
    public static $db_table_fields = ['user_id', 'post_id', 'content', 'media'];

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getPostId()
    {
        return $this->post_id;
    }

    public function setPostId($post_id)
    {
        $this->post_id = $post_id;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }
    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }
    public function getMediaImage()
    {
        return $this->mediaImage;
    }

    public function setMediaImage($mediaImage)
    {
        $this->mediaImage = $mediaImage;

        return $this;
    }
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }
    public function create()
    {
        try {
            $this->setSrc("../../resources/images/");

            if (!empty($this->mediaImage)) {
                $this->startupLoad($this->mediaImage);
                $this->media = $this->filename;
                $uploadFile = $this->uploadFile();
                if ($uploadFile) {
                    parent::create();
                    return true;
                }
            } else if (empty($this->mediaImage)) {
                parent::create();
                return true;
            } else {
                foreach ($this->errors as $error) {
                    echo $error . "<br>";
                }
            }
        } catch (Exception  $e) {
            echo "Test" . $e->getMessage();
        }
    }
    public function fetchCommentsForPost()
    {
        try {
            $query = 'SELECT * FROM ' . self::$db_table . ' WHERE post_id = :post_id ORDER BY created_at DESC';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':post_id', $this->post_id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\{$this->getClassName()}");

            return $stmt->fetchAll();
        } catch (\Throwable $th) {
            echo 'Error loading comments for posts : - ' . $th->getMessage();
        }
    }
    public function fetchSingleComment()
    {
        try {
            $query = 'SELECT * FROM ' . self::$db_table;
            $query .= ' WHERE post_id = :post_id AND user_id = :user_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam('post_id', $this->post_id);
            $stmt->bindParam('user_id', $this->user_id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\{$this->getClassName()}");

            return $stmt->fetch();
        } catch (\Throwable $th) {
            echo 'Error getting single comment for user : - ' . $th->getMessage();
        }
    }
}
