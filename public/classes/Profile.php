<?php

namespace ProjektiBlog\public\classes;

use PDO;
use DateTime;
use Exception;
use ProjektiBlog\public\classes\Dbconn;
use ProjektiBlog\public\classes\ImageHelper;

class Profile extends Dbconn
{
    use ImageHelper;
    protected $id;
    protected $user_id;
    protected $full_name;
    protected $bio;
    protected $cover_image;
    protected $image;
    protected $website_link;
    protected $is_setup;
    protected $created_at;

    public static $db_table = 'profiles';
    public static $db_table_fields = ['full_name', 'bio', 'cover_image', 'website_link'];

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function getFullName()
    {
        return $this->full_name;
    }

    public function setFullName($fullname)
    {
        $this->full_name = $fullname;
    }

    public function getBio()
    {
        return $this->bio;
    }

    public function setBio($bio)
    {
        $this->bio = $bio;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }
    public function getCoverImage()
    {
        return $this->cover_image;
    }


    public function setCoverImage($cover_image)
    {
        $this->cover_image = $cover_image;
    }
    public function getWebsiteLink()
    {
        return $this->website_link;
    }

    public function setWebsiteLink($website_link)
    {
        if (!empty($website_link)) {
            $this->website_link = $website_link;
        }
    }

    public function getIsSetup()
    {
        return $this->is_setup;
    }

    public function setIsSetup($is_setup)
    {
        $this->is_setup = $is_setup;
    }

    public function isProfileSetUp()
    {
        if ($this->getIsSetup() !== 1) {
            return false;
        }
        return true;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }
    public function createUserProfile()
    {
        try {
            $query = "INSERT INTO profiles(user_id, full_name)
        VALUES(:user_id, :full_name)";
            $stmt = $this->prepare($query);
            $stmt->bindParam(':user_id', $this->user_id);
            $stmt->bindParam(':full_name', $this->full_name);
            $stmt->execute();

            return true;
        } catch (Exception $e) {
            echo "Error creating user profile" . $e->getMessage();
        }
    }
    public function findProfileWithUserId($id): mixed
    {
        try {
            $this->user_id = $id;
            $query = 'SELECT * FROM profiles WHERE user_id = :id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . '\\Profile');
            return $stmt->fetch();
        } catch (\Throwable $th) {
            echo "Error getting user profile - " . $th->getMessage();
        }
    }
    public function formatCreatedAt()
    {
        $timestamp = strtotime($this->created_at); // Replace $row['created_at'] with the actual column name

        // Convert the timestamp to a DateTime object
        $date = new DateTime("@$timestamp");

        // Format the date as "F Y" (full month name and four-digit year)
        $formattedDate = $date->format('F Y');

        return $formattedDate;
    }
    public function update()
    {
        try {
            $this->setSrc("../../resources/images/");

            if (isset($this->image)) {
                $this->uploadfile = $this->src . $this->cover_image;
                if ($this->uploadfile != null && $this->size > 0) {
                    unlink($this->uploadfile);
                }

                $this->startupLoad($this->image);
                $this->cover_image = $this->filename;
                $uploadFile = $this->uploadFile();
                if ($uploadFile) {
                    if (parent::update()) {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                if (parent::update()) {
                    return true;
                }
            }
        } catch (Exception $e) {
            echo "User " . $e->getMessage();
        }
    }
    private function finishSetup()
    {
        try {
            $query = 'UPDATE profiles SET is_setup = :is_setup WHERE id = :id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':is_setup', $this->is_setup, PDO::PARAM_INT);
            $stmt->bindParam(':id', $this->id);
            $stmt->execute();
            return true;
        } catch (Exception $e) {
            echo "Error finishing " . $this->getClassName() . ' setup' . $e->getMessage();
            return false;
        }
    }
    public function confirmSetup()
    {
        try {
            if (!is_null($this->bio) && !is_null($this->cover_image) && !is_null($this->full_name)) {
                $this->setIsSetup(1);
            } else {
                $this->setIsSetup(0);
                $this->bio = null;
            }
            if ($this->finishSetup()) {
                return true;
            }
        } catch (\Throwable $th) {
            echo 'Error during ' . $this->getClassName() . ' setup confirmation' . $th->getMessage();
            return false;
        }
    }
}
