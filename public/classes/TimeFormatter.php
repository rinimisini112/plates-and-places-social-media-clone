<?php

namespace ProjektiBlog\public\classes;

date_default_timezone_set('Europe/Skopje');

use DateTime;

class TimeFormatter
{
    public static function formatTimeAgo($timestamp)
    {
        $targetTime = new DateTime($timestamp);
        $currentTime = new DateTime();
        $interval = $currentTime->diff($targetTime);
        $secondsDifference = $interval->s + ($interval->i * 60) + ($interval->h * 3600) + ($interval->d * 86400) + ($interval->m * 30 * 86400) + ($interval->y * 365 * 86400);

        if ($secondsDifference < 60) {
            return 'Just now';
        }

        $years = floor($interval->y);
        $days = floor(($secondsDifference % 31536000) / 86400);
        $hours = floor(($secondsDifference % 86400) / 3600);
        $minutes = floor(($secondsDifference % 3600) / 60);

        $result = '';
        if ($years > 0) {
            $result .= $years . ' years ';
        }
        if ($days > 0) {
            $result .= $days . ' days ';
        }
        if ($hours > 0) {
            $result .= $hours . ' hours ';
        }
        if ($minutes > 0) {
            if ($minutes > 1) {
                $minS = 'minutes';
            } else {
                $minS = 'minute';
            }
            $result .= $minutes . ' ' . $minS;
        }

        return trim($result);
    }
    public static function formatRoundedTimeAgo($timestamp)
    {
        $targetTime = new DateTime($timestamp);
        $currentTime = new DateTime();
        $interval = $currentTime->diff($targetTime);
        $secondsDifference = $interval->s + ($interval->i * 60) + ($interval->h * 3600) + ($interval->d * 86400) + ($interval->m * 30 * 86400) + ($interval->y * 365 * 86400);

        if ($secondsDifference < 60) {
            return 'Just now';
        }

        $days = floor(($secondsDifference % 31536000) / 86400);
        $hours = floor(($secondsDifference % 86400) / 3600);
        $minutes = floor(($secondsDifference % 3600) / 60);

        $result = '';
        if ($days > 0) {
            $result .= $days . ' day' . ($days > 1 ? 's' : '') . ' ago';
        } elseif ($hours >= 1 && $minutes >= 30) {
            // Round hours by half an hour
            $result .= ($hours + 1) . ' hour' . (($hours + 1) > 1 ? 's' : '') . ' ago';
        } elseif ($hours >= 1) {
            $result .= $hours . ' hour' . ($hours > 1 ? 's' : '') . ' ago';
        } elseif ($minutes > 0) {
            $result .= $minutes . ' minute' . ($minutes > 1 ? 's' : '') . ' ago';
        }

        return trim($result);
    }
}
