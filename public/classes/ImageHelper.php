<?php

namespace ProjektiBlog\public\classes;

trait ImageHelper
{
    public $src;
    public $tmp;
    public $filename;
    public $type;
    public $size;
    public $uploadfile;
    public $errors = array();
    public function getErrors()
    {
        foreach ($this->errors as $error) {
            return $error . ' ';
        }
    }
    public function setSrc($src)
    {
        $this->src = $src;
    }
    public function startupLoad($file)
    {
        $this->filename = $file["name"];
        $this->tmp = $file["tmp_name"];
        $this->size = $file["size"];
        $this->type = $file["type"];
        $this->uploadfile = $this->src . basename($this->filename);
    }


    public function uploadFile()
    {
        if (isset($this->filename)) { //123456.png ['123456', 'png']
            $file_ext = explode('.', $this->filename);
            $file_ext = end($file_ext);
            $extensions = array("jpeg", "jpg", "png", "gif", "webp", 'mp4', 'mp3');

            if (!in_array($file_ext, $extensions)) {
                $errors[] = "Extension not allowed, please choose a JPEG, GIF or PNG file.";
            }

            if ($this->size > 20097152) {
                $errors[] = 'File size must not exceed 20 MB';
            }

            if (empty($errors) === true) {
                move_uploaded_file($this->tmp, $this->uploadfile);
                return true;
            } else {
                $this->errors = $errors;
                return false;
            }
        }
    }
}
