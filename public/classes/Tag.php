<?php

namespace ProjektiBlog\public\classes;

use PDO;
use ProjektiBlog\public\classes\Dbconn;

class Tag extends Dbconn
{
    protected $id;
    protected $name;
    protected $created_at;

    public static $db_table = 'tags';
    public static $db_table_fields = ['name'];

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getTagIdByName()
    {
        try {
            $query = 'SELECT id FROM ' . self::$db_table;
            $query .= ' WHERE name = :name';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':name', $this->name);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\{$this->getClassName()}");

            return $stmt->fetch();
        } catch (\Throwable $th) {
            echo 'Error getting ' . $this->getClassName() . ' - ' . $th->getMessage();
            return false;
        }
    }
}
