<?php

namespace ProjektiBlog\public\classes;

use Exception;
use PDO;
use ProjektiBlog\public\classes\User;

require "User.php";
class LoginUser extends User
{
    private $username_or_email;
    private $authenticatedUser;
    private $error_message;

    public function setUsernameOrEmail($username_or_email)
    {
        $this->username_or_email = $this->cleanInput($username_or_email);
    }
    public function getUsernameOrEmail()
    {
        return $this->username_or_email;
    }
    public function setPassword($password)
    {
        $this->password = $this->cleanInput($password);
    }
    public function getErrorMessage()
    {
        return $this->username_or_email;
    }
    /**
     * getUser
     *
     * 
     * @return mixed
     */
    private function getUserForAuth(): mixed
    {
        try {
            $sql = "SELECT * FROM users WHERE username = :usernameOrEmail OR email = :usernameOrEmail";
            $stmt = $this->prepare($sql);
            $stmt->bindParam(':usernameOrEmail', $this->username_or_email);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . "\\LoginUser");
            return $stmt->fetch();
        } catch (Exception $e) {
            echo 'Error getting user :' . $e->getMessage();
        }
    }

    /**
     * verifyPassword
     *
     * @param  mixed $user
     * @return bool
     */
    private function authenticate(): bool
    {
        try {
            $user = $this->getUserForAuth();
            if (!$user) {
                throw new Exception('Username or password are incorrect. Please try again.');
            }

            // Verify the password
            $storedPassword = $user->getPassword();
            if (password_verify($this->password, $storedPassword)) {
                // Authentication successful
                $this->authenticatedUser = $user;
                return true;
            } else {
                throw new Exception('Your password is incorrect. Please try again.');
            }
        } catch (Exception $e) {
            $this->error_message  = $e->getMessage();
            return false;
        }
    }

    public function completeAuthentication()
    {
        if ($this->authenticate()) {
            return $this->authenticatedUser;
        } else {
            $_SESSION['message'] = $this->error_message;
            header("Location: ../../user/login.php");
            return false;
        }
    }
    /**
     * Checks if the user is admin
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        if ($this->getRole() !== 2) {
            return false;
        }
        return true;
    }
}
