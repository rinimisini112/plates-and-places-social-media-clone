<?php

namespace ProjektiBlog\public\classes;

use ProjektiBlog\public\classes\Dbconn;

class CommentTag extends Dbconn
{
    protected int $id;
    protected int $comment_id;
    protected int $tag_id;

    public static $db_table = 'comments_tags';
    public static $db_table_fields = ['comment_id', 'tag_id'];

    public function getId()
    {
        return $this->id;
    }


    public function getComment_id()
    {
        return $this->comment_id;
    }

    public function getTag_id()
    {
        return $this->tag_id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setComment_id($comment_id)
    {
        $this->comment_id = $comment_id;

        return $this;
    }

    public function setTag_id($tag_id)
    {
        $this->tag_id = $tag_id;

        return $this;
    }
}
