<?php

namespace ProjektiBlog\public\classes;

use Exception;
use PDO;
use ProjektiBlog\public\classes\Dbconn;


class Follower extends Dbconn
{
    protected $id;
    protected $user_id;
    protected $follower_user_id;
    protected $followed_at;

    public static $db_table = 'followers';
    public static $db_table_fields = ['user_id', 'follower_user_id'];

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function getFollowerUserId()
    {
        return $this->follower_user_id;
    }

    public function setFollowerUserId($follower_user_id)
    {
        $this->follower_user_id = $follower_user_id;
    }
    public function getFollowedAtTime()
    {
        try {
            $query = 'SELECT * FROM followers WHERE user_id = :user_id AND follower_user_id = :follower_user_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':follower_user_id', $this->follower_user_id);
            $stmt->bindParam(':user_id', $this->user_id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, __NAMESPACE__ . '\\Follower');
            return $stmt->fetch();
        } catch (Exception $e) {
            echo 'Error getting result ' . $e->getMessage();
        }
    }
    public function getFollowedAt()
    {
        return $this->followed_at;
    }
    public function setFollowedAt($followed_at)
    {
        $this->followed_at = $followed_at;
    }

    public function getFollowersCount(): int
    {
        $followers = $this->getFollowersNum();
        return count($followers);
    }

    public function getFollowingCount(): int
    {
        $following = $this->getFollowingNum();
        return count($following);
    }
    public function youFollowUser($follower_user_id)
    {
        try {
            $query = 'SELECT * FROM followers WHERE user_id = :user_id AND follower_user_id = :follower_user_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':follower_user_id', $this->follower_user_id);
            $stmt->bindParam(':user_id', $follower_user_id);
            $stmt->execute(); // Execute the query
            $rowCount = $stmt->rowCount(); // Get the number of rows returned

            return $rowCount > 0; // If rows are returned, the user is following; otherwise, they are not
        } catch (Exception $e) {
            echo 'Error getting info about if you follow this user: ' . $e->getMessage();
        }
    }
    public function getFollowersNum()
    {
        try {
            $query = 'SELECT follower_user_id FROM followers WHERE user_id = :user_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':user_id', $this->user_id);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

            return $result ?: array();
        } catch (\Throwable $th) {
            echo 'Error getting followers for user :' . $th->getMessage();
        }
    }

    public function getFollowingNum()
    {
        try {
            $query = 'SELECT user_id FROM followers WHERE follower_user_id = :follower_user_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':follower_user_id', $this->follower_user_id);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

            return $result ?: array();
        } catch (\Throwable $th) {
            echo 'Error getting following for user :' . $th->getMessage();
        }
    }

    public function unfollow()
    {
        try {
            $query = 'DELETE FROM followers WHERE user_id = :user_id AND follower_user_id = :follower_user_id';
            $stmt = $this->prepare($query);
            $stmt->bindParam(':user_id', $this->user_id);
            $stmt->bindParam(':follower_user_id', $this->follower_user_id);
            $stmt->execute();
            return true;
        } catch (Exception $e) {
            echo 'Could not unfollow user :' . $e->getMessage();
            return false;
        }
    }
}
