<?php
namespace projektiBlog\public\classes;
require '../../vendor/autoload.php';
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

class Validator
{
    
    /**
     * Validate an email address
     *
     * @param string $email
     * @return bool
     */
    public function validateEmail($email)
    {
        $validEmail = filter_var($email, FILTER_VALIDATE_EMAIL);

        return $validEmail !== false;
    }

 /**
     * Validate a confirm password
     *
     * @param string $password
     * @param string $confirmPassword
     * @return bool
     */
    public function validateConfirmPassword($password, $confirmPassword)
    {
        $validator = Validation::createValidator();

        // Validate confirm password
        $violations = $validator->validate($confirmPassword, [
            new Assert\NotBlank(),
            new Assert\EqualTo([
                'value' => $password,
                'message' => 'Passwords do not match.',
            ]),
        ]);

        return count($violations) === 0;
    }
}
